<?php $settings = App\Models\AdminSettings::first(); ?>
@extends('app')

@section('title'){{ trans('misc.terms').' - ' }}@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('public/plugins/datepicker/datepicker3.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('content')
 
 <div class="jumbotron md index-header jumbotron_set jumbotron-cover">
      <div class="container wrap-jumbotron position-relative">
      	<h1 class="title-site">Terms and Conditions</h1>
      </div>
    </div>

<div class="container" style="width:50%;margin-bottom: 5%;text-align: justify;">
	<div class="row">
		<div class="col-md-10">
			<h2 style="margin-bottom: 4%">We are a troll free website</h2>
		</div>
		<div class="col-md-2">
			<img src="{{url('public/img/goldenbow.jpg')}}" width="80" height="80"></img>
		</div>
	</div>
 	
 	<h4>Word Explanations.</h4>
 	<ul style="margin-bottom: 2%">
		<li>The <b>Giftee</b> is the Recipient of Gifts of Money i.e. the person who receives the Gifts Of Money</li>
		<li>The <b>Gifter</b> is the person who is giving a Gift Of Money.</li>
		<li><b>GOMP</b> stands for <b>G</b>ift <b>O</b>f <b>M</b>oney <b>P<b/>ot and is a wallet for Gifts Of Money to be collected in.</li>

		</li>
	</ul>
 	<div class="row">
		<div class="col-md-10">
			
		</div>
		<div class="col-md-2">
			<img src="{{url('public/img/redbow.png')}}" width="80" height="80"></img>
		</div>
	</div>
 	
 	<h2>Receiving Gifts of Money</h2>
 	<p>All people wishing to receive Gifts Of Money must complete the Registration Form. All fields in the Registration Form must be completed. Any information we ask for is needed to provide our Gift Of Money service and to comply with the law. To receive a Gift Of Money, the Giftee must pay for a GOMP (wallet) which is used to collect their Gifts Of Money. The Account Number used to pay for the GOMP MUST be the same as the Account Number in the Registration Form. This is to prevent fraud and money laundering.</p>
	 <h2>Giving A Gift Of Money</h2>
	 <p>Anyone wishing to Give A Gift Of Money using a debit or credit card does not need to register. However, to send a Payment without a debit or credit card e.g. directly 	from your bank account, requires the Gifter to Register. Any information we ask for is 	     needed to provide our Gift Of Money service and to comply with the law. 
	</p>
	<h2>Sending A Message</h2>
	 <p>A Gifter will be offered the chance to send a Message along with their Gift Of Money. There is no charge for this. However, if the Gifter wants to give the Giftee a chance to send a 'Thank You' via GOMP then the Gifter would have to Register if they haven't done so already.
	</p>
	<p>Anyone wishing to Send A Message (without giving a Gift Of Money) must register because we are a Troll Free website. We RESPECT others rights to a peaceful life free from harrassment and hate. We charge a one-off £0.01 fee ( a total charge of £0.30 including other bank/card charges) to register to send a Message which is paid for by the Person wishing to send a message to the Giftee.  By doing this, hate and harrassment crimes can be traced to the perpetrator and with this deterrant we aim to keep our website a respectful forum where people can use the service in peace :)</p>
	<h2>Prohibited Activity</h2>
	<p>Fraud and Money Laundering are prohibited on this website and any suspicious activity will be reported to the relevant authorities and we will fully assist with any investigations.</p>
 
</div>

<div class="margin-bottom-40 padding-top-40">
	<div class="row">
		
	<!-- col-md-8 -->
	<div class="col-md-12">
		<div class="wrap-center center-block">
			@include('errors.errors-forms')

</div><!-- container -->

@endsection

@section('javascript')
	<script src="{{ asset('public/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('public/plugins/tinymce/tinymce.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('public/plugins/datepicker/bootstrap-datepicker.js')}}" type="text/javascript"></script>
	
	<script type="text/javascript">
	
	//Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'dd-mm-yyyy',
      startDate: '+7d',
      language: 'en'
    });
    
    $(document).ready(function() {
	
    $("#onlyNumber").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    
    $('input').iCheck({
	  	checkboxClass: 'icheckbox_square-red',
    	radioClass: 'iradio_square-red',
	    increaseArea: '20%' // optional
	  });
	  
});

	//Flat red color scheme for iCheck
        $('input[type="radio"]').iCheck({
          radioClass: 'iradio_flat-red'
        });
        
$('#removePhoto').click(function(){
	 	$('#filePhoto').val('');
	 	$('.previewPhoto').css({backgroundImage: 'none'}).hide();
	 	$('.filer-input-dragDrop').removeClass('hoverClass');
	 });
	 	
//================== START FILE IMAGE FILE READER
$("#filePhoto").on('change', function(){
	
	var loaded = false;
	if(window.File && window.FileReader && window.FileList && window.Blob){
		if($(this).val()){ //check empty input filed
			oFReader = new FileReader(), rFilter = /^(?:image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/png|image)$/i;
			if($(this)[0].files.length === 0){return}
			
			
			var oFile = $(this)[0].files[0];
			var fsize = $(this)[0].files[0].size; //get file size
			var ftype = $(this)[0].files[0].type; // get file type
			
			
			if(!rFilter.test(oFile.type)) {
				$('#filePhoto').val('');
				$('.popout').addClass('popout-error').html("{{ trans('misc.formats_available') }}").fadeIn(500).delay(5000).fadeOut();
				return false;
			}
			
			var allowed_file_size = {{$settings->file_size_allowed * 1024}};	
						
			if(fsize>allowed_file_size){
				$('#filePhoto').val('');
				$('.popout').addClass('popout-error').html("{{trans('misc.max_size').': '.App\Helper::formatBytes($settings->file_size_allowed * 1024)}}").fadeIn(500).delay(5000).fadeOut();
				return false;
			}
		<?php $dimensions = explode('x',$settings->min_width_height_image); ?>
			
			oFReader.onload = function (e) {
				
				var image = new Image();
			    image.src = oFReader.result;
			    
				image.onload = function() {
			    	
			    	if( image.width < {{ $dimensions[0] }}) {
			    		$('#filePhoto').val('');
			    		$('.popout').addClass('popout-error').html("{{trans('misc.width_min',['data' => $dimensions[0]])}}").fadeIn(500).delay(5000).fadeOut();
			    		return false;
			    	} 
			    	
			    	if( image.height < {{ $dimensions[1] }} ) {
			    		$('#filePhoto').val('');
			    		$('.popout').addClass('popout-error').html("{{trans('misc.height_min',['data' => $dimensions[1]])}}").fadeIn(500).delay(5000).fadeOut();
			    		return false;
			    	} 
			    	
			    	$('.previewPhoto').css({backgroundImage: 'url('+e.target.result+')'}).show();
			    	$('.filer-input-dragDrop').addClass('hoverClass');
			    	var _filname =  oFile.name;
					var fileName = _filname.substr(0, _filname.lastIndexOf('.'));
			    };// <<--- image.onload

				
           }
           
           oFReader.readAsDataURL($(this)[0].files[0]);
			
		}
	} else{
		$('.popout').html('Can\'t upload! Your browser does not support File API! Try again with modern browsers like Chrome or Firefox.').fadeIn(500).delay(5000).fadeOut();
		return false;
	}
});

		$('input[type="file"]').attr('title', window.URL ? ' ' : '');
	

function initTinymce() {
			tinymce.remove('.tinymce-txt');		
tinymce.init({
  selector: '.tinymce-txt',
  relative_urls: false,
  resize: true,
  menubar:false,
    statusbar: false,
    forced_root_block : false,
    extended_valid_elements : "span[!class]", 
    //visualblocks_default_state: true,
  setup: function(editor){
  	        
    	editor.on('change',function(){
    		editor.save();
    	});
   },   
  theme: 'modern',
  height: 150,
  plugins: [
    'advlist autolink autoresize lists link image charmap preview hr anchor pagebreak', //image
    'searchreplace wordcount visualblocks visualchars code fullscreen',
    'insertdatetime media nonbreaking save code contextmenu directionality', //
    'emoticons template paste textcolor colorpicker textpattern ' //imagetools
  ],
  toolbar1: 'undo redo | bold italic underline strikethrough charmap | bullist numlist  | link | image | media',
  image_advtab: true,
 });
 
}

initTinymce();	

</script>
@endsection