<?php

$settings = App\Models\AdminSettings::first(); 
$loggedIn = Auth::check();

$conversations = $convo;
$convo_users=$convo_users;
$hasConversations = $hasconvo;
$searchedUsers = [];
$friendName = $friendName;
$friendID = $friendID;
$loggedUserID = $user_id;

?>

@extends('app')


@section('css')

<link href="{{ asset('public/bootstrap/css/bootstrap.css')}}" rel="stylesheet" type="text/css" />

@endsection

@section('content')
<div class="container">
<div class="row ">
    <!-- <h3 class="text-center" >BOOTSTRAP CHAT EXAMPLE </h3> -->
    <!-- <br /><br /> -->
    <div class="col-md-3" style="padding: 0">
          <div class="panel panel-primary" style="height: 500px">
            <div class="panel-heading" style="padding-top: 0; height: 50px">
                <img class="media-object img-circle" style="float: left; max-height:50px;" src="{{{ asset('public/img/default_user.png') }}}" />
                <div style="padding-top: 3%; font-size: 18px">
                    Chats
                </div>
            </div>
            <div class="panel-body" style="height: 445px; overflow-y: auto; max-height: 445px">
                <ul class="media-list" id="divUsers">
                    @if(empty($convo_users))

                        <img class="media-object img-circle" src="{{{ asset('public/img/default_user.png') }}}" style="margin: 0 auto" />
                        <h5>Start a conversation by searching a user.</h5>
                    @else
                    @foreach($convo_users as $users)
                    <li class="" style="cursor: pointer;border-bottom: 1px solid #eee;
                            padding-bottom: 2%; padding-top: 2%;" onclick="fetchConvo({{$users->id}}, '{{$users->name}}')">

                        <div class="media-body">

                            <div class="media">
                                <a class="pull-left" href="#">
                                    <img class="media-object img-circle" style="max-height:40px;" src="{{{ asset('public/img/default_user.png') }}}" />
                                </a>
                                <div class="media-body" >
                                    <h5>{{$users->name}}</h5>
                                   <!-- <small class="text-muted">Active From 3 hours</small> -->
                                </div>
                            </div>

                        </div>
                    </li>
                    @endforeach
                    @endif
                </ul>
            </div>
        </div>
        
    </div>
    <div class="col-md-6" style="padding: 0">
        <div class="panel panel-info" style="height: 500px">
            <div class="panel-heading" id="divChatHeader" style="height: 50px; font-size: 23px;">
                {{$friendName}}
            </div>
            <div class="panel-body" style="height: 400px; overflow-y: auto; max-height: 400px">
                <ul class="media-list" id="divConversations">
                    @foreach($conversations as $conversation)
                    <li class="media">

                        <div class="media-body">

                            <div class="">
                                <a class="pull-left" href="#" style="width: 10%">
                                    <img class="media-object img-circle " src="{{{ asset('public/img/default_user.png') }}}" style="width: 100%" />
                                </a>
                                <div class="media-body" >
                                    {{$conversation->description}}
                                    <br />
                                   <small class="text-muted">{{$conversation->updated_on}}</small>
                                    <hr />
                                </div>
                            </div>

                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="panel-footer" style="position: absolute; bottom: 21px; width: 99.75%">
                <div class="input-group">
                    <input type="text" class="form-control" id="message" placeholder="Enter Message" />
                    <span class="input-group-btn">
                        <button class="btn btn-info" onclick="sendMessage()" type="button">SEND</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3" style="padding: 0">
          <div class="panel panel-primary" style="height: 500px">
            <div class="panel-heading" style="padding-top: 0; height: 50px">
                <div style="padding-top: 3%; font-size: 18px">
                    Search 
                </div>
            </div>
            <div class="panel-body" style="height: 445px; overflow-y: auto; max-height: 445px">
                <div>
                    <input type="text" class="form-control" id="serach_email" name="serach_email">
                    <button class="btn btn-primary" onclick="searchUsers()" style="float:right;margin-top: 2%">Search</button>
                </div>
                <ul class="media-list" id="searched_users" style="clear: both; margin-top: 1%;">
                </ul>
            </div>
        </div>
        
    </div>
    <div>
        <input type="text" style="display: none;" id="username" name="username">
        <input type="text" style="display: none;" id="user_id" name="user_id">
    </div>
    
</div>
</div>
@endsection

@include('includes.javascript_general')

@section('javascript')
    <script src = "https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.9.1/underscore-min.js"> 
     </script> 
    <script type="text/javascript">

        var val = "<?php echo $hasConversations; ?>"
        var user = "<?php echo $loggedUserID; ?>"
        var friendID = "<?php echo $friendID; ?>"

        function searchUsers()
        {
            if(_.isEmpty(user))
            {
                window.location.url = URL_BASE+"/login";
                return;
            }
            $email = $('#serach_email').val();
            $.ajax({
                    type: "POST",
                    url: URL_BASE+"/api/search_users",
                    dataType: 'json',
                    contentType: "application/json",
                    Accept: 'application/json',
                    data: JSON.stringify({ "email": $email }),
                    headers: {
                      // Set any custom headers here.
                      // If you set any non-simple headers, your server must include these
                      // headers in the 'Access-Control-Allow-Headers' response header.
                      'Accept': 'application/json',
                    },
                   success: function( result ){
                    console.log(result["users"][0]);
                    var id = result["users"][0].id;
                    name = result["users"][0].name;
                    $('#username').val(name);
                    var users = result["users"];
                    for (var i = 0; i < users.length; i++) {
                        var html = '<li class="media" style="cursor: pointer;" onclick="startConvo('+id+')">';
                        html += '<div class="media-body">';
                        html += '<div class="media">'
                        html += '<a class="pull-left" href="#">'
                        html += '<img class="media-object img-circle" style="max-height:40px;" src="{{{ asset("public/img/default_user.png") }}}" />';
                        html += '</a>';
                        html += '<div class="media-body" >'
                        html += '<h5>'+ result["users"][0].name + '</h5>'
                        // html += '<small class="text-muted">Active From 3 hours</small>';
                        html += '</div>'
                        html += '</div>'
                        html += '</div>'
                        html += '</li>'

                        $('#searched_users').append(html);
                    }
                    // document.getElementById("searched_users").appendChild(html);

                }//<-- RESULT
            });//<-- AJAX
        }

        function startConvo(id)
        {
            $('#user_id').val(id);

            var html = '<li class="media active" style="cursor: pointer;" onclick="fetchConvo('+id+')">';
            html += '<div class="media-body">';
            html += '<div class="media">'
            html += '<a class="pull-left" href="#">'
            html += '<img class="media-object img-circle" style="max-height:40px;" src="{{{ asset("public/img/default_user.png") }}}" />';
            html += '</a>';
            html += '<div class="media-body" >'
            html += '<h5>'+ $('#username').val() + '</h5>'
            // html += '<small class="text-muted">Active From 3 hours</small>';
            html += '</div>'
            html += '</div>'
            html += '</div>'
            html += '</li>'

            if(val == '')
            {
                $("#divUsers").empty();
            }
            $("#divConversations").empty();
            $('#divUsers').append(html);
        }

        function fetchConvo(id, name=null)
        {
            $('#user_id').val(id);

            $('#divChatHeader').empty();
            $('#divChatHeader').append(name);

            $.ajax({
                    type: "POST",
                    url: URL_BASE+"/api/fetch_conversation",
                    dataType: 'json',
                    contentType: "application/json",
                    Accept: 'application/json',
                    data: JSON.stringify({ "friend_id": id, "user_id": user}),
                    headers: {
                      // Set any custom headers here.
                      // If you set any non-simple headers, your server must include these
                      // headers in the 'Access-Control-Allow-Headers' response header.
                      'Accept': 'application/json',
                    },
                    success: function( result ){
                        console.log(result);

                        $("#divConversations").empty();

                        var messages = result["messages"];
                        for (var i = 0; i < messages.length; i++) {
                            var html = '<li class="media">';
                            html +='<div class="media-body">'
                            html +='<div class="media">'
                            html +='<a class="pull-left" href="#" style="width: 10%">'
                            html +='<img class="media-object img-circle " src="{{{ asset("public/img/default_user.png") }}}" style="width:100%" />'
                            html +='</a>'
                            html +='<div class="media-body" >'
                            html +=result["messages"][i].description;
                            html +='<br />'
                            html +='<small class="text-muted">Alex Deo | 23rd June at 5:00pm</small>'
                            html +='<hr />'
                            html +='</div>'
                            html +='</div>'

                            html +='</div>'
                            html +='</li>'

                            $('#divConversations').append(html);
                        }

                        

                    }//<-- RESULT
            });//<-- AJAX
        }

        function sendMessage()
        {
            $message = $('#message').val();
            // alert($message);
            $target_id = friendID; //$('#user_id').val();
            if(_.isEmpty(user))
            {
                window.location = URL_BASE+"/login";
            }

            alert("Logged user " + user);
            alert("Friend " + $target_id);

            $.ajax({
                    type: "POST",
                    url: URL_BASE+"/api/send_message",
                    dataType: 'json',
                    contentType: "application/json",
                    Accept: 'application/json',
                    data: JSON.stringify({ "message": $message, "user_id": user, "target_id": $target_id }),
                    headers: {
                      // Set any custom headers here.
                      // If you set any non-simple headers, your server must include these
                      // headers in the 'Access-Control-Allow-Headers' response header.
                      'Accept': 'application/json',
                    },
                   success: function( result ){
                    console.log(result);
                    if(result["status"] == "success")
                    {
                        var html = '<li class="media">';
                        html +='<div class="media-body">'
                        html +='<div class="">'
                        html +='<a class="pull-left" href="#" style="width: 10%">'
                        html +='<img class="media-object img-circle " src="{{{ asset("public/img/default_user.png") }}}" style="width:100%" />'
                        html +='</a>'
                        html +='<div class="media-body" >'
                        html +=$message;
                        html +='<br />'
                        html +='<small class="text-muted">Alex Deo | 23rd June at 5:00pm</small>'
                        html +='<hr />'
                        html +='</div>'
                        html +='</div>'

                        html +='</div>'
                        html +='</li>'

                        $('#divConversations').append(html);
                    }
                    else{
                        alert("Something went wrong, please try later.")
                    }
                    
                }//<-- RESULT
            });//<-- AJAX
        }

    </script>

    <script src="https://www.gstatic.com/firebasejs/6.2.4/firebase-app.js"></script>
  <script src="https://www.gstatic.com/firebasejs/6.2.4/firebase-messaging.js"></script>
  <script type="text/javascript">
    // Your web app's Firebase configuration
    var firebaseConfig = {
      apiKey: "AIzaSyA7lyZ_M0Jrrqd8dFRc4SAwu4Fnks9cV34",
      authDomain: "gomp-fa21f.firebaseapp.com",
      databaseURL: "https://gomp-fa21f.firebaseio.com",
      projectId: "gomp-fa21f",
      storageBucket: "",
      messagingSenderId: "246577318659",
      appId: "1:246577318659:web:873cd45d6ed5d989"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);

      // Retrieve Firebase Messaging object.
    messaging = firebase.messaging();

    
    messaging.usePublicVapidKey("BLf3KF70HfZiNWBALD4Htk4_VZ5nL1VcuRKJUqvWcpyxou39dUyHJ-xpbs2afN2TSqdY1EK8wCdLj2MeKS47kzo");

    // Get Instance ID token. Initially this makes a network call, once retrieved
    // subsequent calls to getToken will return from cache.
    messaging.getToken().then(function(currentToken) {
        console.log(currentToken);
      if (currentToken) {
        // sendTokenToServer(currentToken);
        // updateUIForPushEnabled(currentToken);
      } else {
        // Show permission request.
        console.log('No Instance ID token available. Request permission to generate one.');
        // Show permission UI.
        // updateUIForPushPermissionRequired();
        // setTokenSentToServer(false);
      }
    }).catch(function(err) {
      console.log('An error occurred while retrieving token. ', err);
      // showToken('Error retrieving Instance ID token. ', err);
      // setTokenSentToServer(false);
    });

    messaging.onMessage(function(payload) {
      console.log('Message received. ', payload);
      switch(payload.data.action_type)
      {
        case 'new_message':
            var html = '<li class="media">';
            html +='<div class="media-body">'
            html +='<div class="">'
            html +='<a class="pull-left" href="#" style="width: 10%">'
            html +='<img class="media-object img-circle " src="{{{ asset("public/img/default_user.png") }}}" style="width:100%" />'
            html +='</a>'
            html +='<div class="media-body" >'
            html +=payload.data.message;
            html +='<br />'
            html +='<small class="text-muted">Alex Deo | 23rd June at 5:00pm</small>'
            html +='<hr />'
            html +='</div>'
            html +='</div>'

            html +='</div>'
            html +='</li>'

            $('#divConversations').append(html);
        break;
      }
    });

    </script>

    <!-- <script type="text/javascript" src="{{url('/firebase-messaging-sw.js')}}"></script> -->

@endsection
