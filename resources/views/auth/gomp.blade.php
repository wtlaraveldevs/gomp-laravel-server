<?php $settings = App\Models\AdminSettings::first(); ?>
<!DOCTYPE html>
<html lang="en" style="overflow: hidden;">
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="@yield('description_custom'){{{ $settings->description }}}">
    <meta name="keywords" content="{{{ $settings->keywords }}}" />
    <link rel="shortcut icon" href="{{{ asset('public/img/favicon.png') }}}" />

	<title>@section('title')@show @if( isset( $settings->title ) ){{{$settings->title}}}@endif</title>

	@include('includes.css_general')

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Raleway:100,600' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	@yield('css')

	<!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

</head>
<body>

	<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/{{config('fb_app.lang')}}/sdk.js#xfbml=1&version=v2.8&appId={{config('fb_app.id')}}";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

	<div class="popout font-default"></div>
	<div class="wrap-loader">
		<i class="fa fa-cog fa-spin fa-3x fa-fw cog-loader"></i>
		<i class="fa fa-cog fa-spin fa-3x fa-fw cog-loader-small"></i>
	</div>
	
	<div class="navbar navbar-inverse navbar-px padding-top-10 padding-bottom-10">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">

          	 <?php if( isset( $totalNotify ) ) : ?>
        	<span class="notify"><?php echo $totalNotify; ?></span>
        	<?php endif; ?>

            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{ url('/') }}">
          	<img src="{{ asset('public/img/log.png') }}" class="logo" />
            <!-- <img src="{{ asset('public/img/watermark.png') }}" width="96" style="width: 30%" /> -->
          	</a>
        </div><!-- navbar-header -->

        <div class="navbar-collapse collapse">

        	<ul class="nav navbar-nav navbar-right">

        			@foreach( \App\Models\Pages::where('show_navbar', '1')->get() as $_page )
					 	<!-- <li @if(Request::is("page/$_page->slug")) class="active-navbar" @endif>
					 		<a class="text-uppercase font-default" href="{{ url('page',$_page->slug) }}">{{ $_page->title }}</a>
					 		</li> -->
					 	@endforeach

        		@if( Auth::check() )

		            <!-- <li>
			            <a class="text-uppercase font-default" href="{{url('register')}}">
			            </i> Pricing
			            </a>
		            </li> -->


        			<li class="dropdown">
			          <a href="javascript:void(0);" data-toggle="dropdown" class="userAvatar myprofile dropdown-toggle"> <!-- "{{ asset('public/avatar').'/'.Auth::user()->avatar }}"-->
			          		<img src="{{ asset('public/avatar').'/'.Auth::user()->avatar }}" alt="User" class="img-circle avatarUser" width="36" height="36" />
			          		<span class="title-dropdown font-default"><strong>{{ trans('users.my_profile') }}</strong></span>
			          		<i class="ion-chevron-down margin-lft5"></i>
			          	</a>

			          <!-- DROPDOWN MENU -->
			          <ul class="dropdown-menu arrow-up nav-session" role="menu" aria-labelledby="dropdownMenu4">
	          		 @if( Auth::user()->role == 'admin' )
	          		 	<li>
	          		 		<a href="{{ url('panel/admin') }}" class="text-overflow">
	          		 			<i class="icon-cogs myicon-right"></i> {{ trans('admin.admin') }}</a>
	          		 			</li>
                      <li role="separator" class="divider"></li>
	          		 			@endif

	          		 			<li>
	          		 			<a href="{{ url('dashboard') }}" class="text-overflow">
	          		 				<i class="icon icon-dashboard myicon-right"></i> {{ trans('admin.dashboard') }}
	          		 				</a>
	          		 			</li>

                      <li>
	          		 			<a href="{{ url('dashboard/campaigns') }}" class="text-overflow">
	          		 			<i class="ion ion-speakerphone myicon-right"></i> {{ trans('misc.campaigns') }}
	          		 				</a>
	          		 			</li>

	          		 			<li>
	          		 			<a href="{{ url('user/likes') }}" class="text-overflow">
	          		 				<i class="fa fa-heart myicon-right"></i> {{ trans('misc.likes') }}
	          		 				</a>
	          		 			</li>

	          		 			<li>
	          		 			<a href="{{ url('account') }}" class="text-overflow">
	          		 				<i class="glyphicon glyphicon-cog myicon-right"></i> {{ trans('users.account_settings') }}
	          		 				</a>
	          		 			</li>

	          		 		<li>
	          		 			<a href="{{ url('logout') }}" class="logout text-overflow">
	          		 				<i class="glyphicon glyphicon-log-out myicon-right"></i> {{ trans('users.logout') }}
	          		 			</a>
	          		 		</li>
	          		 	</ul>
	          		</li>
					@else

						<!-- <li>
							<a class="text-uppercase font-default" href="{{url('login')}}">{{trans('auth.login')}}</a>
						</li>

						<li>
							<a class="log-in custom-rounded text-uppercase font-default" href="{{url('register')}}">
							<i class="glyphicon glyphicon-user"></i> Register
							</a>
						</li> -->

            <!-- <li>
	            <a class="text-uppercase font-default" href="{{url('register')}}">
	            </i> Fees
	            </a>
            </li> -->

        	  @endif
          </ul>

         </div>
     </div>
 </div>

<div id="search">
    <button type="button" class="close">×</button>
    <form autocomplete="off" action="{{ url('search') }}" method="get">
        <input type="search" value="" name="q" id="btnItems" placeholder="{{trans('misc.search_query')}}" />
        <button type="submit" class="btn btn-lg no-shadow btn-trans custom-rounded btn_search"  id="btnSearch">{{trans('misc.search')}}</button>
    </form>
</div>

	<div class="jumbotron index-header jumbotron_set jumbotron-cover" style="height: 100%; padding: 100px 0;">  <!-- @if( Auth::check() ) session-active-cover @endif -->
		<!-- <h1 class="title-site txt-left" id="titleSite">{{$settings->welcome_text}}</h1> -->
      <div class="container wrap-jumbotron position-relative">
      	

      	
        <div class="row">
        	<div class="col-md-3">
        		<div style="background: #FCC438;width: 10%;padding-left: 2%;padding-right: 2%;padding-top: 1%;padding-bottom: 1%;border-radius: 6px; margin-bottom: 1%;
        		text-align: center;">
        		<a href="#search"  class="text-uppercase font-default" style="color: white;font-weight: bold;">
					Give
				</a>
        		</div>
        		<div style="background: #FCC438;width: 10%;padding-left: 2%;padding-right: 2%;padding-top: 1%;padding-bottom: 1%;border-radius: 6px; margin-bottom: 1%;text-align: center;">
        		<a class="text-uppercase font-default" href="{{url('create/campaign')}}" style="color: white;font-weight: bold;">
					Receive
				</a>
        		</div>
        		<div style="background: #FCC438;width: 10%;padding-left: 2%;padding-right: 2%;padding-top: 1%;padding-bottom: 1%;border-radius: 6px; margin-bottom: 1%;text-align: center;">
        		<a class="log-in custom-rounded text-uppercase font-default" href="{{url('register')}}" style="color: white;font-weight: bold;">
					Send a message
				</a>
        		</div>
        		<div style="background: #FCC438;width: 10%;padding-left: 2%;padding-right: 2%;padding-top: 1%;padding-bottom: 1%;border-radius: 6px; margin-bottom: 1%;text-align: center;">
        		<a class="log-in custom-rounded text-uppercase font-default" href="{{url('register')}}" style="color: white;font-weight: bold;">
					Register
				</a>
        		</div>
        		<div style="background: #FCC438;width: 10%;padding-left: 2%;padding-right: 2%;padding-top: 1%;padding-bottom: 1%;border-radius: 6px; margin-bottom: 1%;text-align: center;">
        		<a class="log-in custom-rounded text-uppercase font-default" href="{{url('register')}}" style="color: white;font-weight: bold;">
					Get my money
				</a>
        		</div>
        		<div style="background: #FCC438;width: 10%;padding-left: 2%;padding-right: 2%;padding-top: 1%;padding-bottom: 1%;border-radius: 6px; margin-bottom: 1%;text-align: center;">
        		<a href="#search"  class="text-uppercase font-default" style="color: white;font-weight: bold;">
					Search
				</a>
        		</div>
        	<!-- <div style="background: #FCC438;width: 10%;padding-left: 2%;padding-right: 2%;padding-top: 1%;padding-bottom: 1%;border-radius: 6px; margin-bottom: 1%;text-align: center;">
        		<a class="log-in custom-rounded text-uppercase font-default" href="{{url('register')}}" style="color: white;font-weight: bold;">
					Thank you
				</a>
        	</div> -->
        		<div style="background: #FCC438;width: 10%;padding-left: 2%;padding-right: 2%;padding-top: 1%;padding-bottom: 1%;border-radius: 6px; margin-bottom: 1%;text-align: center;">
        		<a class="log-in custom-rounded text-uppercase font-default" href="{{url('register')}}" style="color: white;font-weight: bold;">
					Fee
				</a>
        		</div>
        	</div>

        	<div class="col-md-6">
        		<div style="border:1px solid black">
        			<!-- <div style="background-image: url('/public/img/valentines-day-gifts_112225.jpg') no-repeat;height: 300px;"> -->
        				<img src="../public/img/valentines-day-gifts_112225.jpg" style= 'height:100%: width:50%'>
        				
					
        		</div>

        	</div>

        	<div class="col-md-3">
        		<div style="background: #FCC438;width: 10%;padding-left: 2%;padding-right: 2%;padding-top: 1%;padding-bottom: 1%;border-radius: 6px; margin-bottom: 1%;text-align: center;">
        							<a class="log-in custom-rounded text-uppercase font-default" href="{{url('buygomp')}}" style="color: white;font-weight: bold;">
									Paypal
								</a>
        				</div>
        				<div style="background: #FCC438;width: 10%;padding-left: 2%;padding-right: 2%;padding-top: 1%;padding-bottom: 1%;border-radius: 6px; margin-bottom: 1%;text-align: center;">
        							<a class="log-in custom-rounded text-uppercase font-default" href="{{url('https://www.paypal.com/cgi-bin/webscr')}}" style="color: white;font-weight: bold;">
									Stripe
								</a>
        				</div>

        	</div>



        </div>


        <!-- <p class="subtitle-site txt-left"><strong>{{$settings->welcome_subtitle}}</strong></p> -->
      </div><!-- container wrap-jumbotron -->
	</div><!-- jumbotron -->

		@include('includes.javascript_general')

	<script src="{{ asset('public/plugins/jquery.counterup/waypoints.min.js') }}"></script>
	<script src="{{ asset('public/plugins/jquery.counterup/jquery.counterup.min.js') }}"></script>

		<script type="text/javascript">

		$(document).on('click','#campaigns .loadPaginator', function(r){
			r.preventDefault();
			 $(this).remove();
			 $('.loadMoreSpin').remove();
					$('<div class="col-xs-12 loadMoreSpin"><a class="list-group-item text-center"><i class="fa fa-circle-o-notch fa-spin fa-1x fa-fw"></i></a></div>').appendTo( "#campaigns" );

					var page = $(this).attr('href').split('page=')[1];
					$.ajax({
						url: '{{ url("ajax/campaigns") }}?page=' + page
					}).done(function(data){
						if( data ) {
							$('.loadMoreSpin').remove();

							$( data ).appendTo( "#campaigns" );
						} else {
							bootbox.alert( "{{trans('misc.error')}}" );
						}
						//<**** - Tooltip
					});
			});

		jQuery(document).ready(function( $ ) {
			$('.counter').counterUp({
			delay: 10, // the delay time in ms
			time: 1000 // the speed time in ms
			});
		});

		 @if (session('success_verify'))
    		swal({
    			title: "{{ trans('misc.welcome') }}",
    			text: "{{ trans('users.account_validated') }}",
    			type: "success",
    			confirmButtonText: "{{ trans('users.ok') }}"
    			});
   		 @endif

   		 @if (session('error_verify'))
    		swal({
    			title: "{{ trans('misc.error_oops') }}",
    			text: "{{ trans('users.code_not_valid') }}",
    			type: "error",
    			confirmButtonText: "{{ trans('users.ok') }}"
    			});
   		 @endif

		</script>

	<script type="text/javascript">

	Cookies.set('cookieBanner');

		$(document).ready(function() {
    if (Cookies('cookieBanner'));
    else {
    	$('.showBanner').fadeIn();
        $("#close-banner").click(function() {
            $(".showBanner").slideUp(50);
            Cookies('cookieBanner', true);
        });
    }
});

	</script>



</body>
</html>
