<?php
	// Route::post('/showTableProduct','rn\PlatyPOSOpenBillController@showTableProduct');
	// Route::post('/payTablebill','rn\PlatyPOSOpenBillController@payTablebill');
	// Route::post('/removeOpenbillProduct','rn\PlatyPOSOpenBillController@removeOpenbillProduct');
	// Route::post('/removeTableNumber','rn\PlatyPOSOpenBillController@removeTableNumber');
	Route::group(['middleware' => 'api'], function() {
		Route::post('/login','RN\AuthController@authenticate');
		Route::post('/register','RN\AuthController@register');

		// Stripe Account
		Route::post('activate/stripe','UserController@getStripeAccount');
		/*
		 |
		 |-----------------------------------
		 | Payment Campaign
		 |--------- -------------------------
		 */
		Route::post('/webf','UserController@store_ftoken');
		/*
		 |
		 |-----------------------------------
		 | Purchasing GOMP
		 |-----------------------------------
		 */
		Route::post('purchase','UserController@buyGomp');
		Route::post('calculateFee','UserController@calculateFee');

		/*
		 |
		 |-----------------------------------
		 | Conversation AJAX calls
		 |--------- -------------------------
		 */
		Route::post('/search_users','ConversationController@searchUsers');
		Route::post('/send_message','ConversationController@sendMessage');
		Route::post('/fetch_conversation','ConversationController@getConversation');
	});
	// Route::post('/updatePax','rn\PlatyPOSOpenBillController@update_pax');
	// Route::post('/getPax','rn\PlatyPOSOpenBillController@get_pax');

?>