<?php
	Route::group(['middleware' => 'api'], function() {
		Route::post('/search', 'RN\GlobalController@search');
		Route::post('/searchPot', 'RN\GlobalController@searchPot');
	});

?>