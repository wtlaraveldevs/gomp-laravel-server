<?php $settings = App\Models\AdminSettings::first(); ?>
<!DOCTYPE html>
<html lang="en" style="overflow: hidden;">
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token(), false); ?>">
    <meta name="description" content="<?php echo $__env->yieldContent('description_custom'); ?><?php echo e($settings->description); ?>">
    <meta name="keywords" content="<?php echo e($settings->keywords); ?>" />
    <link rel="shortcut icon" href="<?php echo e(asset('public/img/favicon.png')); ?>" />

	<title><?php $__env->startSection('title'); ?><?php echo $__env->yieldSection(); ?> <?php if( isset( $settings->title ) ): ?><?php echo e($settings->title); ?><?php endif; ?></title>

	<?php echo $__env->make('includes.css_general', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Raleway:100,600' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<?php echo $__env->yieldContent('css'); ?>

	<!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

</head>
<body>

	<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/<?php echo e(config('fb_app.lang'), false); ?>/sdk.js#xfbml=1&version=v2.8&appId=<?php echo e(config('fb_app.id'), false); ?>";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

	<div class="popout font-default"></div>
	<div class="wrap-loader">
		<i class="fa fa-cog fa-spin fa-3x fa-fw cog-loader"></i>
		<i class="fa fa-cog fa-spin fa-3x fa-fw cog-loader-small"></i>
	</div>
	
	<div class="navbar navbar-inverse navbar-px padding-top-10 padding-bottom-10">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">

          	 <?php if( isset( $totalNotify ) ) : ?>
        	<span class="notify"><?php echo $totalNotify; ?></span>
        	<?php endif; ?>

            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo e(url('/'), false); ?>">
          	<img src="<?php echo e(asset('public/img/log.png'), false); ?>" class="logo" />
            <!-- <img src="<?php echo e(asset('public/img/watermark.png'), false); ?>" width="96" style="width: 30%" /> -->
          	</a>
        </div><!-- navbar-header -->

        <div class="navbar-collapse collapse">

        	<ul class="nav navbar-nav navbar-right">

        			<?php $__currentLoopData = \App\Models\Pages::where('show_navbar', '1')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $_page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					 	<!-- <li <?php if(Request::is("page/$_page->slug")): ?> class="active-navbar" <?php endif; ?>>
					 		<a class="text-uppercase font-default" href="<?php echo e(url('page',$_page->slug), false); ?>"><?php echo e($_page->title, false); ?></a>
					 		</li> -->
					 	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        		<?php if( Auth::check() ): ?>

		            <li>
			            <a class="text-uppercase font-default" href="<?php echo e(url('register'), false); ?>">
			            </i> Pricing <!-- <?php echo e(trans('auth.sign_up'), false); ?> -->
			            </a>
		            </li>


        			<li class="dropdown">
			          <a href="javascript:void(0);" data-toggle="dropdown" class="userAvatar myprofile dropdown-toggle">
			          		<img src="<?php echo e(asset('public/avatar').'/'.Auth::user()->avatar, false); ?>" alt="User" class="img-circle avatarUser" width="21" height="21" />
			          		<span class="title-dropdown font-default"><strong><?php echo e(trans('users.my_profile'), false); ?></strong></span>
			          		<i class="ion-chevron-down margin-lft5"></i>
			          	</a>

			          <!-- DROPDOWN MENU -->
			          <ul class="dropdown-menu arrow-up nav-session" role="menu" aria-labelledby="dropdownMenu4">
	          		 <?php if( Auth::user()->role == 'admin' ): ?>
	          		 	<li>
	          		 		<a href="<?php echo e(url('panel/admin'), false); ?>" class="text-overflow">
	          		 			<i class="icon-cogs myicon-right"></i> <?php echo e(trans('admin.admin'), false); ?></a>
	          		 			</li>
                      <li role="separator" class="divider"></li>
	          		 			<?php endif; ?>

	          		 			<li>
	          		 			<a href="<?php echo e(url('dashboard'), false); ?>" class="text-overflow">
	          		 				<i class="icon icon-dashboard myicon-right"></i> <?php echo e(trans('admin.dashboard'), false); ?>

	          		 				</a>
	          		 			</li>

                      <li>
	          		 			<a href="<?php echo e(url('dashboard/campaigns'), false); ?>" class="text-overflow">
	          		 			<i class="ion ion-speakerphone myicon-right"></i> <?php echo e(trans('misc.campaigns'), false); ?>

	          		 				</a>
	          		 			</li>

	          		 			<li>
	          		 			<a href="<?php echo e(url('user/likes'), false); ?>" class="text-overflow">
	          		 				<i class="fa fa-heart myicon-right"></i> <?php echo e(trans('misc.likes'), false); ?>

	          		 				</a>
	          		 			</li>

	          		 			<li>
	          		 			<a href="<?php echo e(url('account'), false); ?>" class="text-overflow">
	          		 				<i class="glyphicon glyphicon-cog myicon-right"></i> <?php echo e(trans('users.account_settings'), false); ?>

	          		 				</a>
	          		 			</li>

	          		 		<li>
	          		 			<a href="<?php echo e(url('logout'), false); ?>" class="logout text-overflow">
	          		 				<i class="glyphicon glyphicon-log-out myicon-right"></i> <?php echo e(trans('users.logout'), false); ?>

	          		 			</a>
	          		 		</li>
	          		 	</ul>
	          		</li>
					<?php else: ?>

						<!-- <li>
							<a class="text-uppercase font-default" href="<?php echo e(url('login'), false); ?>"><?php echo e(trans('auth.login'), false); ?></a>
						</li>

						<li>
							<a class="log-in custom-rounded text-uppercase font-default" href="<?php echo e(url('register'), false); ?>">
							<i class="glyphicon glyphicon-user"></i> Register
							</a>
						</li> -->

            <!-- <li>
	            <a class="text-uppercase font-default" href="<?php echo e(url('register'), false); ?>">
	            </i> Fees
	            </a>
            </li> -->

        	  <?php endif; ?>
          </ul>

         </div>
     </div>
 </div>

<div id="search">
    <button type="button" class="close">×</button>
    <form autocomplete="off" action="<?php echo e(url('search'), false); ?>" method="get">
        <input type="search" value="" name="q" id="btnItems" placeholder="<?php echo e(trans('misc.search_query'), false); ?>" />
        <button type="submit" class="btn btn-lg no-shadow btn-trans custom-rounded btn_search"  id="btnSearch"><?php echo e(trans('misc.search'), false); ?></button>
    </form>
</div>

	<div class="jumbotron index-header jumbotron_set jumbotron-cover" style="height: 100%; padding: 100px 0;">  <!-- <?php if( Auth::check() ): ?> session-active-cover <?php endif; ?> -->
		<!-- <h1 class="title-site txt-left" id="titleSite"><?php echo e($settings->welcome_text, false); ?></h1> -->
      <div class="container wrap-jumbotron position-relative">
        <div class="row">
        	<div style="background: #FCC438;width: 10%;padding-left: 2%;padding-right: 2%;padding-top: 1%;padding-bottom: 1%;border-radius: 6px; margin-bottom: 1%;
        		text-align: center;">
        		<a href="#search"  class="text-uppercase font-default" style="color: white;font-weight: bold;">
					Give
				</a>
        	</div>
        	<div style="background: #FCC438;width: 10%;padding-left: 2%;padding-right: 2%;padding-top: 1%;padding-bottom: 1%;border-radius: 6px; margin-bottom: 1%;text-align: center;">
        		<a class="text-uppercase font-default" href="<?php echo e(url('create/campaign'), false); ?>" style="color: white;font-weight: bold;">
					Receive
				</a>
        	</div>
        	<div style="background: #FCC438;width: 10%;padding-left: 2%;padding-right: 2%;padding-top: 1%;padding-bottom: 1%;border-radius: 6px; margin-bottom: 1%;text-align: center;">
        		<a class="log-in custom-rounded text-uppercase font-default" href="<?php echo e(url('register'), false); ?>" style="color: white;font-weight: bold;">
					Send a message
				</a>
        	</div>
        	<div style="background: #FCC438;width: 10%;padding-left: 2%;padding-right: 2%;padding-top: 1%;padding-bottom: 1%;border-radius: 6px; margin-bottom: 1%;text-align: center;">
        		<a class="log-in custom-rounded text-uppercase font-default" href="<?php echo e(url('register'), false); ?>" style="color: white;font-weight: bold;">
					Register
				</a>
        	</div>
        	<div style="background: #FCC438;width: 10%;padding-left: 2%;padding-right: 2%;padding-top: 1%;padding-bottom: 1%;border-radius: 6px; margin-bottom: 1%;text-align: center;">
        		<a class="log-in custom-rounded text-uppercase font-default" href="<?php echo e(url('register'), false); ?>" style="color: white;font-weight: bold;">
					Get my money
				</a>
        	</div>
        	<div style="background: #FCC438;width: 10%;padding-left: 2%;padding-right: 2%;padding-top: 1%;padding-bottom: 1%;border-radius: 6px; margin-bottom: 1%;text-align: center;">
        		<a class="log-in custom-rounded text-uppercase font-default" href="<?php echo e(url('register'), false); ?>" style="color: white;font-weight: bold;">
					Search
				</a>
        	</div>
        	<div style="background: #FCC438;width: 10%;padding-left: 2%;padding-right: 2%;padding-top: 1%;padding-bottom: 1%;border-radius: 6px; margin-bottom: 1%;text-align: center;">
        		<a class="log-in custom-rounded text-uppercase font-default" href="<?php echo e(url('register'), false); ?>" style="color: white;font-weight: bold;">
					Thank you
				</a>
        	</div>
        	<div style="background: #FCC438;width: 10%;padding-left: 2%;padding-right: 2%;padding-top: 1%;padding-bottom: 1%;border-radius: 6px; margin-bottom: 1%;text-align: center;">
        		<a class="log-in custom-rounded text-uppercase font-default" href="<?php echo e(url('register'), false); ?>" style="color: white;font-weight: bold;">
					Fee
				</a>
        	</div>
        </div>
        <!-- <p class="subtitle-site txt-left"><strong><?php echo e($settings->welcome_subtitle, false); ?></strong></p> -->
      </div><!-- container wrap-jumbotron -->
	</div><!-- jumbotron -->

		<?php echo $__env->make('includes.javascript_general', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<script src="<?php echo e(asset('public/plugins/jquery.counterup/waypoints.min.js'), false); ?>"></script>
	<script src="<?php echo e(asset('public/plugins/jquery.counterup/jquery.counterup.min.js'), false); ?>"></script>

		<script type="text/javascript">

		$(document).on('click','#campaigns .loadPaginator', function(r){
			r.preventDefault();
			 $(this).remove();
			 $('.loadMoreSpin').remove();
					$('<div class="col-xs-12 loadMoreSpin"><a class="list-group-item text-center"><i class="fa fa-circle-o-notch fa-spin fa-1x fa-fw"></i></a></div>').appendTo( "#campaigns" );

					var page = $(this).attr('href').split('page=')[1];
					$.ajax({
						url: '<?php echo e(url("ajax/campaigns"), false); ?>?page=' + page
					}).done(function(data){
						if( data ) {
							$('.loadMoreSpin').remove();

							$( data ).appendTo( "#campaigns" );
						} else {
							bootbox.alert( "<?php echo e(trans('misc.error'), false); ?>" );
						}
						//<**** - Tooltip
					});
			});

		jQuery(document).ready(function( $ ) {
			$('.counter').counterUp({
			delay: 10, // the delay time in ms
			time: 1000 // the speed time in ms
			});
		});

		 <?php if(session('success_verify')): ?>
    		swal({
    			title: "<?php echo e(trans('misc.welcome'), false); ?>",
    			text: "<?php echo e(trans('users.account_validated'), false); ?>",
    			type: "success",
    			confirmButtonText: "<?php echo e(trans('users.ok'), false); ?>"
    			});
   		 <?php endif; ?>

   		 <?php if(session('error_verify')): ?>
    		swal({
    			title: "<?php echo e(trans('misc.error_oops'), false); ?>",
    			text: "<?php echo e(trans('users.code_not_valid'), false); ?>",
    			type: "error",
    			confirmButtonText: "<?php echo e(trans('users.ok'), false); ?>"
    			});
   		 <?php endif; ?>

		</script>

	<script type="text/javascript">

	Cookies.set('cookieBanner');

		$(document).ready(function() {
    if (Cookies('cookieBanner'));
    else {
    	$('.showBanner').fadeIn();
        $("#close-banner").click(function() {
            $(".showBanner").slideUp(50);
            Cookies('cookieBanner', true);
        });
    }
});
	</script>

</body>
</html>
