<!-- Bootstrap core JavaScript 
    ================================================== -->    
    <script src="<?php echo e(asset('public/plugins/jQuery/jQuery-2.1.4.min.js'), false); ?>"></script>
    <script src="<?php echo e(asset('public/js/jquery.easing.1.3.js'), false); ?>"></script>
    <script src="<?php echo e(asset('public/js/jquery-ui-1.10.3.custom.min.js'), false); ?>"></script>
    <script src="<?php echo e(asset('public/js/jquery.ui.touch-punch.min.js'), false); ?>"></script>
    <script src="<?php echo e(asset('public/bootstrap/js/bootstrap.min.js'), false); ?>"></script>
    <script src="<?php echo e(asset('public/js/jquery.autosize.min.js'), false); ?>"></script>
    <script src="<?php echo e(asset('public/js/jqueryTimeago.js'), false); ?>"></script>
    <script src="<?php echo e(asset('public/js/bootbox.min.js'), false); ?>"></script>
    <script src="<?php echo e(asset('public/js/count.js'), false); ?>"></script>
    <script src="<?php echo e(asset('public/js/functions.js'), false); ?>"></script>
    <script src="<?php echo e(asset('public/js/jquery.form.js'), false); ?>"></script>
    <script src="<?php echo e(asset('public/plugins/sweetalert/sweetalert.min.js'), false); ?>"></script>
    <script src="<?php echo e(asset('public/js/holder.min.js'), false); ?>"></script>
    
    <script src="<?php echo e(asset('public/js/cookies.js'), false); ?>"></script>
    
    
    
    