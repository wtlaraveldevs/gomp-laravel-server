<ul class="nav nav-pills nav-stacked">

	<li class="margin-bottom-5">
		<!-- **** list-group-item **** -->
	<a href="<?php echo e(url('dashboard'), false); ?>" class="list-group-item">
		<i class="icon icon-dashboard myicon-right"></i> <?php echo e(trans('admin.dashboard'), false); ?>

		</a> <!-- **** ./ list-group-item **** -->
	</li>

			<li class="margin-bottom-5">
				<!-- **** list-group-item **** -->
		  <a href="<?php echo e(url('account'), false); ?>" class="list-group-item <?php if(Request::is('account')): ?>active <?php endif; ?>">
		  	<i class="icon icon-pencil myicon-right"></i> <?php echo e(trans('users.account_settings'), false); ?>

		  	</a> <!-- **** ./ list-group-item **** -->
			</li>

		  	<li class="margin-bottom-5">
		  		<!-- **** list-group-item **** -->
		  <a href="<?php echo e(url('account/password'), false); ?>" class="list-group-item <?php if(Request::is('account/password')): ?>active <?php endif; ?>">
		  	<i class="icon icon-lock myicon-right"></i> <?php echo e(trans('auth.password'), false); ?>

		  	</a> <!-- **** ./ list-group-item **** -->
		  	</li>
		</ul>
