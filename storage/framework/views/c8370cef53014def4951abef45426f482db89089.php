<?php
$categoriesMenu = App\Models\Categories::where('mode','on')->orderBy('name')->take(6)->get();
$categoriesTotal = App\Models\Categories::count();
?>

<div class="btn-block text-center showBanner padding-top-10 padding-bottom-10" style="display:none;"><?php echo e(trans('misc.cookies_text'), false); ?> <button class="btn btn-sm btn-success" id="close-banner"><?php echo e(trans('misc.agree'), false); ?></button></div>

<?php if( Auth::check() && Auth::user()->status == 'pending' ): ?>
<div class="btn-block margin-top-zero text-center confirmEmail"><?php echo e(trans('misc.confirm_email'), false); ?> <strong><?php echo e(Auth::user()->email, false); ?></strong></div>
<?php endif; ?>
<div class="navbar navbar-inverse navbar-px padding-top-10 padding-bottom-10">
      <div class="container"> 
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">

          	 <?php if( isset( $totalNotify ) ) : ?>
        	<span class="notify"><?php echo $totalNotify; ?></span>
        	<?php endif; ?>

            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand logo" href="<?php echo e(url('/'), false); ?>" >
            <p style="color: white; font-size: 30px; padding-top: 1%">G<img src="<?php echo e(asset('public/img/logo.png'), false); ?>" class="logo" style="width: 7%; margin-top: -1%"/>MP</p>
            </a>
        </div><!-- navbar-header -->

        <div class="navbar-collapse collapse">

        	<ul class="nav navbar-nav navbar-right">

          		<!-- <li>
          			<a href="#search"  class="text-uppercase font-default"> -->
        				<!-- <i class="glyphicon glyphicon-search">
                </i>  -->
                <!-- <span class="title-dropdown font-default"> -->
                  <!-- <strong><?php echo e(trans('misc.search'), false); ?></strong> -->
                  <!-- <strong>Search</strong> -->
                <!-- </span> -->
        				<!-- </a>
          		</li> -->

              <!-- <li>
                <a class="text-uppercase font-default" href="<?php echo e(url('register'), false); ?>">
                  Give
                </a>
              </li> -->

              <!-- <li>
                <a class="text-uppercase font-default" href="<?php echo e(url('create/campaign'), false); ?>">
                  Receive
                </a>
              </li> -->

          		<!-- <li class="dropdown">
        				<a class="text-uppercase font-default" data-toggle="dropdown" href="javascript:void(0);"><?php echo e(trans('misc.campaigns'), false); ?> <i class="ion-chevron-down margin-lft5"></i></a>
 -->
                <!-- DROPDOWN MENU -->
        				<!-- <ul class="dropdown-menu arrow-up" role="menu" aria-labelledby="dropdownMenu2">
                  <li><a class="text-overflow" href="<?php echo e(url('campaigns/featured'), false); ?>"><?php echo e(trans('misc.featured'), false); ?></a></li>
        					<li><a class="text-overflow" href="<?php echo e(url('/'), false); ?>"><?php echo e(trans('misc.latest'), false); ?></a></li>
        				</ul> -->
                <!-- DROPDOWN MENU -->
        			<!-- </li> -->

        		<!-- <?php if( $categoriesTotal != 0 ): ?>
        		<li class="dropdown">
        			<a href="javascript:void(0);" data-toggle="dropdown" class="text-uppercase font-default">GOMP Type
        				<i class="ion-chevron-down margin-lft5"></i>
        				</a>

        				<ul class="dropdown-menu arrow-up" role="menu" aria-labelledby="dropdownMenu2">
        				<?php $__currentLoopData = $categoriesMenu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        					<li <?php if(Request::path() == "category/$category->slug"): ?> class="active" <?php endif; ?>>
        						<a href="<?php echo e(url('category'), false); ?>/<?php echo e($category->slug, false); ?>" class="text-overflow">
        						<?php echo e($category->name, false); ?>

        							</a>
        					</li>
        					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        					<?php if( $categoriesTotal > 6 ): ?>
			        		<li><a href="<?php echo e(url('categories'), false); ?>">
			        			<strong><?php echo e(trans('misc.view_all'), false); ?> <i class="fa fa-long-arrow-right"></i></strong>
			        		</a></li>
			        		<?php endif; ?>
        				</ul>

        		</li>
        		<?php endif; ?> -->

        			<!-- <?php $__currentLoopData = \App\Models\Pages::where('show_navbar', '1')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $_page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					 	<li <?php if(Request::is("page/$_page->slug")): ?> class="active-navbar" <?php endif; ?>>
					 		<a class="text-uppercase font-default" href="<?php echo e(url('page',$_page->slug), false); ?>"><?php echo e($_page->title, false); ?></a>
					 		</li>
					 	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> -->

        		<!-- <?php if( Auth::check() ): ?> -->

            <!-- <li>
            <a class="text-uppercase font-default" href="<?php echo e(url('register'), false); ?>">
            </i> Pricing
            </a>
            </li> -->


        			<li class="dropdown">
			          <a href="javascript:void(0);" data-toggle="dropdown" class="userAvatar myprofile dropdown-toggle">
			          		<img src="<?php echo e(asset('public/avatar').'/'.Auth::user()->avatar, false); ?>" alt="User" class="img-circle avatarUser" width="21" height="21" />
			          		<span class="title-dropdown font-default"><strong><?php echo e(trans('users.my_profile'), false); ?></strong></span>
			          		<i class="ion-chevron-down margin-lft5"></i>
			          	</a>

			          <!-- DROPDOWN MENU -->
			          <ul class="dropdown-menu arrow-up nav-session" role="menu" aria-labelledby="dropdownMenu4">
	          		 <?php if( Auth::user()->role == 'admin' ): ?>
	          		 	<li>
	          		 		<a href="<?php echo e(url('panel/admin'), false); ?>" class="text-overflow">
	          		 			<i class="icon-cogs myicon-right"></i> <?php echo e(trans('admin.admin'), false); ?></a>
	          		 			</li>
                      <li role="separator" class="divider"></li>
	          		 			<?php endif; ?>

	          		 			<li>
	          		 			<a href="<?php echo e(url('dashboard'), false); ?>" class="text-overflow">
	          		 				<i class="icon icon-dashboard myicon-right"></i> <?php echo e(trans('admin.dashboard'), false); ?>

	          		 				</a>
	          		 			</li>

                      <li>
	          		 			<a href="<?php echo e(url('dashboard/campaigns'), false); ?>" class="text-overflow">
	          		 			<i class="ion ion-speakerphone myicon-right"></i> <?php echo e(trans('misc.campaigns'), false); ?>

	          		 				</a>
	          		 			</li>

	          		 			<li>
	          		 			<a href="<?php echo e(url('user/likes'), false); ?>" class="text-overflow">
	          		 				<i class="fa fa-heart myicon-right"></i> <?php echo e(trans('misc.likes'), false); ?>

	          		 				</a>
	          		 			</li>

	          		 			<li>
	          		 			<a href="<?php echo e(url('account'), false); ?>" class="text-overflow">
	          		 				<i class="glyphicon glyphicon-cog myicon-right"></i> <?php echo e(trans('users.account_settings'), false); ?>

	          		 				</a>
	          		 			</li>

	          		 		<li>
	          		 			<a href="<?php echo e(url('logout'), false); ?>" class="logout text-overflow">
	          		 				<i class="glyphicon glyphicon-log-out myicon-right"></i> <?php echo e(trans('users.logout'), false); ?>

	          		 			</a>
	          		 		</li>
	          		 	</ul><!-- DROPDOWN MENU -->
	          		</li>

	       <!-- <li><a class="log-in custom-rounded" href="<?php echo e(url('create/campaign'), false); ?>" title="<?php echo e(trans('misc.create_campaign'), false); ?>">
					<i class="glyphicon glyphicon-edit"></i> <strong><?php echo e(trans('misc.create_campaign'), false); ?></strong></a>
					</li> -->

					<?php else: ?>

					<!-- <li><a class="text-uppercase font-default" href="<?php echo e(url('login'), false); ?>"><?php echo e(trans('auth.login'), false); ?></a></li> -->

					<!-- <li>
						<a class="log-in custom-rounded text-uppercase font-default" href="<?php echo e(url('register'), false); ?>">
						<i class="glyphicon glyphicon-user"></i> Register
						</a>
						</li>

            <li>
            <a class="text-uppercase font-default" href="<?php echo e(url('register'), false); ?>">
            </i> Fees 
            </a>
            </li> -->

        	  <?php endif; ?>
          </ul>

         </div><!--/.navbar-collapse -->
     </div>
 </div>

<div id="search">
    <button type="button" class="close">×</button>
    <form autocomplete="off" action="<?php echo e(url('search'), false); ?>" method="get">
        <input type="search" value="" name="q" id="btnItems" placeholder="<?php echo e(trans('misc.search_query'), false); ?>" />
        <button type="submit" class="btn btn-lg no-shadow btn-trans custom-rounded btn_search"  id="btnSearch"><?php echo e(trans('misc.search'), false); ?></button>
    </form>
</div>
