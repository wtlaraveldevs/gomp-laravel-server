<?php $__env->startSection('title'); ?><?php echo e(trans('misc.add_reward').' - ', false); ?><?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?><link rel="stylesheet" href="<?php echo e(asset('public/plugins/datepicker/datepicker3.css'), false); ?>" rel="stylesheet" type="text/css"><?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

 <div class="jumbotron md index-header jumbotron_set jumbotron-cover">
      <div class="container wrap-jumbotron position-relative">
      	<h2 class="title-site"><?php echo e(trans('misc.add_reward'), false); ?></h2>
      	<p class="subtitle-site"><strong><?php echo e($data->title, false); ?></strong></p>
      </div>
    </div>

<div class="container margin-bottom-40 padding-top-40">
	<div class="row">

	<!-- col-md-8 -->
	<div class="col-md-12">
		<div class="wrap-center center-block">

			<?php echo $__env->make('errors.errors-forms', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <!-- form start -->
    <form method="POST" action="" id="formUpdateCampaign" enctype="multipart/form-data">

    	<input type="hidden" name="_token" value="<?php echo e(csrf_token(), false); ?>">
    	<input type="hidden" name="id" value="<?php echo e($data->id, false); ?>">

      <!-- Start Form Group -->
      <div class="form-group">
        <label><?php echo e(trans('misc.title'), false); ?></label>
          <input type="text" value=""  name="title" autocomplete="off" class="form-control" placeholder="<?php echo e(trans('misc.title'), false); ?>">
        <span class="help-block"><?php echo e(trans('misc.title_reward_desc'), false); ?></span>

      </div><!-- /.form-group-->

      <div class="form-group">
        <label><?php echo e(trans('misc.amount'), false); ?></label>
        <div class="input-group">
          <div class="input-group-addon addon-dollar"><?php echo e($settings->currency_symbol, false); ?></div>
          <input type="number" min="1" class="form-control onlyNumber" name="amount" autocomplete="off" value="<?php echo e(old('amount'), false); ?>" placeholder="<?php echo e(trans('misc.amount'), false); ?>">
        </div>
      </div>

        <div class="form-group">
            <label><?php echo e(trans('misc.description'), false); ?></label>
            	<textarea name="description" rows="4" id="description" class="form-control" placeholder="<?php echo e(trans('misc.description'), false); ?>"></textarea>
          </div>

          <div class="form-group">
            <label><?php echo e(trans('misc.quantity'), false); ?></label>
            <div class="input-group">
              <div class="input-group-addon addon-dollar"><i class="icon-copy"></i></div>
              <input type="number" min="1" class="form-control onlyNumber" name="quantity" autocomplete="off" value="<?php echo e(old('quantity'), false); ?>" placeholder="<?php echo e(trans('misc.quantity'), false); ?>">
            </div>
          </div>

          <!-- Start Form Group -->
          <div class="form-group">
            <label><?php echo e(trans('misc.delivery'), false); ?></label>
            <div class="input-group">
              <div class="input-group-addon addon-dollar"><i class="fa fa-calendar"></i></div>
              <input type="text" value="<?php echo e(old('delivery'), false); ?>" id="datepicker" name="delivery" autocomplete="off" class="form-control" placeholder="<?php echo e(trans('misc.delivery'), false); ?>">
            </div>
            <span class="help-block"><?php echo e(trans('misc.delivery_desc'), false); ?></span>

          </div><!-- /.form-group-->

            <!-- Alert -->
            <div class="alert alert-danger display-none" id="dangerAlert">
							<ul class="list-unstyled" id="showErrors"></ul>
						</div><!-- Alert -->

            <div class="alert alert-success display-none" id="successAlert">
                    <ul class="list-unstyled" id="success_update">
                      <li><?php echo e(trans('misc.success_add_rewards'), false); ?>

                        <a href="<?php echo e(url('campaign',$data->id), false); ?>" class="btn btn-default btn-sm"><?php echo e(trans('misc.view_campaign'), false); ?></a>
                      </li>
                    </ul>
                  </div><!-- Alert -->

            <div class="box-footer">
            	<hr />
              <button type="submit" id="buttonUpdateForm" class="btn btn-block btn-lg btn-main custom-rounded"><?php echo e(trans('auth.send'), false); ?></button>
              <div class="btn-block text-center margin-top-20">
           		<a href="<?php echo e(url('campaign',$data->id), false); ?>" class="text-muted">
           		<i class="fa fa-long-arrow-left"></i>	<?php echo e(trans('auth.back'), false); ?></a>
           </div>
            </div><!-- /.box-footer -->
          </form>
        </div><!-- wrap-center -->
		</div><!-- col-md-12-->

	</div><!-- row -->
</div><!-- container -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
	<script src="<?php echo e(asset('public/plugins/datepicker/bootstrap-datepicker.js'), false); ?>" type="text/javascript"></script>

	<script type="text/javascript">

  //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm',
      startView: "months",
      minViewMode: "months",
      startDate: '+1m',
      //endDate: '+30d',
      language: 'en'
    });

    $(".onlyNumber").keypress(function(event) {
        return /\d/.test(String.fromCharCode(event.keyCode));
    });

    </script>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>