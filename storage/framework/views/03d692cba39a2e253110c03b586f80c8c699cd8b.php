<?php

	$settings = App\Models\AdminSettings::first();
	$percentage=0;
	$message = '';
	// if ($response->donations()->sum('donation')) {
 //    // calculate the percentage
	// 	$percentage = round($response->donations()->sum('donation') / $response->goal * 100);
	// }
	

	/*if( $percentage > 100 ) {
		$percentage = 100;
	} else {
		$percentage = $percentage;
	}*/

	// All Donations
	$donations = $response->donations()->orderBy('id','desc')->paginate(2);

	// Updates
	$updates = $response->updates()->orderBy('id','desc')->paginate(1);

	if( str_slug( $response->title ) == '' ) {
		$slug_url  = '';
	} else {
		$slug_url  = '/'.str_slug( $response->title );
	}

 ?>
 

<?php $__env->startSection('title'); ?><?php echo e(trans('misc.donate').' - '.$response->title.' - ', false); ?><?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('public/plugins/iCheck/all.css'), false); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>


<?php $__env->startSection('javascript'); ?>
<script src="https://checkout.stripe.com/checkout.js"></script>
<script type="text/javascript">

$('#message').on('change', function(){
    $message = $('#message').val();
    $("#div_message").html($message);
});

$('#fullname').on('change', function(){
    $fullname = "From "+$('#fullname').val();
    $("#message_footer").html($fullname);
});
$('#onlyNumber').on('change', function(){
    $onlyNumber = "£"+$('#onlyNumber').val()+" Gift of money + Gomp Fee (£1)";
    $total="Total £"+$('#onlyNumber').val();
    $("#pay").html($onlyNumber);
    $("#total_pay").html($total);
});

</script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="jumbotron md header-donation jumbotron_set">
      <div class="container wrap-jumbotron position-relative">
      	<h1 class="title-site">Give Now</h1>
      	<h2 class="subtitle-site"><strong><?php echo e($response->title, false); ?></strong></h2>
      </div>
    </div>

<div class="container margin-bottom-40 padding-top-40">

<!-- Col MD -->
<div class="col-md-8 margin-bottom-20">

	   <!-- form start -->
    <form method="POST" action="<?php echo e(url('donate',$response->id), false); ?>" enctype="multipart/form-data" id="formDonation">

    	<input type="hidden" name="_token" value="<?php echo e(csrf_token(), false); ?>">
    	<input type="hidden" name="_id" value="<?php echo e($response->id, false); ?>">
			<?php if( isset($pledge) ): ?>
				<input type="hidden" name="_pledge" value="<?php echo e($pledge->id, false); ?>">
			<?php endif; ?>


			<div class="row">
				<div class="col-md-6">
					<div class="modal-content headerModal headerModalOverlay" style= "background-image: url('<?php echo e(url('/public/img/pot1.jpg'), false); ?>');background-size: 100%; height: 325px;position: relative;opacity: 1 ">
					<div class="form-group" style=" padding-top: 40%;padding-right: 4%;padding-left: 4%;">
						    <label><?php echo e(trans('Enter amount for gift'), false); ?></label>
						    <div class="input-group has-success" style="width: 80%;margin: 0 auto;">
						      <div class="input-group-addon addon-dollar" ><?php echo e($settings->currency_symbol, false); ?></div>
						      <input type="number" min="<?php echo e($settings->min_donation_amount, false); ?>"  autocomplete="off" id="onlyNumber" class="form-control input-lg" name="amount" <?php if( isset($pledge) ): ?>readonly='readonly'<?php endif; ?> value="<?php if( isset($pledge) ): ?><?php echo e($pledge->amount, false); ?><?php endif; ?>" placeholder="<?php echo e(trans('misc.minimum_amount'), false); ?>" required>
						    </div>
						  </div>
						</div>

				</div>


                 <!-- Start -->
				<div class="col-md-6">

                    <div class="form-group">
                      <label><?php echo e(trans('auth.full_name'), false); ?></label>
                        <input type="text" id='fullname'  value="<?php if( Auth::check() ): ?><?php echo e(Auth::user()->name, false); ?><?php endif; ?>" name="full_name" class="form-control input-lg" placeholder="<?php echo e(trans('misc.first_name_and_last_name'), false); ?>">
                    </div><!-- /. End-->

                    <!-- Start -->
                    <!--  <div class="form-group">
                      <label><?php echo e(trans('auth.email'), false); ?></label>
                        <input type="text"  value="<?php if( Auth::check() ): ?><?php echo e(Auth::user()->email, false); ?><?php endif; ?>" name="email" class="form-control input-lg" placeholder="<?php echo e(trans('auth.email'), false); ?>">
                    </div>/. End

              <div class="row form-group">
                   Start
                    <div class="col-xs-6">
                      <label><?php echo e(trans('misc.country'), false); ?></label>
                      	<select name="country" class="form-control input-lg" >
                      		<option value=""><?php echo e(trans('misc.select_one'), false); ?></option>
                      	<?php $__currentLoopData = App\Models\Countries::orderBy('country_name')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option <?php if( Auth::check() && Auth::user()->countries_id == $country->id ): ?> selected="selected" <?php endif; ?> value="<?php echo e($country->country_name, false); ?>"><?php echo e($country->country_name, false); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </select>
                  </div>/. End

                Start -->
                   <!--  <div class="col-xs-6">
                      <label><?php echo e(trans('misc.postal_code'), false); ?></label>
                        <input type="text"  value="<?php echo e(old('postal_code'), false); ?>" name="postal_code" class="form-control input-lg" placeholder="<?php echo e(trans('misc.postal_code'), false); ?>">
                    </div>/. End-->

              <!-- </div> -->

                  <!-- Start -->
                    <div class="form-group">
                    	<label><?php echo e(trans('Message'), false); ?></label>
                        <input type="text" id="message" value="<?php echo e(old('comment'), false); ?>" name="comment" class="form-control input-lg" placeholder="<?php echo e(trans('Write a message'), false); ?>">
                    </div><!-- /. End-->

										<!-- Start -->
	                    <div class="form-group">
												<label><?php echo e(trans('misc.payment_gateway'), false); ?></label>
													<select name="payment_gateway" id="paymentGateway" class="form-control input-lg" >
															<option value=""><?php echo e(trans('misc.select_one'), false); ?></option>
															<?php if($settings->enable_paypal == '1'): ?>
																<option value="paypal">PayPal</option>
															<?php endif; ?>

															<?php if($settings->enable_stripe == '1'): ?>
																<option value="stripe"><?php echo e(trans('misc.debit_credit_card'), false); ?></option>
															<?php endif; ?>



															<?php if($settings->enable_bank_transfer == '1'): ?>
																<option value="bank_transfer"><?php echo e(trans('misc.bank_transfer'), false); ?></option>
															<?php endif; ?>

															<?php if($settings->enable_mangopay == '1'): ?>
																<option value="mangopay">MangoPay</option>
															<?php endif; ?>

														</select>
	                    </div><!-- /. End-->

											<?php if($settings->enable_bank_transfer == '1'): ?>

											<div class="btn-block display-none" id="bankTransferBox">
												<div class="alert alert-info">
												<h4><strong><i class="fa fa-bank"></i> <?php echo e(trans('misc.make_payment_bank'), false); ?></strong></h4>
												<ul class="list-unstyled">
														<?php if( $settings->bank_swift_code != '' ): ?>
													<li><strong><?php echo e(trans('misc.bank_swift_code'), false); ?>:</strong> <?php echo e($settings->bank_swift_code, false); ?></li>
													<?php endif; ?>
														<?php if( $settings->account_number != '' ): ?>
													<li><strong><?php echo e(trans('misc.account_number'), false); ?>:</strong> <?php echo e($settings->account_number, false); ?></li>
													<?php endif; ?>
														<?php if( $settings->branch_name != '' ): ?>
													<li><strong><?php echo e(trans('misc.branch_name'), false); ?>:</strong> <?php echo e($settings->branch_name, false); ?></li>
													<?php endif; ?>
														<?php if( $settings->branch_address != '' ): ?>
													<li><strong><?php echo e(trans('misc.branch_address'), false); ?>:</strong> <?php echo e($settings->branch_address, false); ?></li>
													<?php endif; ?>
														<?php if( $settings->account_name != '' ): ?>
													<li><strong><?php echo e(trans('misc.account_name'), false); ?>:</strong> <?php echo e($settings->account_name, false); ?></li>
													<?php endif; ?>
													<?php if( $settings->iban != '' ): ?>
													<li><strong><?php echo e(trans('misc.iban'), false); ?>:</strong> <?php echo e($settings->iban, false); ?></li>
												<?php endif; ?>
												</ul>
											</div>

											<div class="row form-group">
				                  <!-- Start -->
				                    <div class="col-sm-6">
															<label><?php echo e(trans('misc.bank_swift_code'), false); ?></label>
				                        <input type="text"  value="" name="bank_swift_code" class="form-control input-lg" placeholder="<?php echo e(trans('misc.bank_swift_code'), false); ?>">
				                  </div><!-- /. End-->

				                  <!-- Start -->
				                    <div class="col-sm-6">
				                      <label><?php echo e(trans('misc.account_number'), false); ?></label>
				                        <input type="text"  value="" name="account_number" class="form-control input-lg" placeholder="<?php echo e(trans('misc.account_number'), false); ?>">
				                    </div><!-- /. End-->
				              </div><!-- row form-control -->

											<div class="row form-group">
				                  <!-- Start -->
				                    <div class="col-sm-6">
															<label><?php echo e(trans('misc.branch_name'), false); ?></label>
				                        <input type="text"  value="" name="branch_name" class="form-control input-lg" placeholder="<?php echo e(trans('misc.branch_name'), false); ?>">
				                  </div><!-- /. End-->

				                  <!-- Start -->
				                    <div class="col-sm-6">
				                      <label><?php echo e(trans('misc.branch_address'), false); ?></label>
				                        <input type="text"  value="" name="branch_address" class="form-control input-lg" placeholder="<?php echo e(trans('misc.branch_address'), false); ?>">
				                    </div><!-- /. End-->
				              </div><!-- row form-control -->

											<div class="row form-group">
				                  <!-- Start -->
				                    <div class="col-sm-6">
															<label><?php echo e(trans('misc.account_name'), false); ?></label>
				                        <input type="text"  value="" name="account_name" class="form-control input-lg" placeholder="<?php echo e(trans('misc.account_name'), false); ?>">
				                  </div><!-- /. End-->

				                  <!-- Start -->
				                    <div class="col-sm-6">
				                      <label><?php echo e(trans('misc.iban'), false); ?></label>
				                        <input type="text"  value="" name="iban" class="form-control input-lg" placeholder="<?php echo e(trans('misc.iban'), false); ?>">
				                    </div><!-- /. End-->
				              </div><!-- row form-control -->
											</div><!-- Alert -->
											<?php endif; ?>


        <div class="form-group checkbox icheck">
				<label class="margin-zero">
					<input class="no-show" style="position: static;" name="anonymous" type="checkbox" value="1">
					<span class="margin-lft5 keep-login-title"><?php echo e(trans('misc.anonymous_donation'), false); ?></span>
			</label>
		</div>
	</div>
</div>


                    <!-- Alert -->
            <div class="alert alert-danger display-none" id="errorDonation">
							<ul class="list-unstyled" id="showErrorsDonation"></ul>
						</div><!-- Alert -->

                  <div class="box-footer text-center">
                  	<hr />

                   <button href="#giveModal" data-toggle="modal" type="button" class="btn btn-primary">Preview</button>
                   
                    <div class="btn-block text-center margin-top-20">
			           		<a href="<?php echo e(url('campaign',$response->id), false); ?>" class="text-muted">
			           		<i class="fa fa-long-arrow-left"></i>	<?php echo e(trans('auth.back'), false); ?></a>
			           </div>
                  </div><!-- /.box-footer -->

                </form>

 </div><!-- /COL MD -->




 <div class="modal" id="giveModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content headerModal headerModalOverlay" style= "background-image: url('<?php echo e(url('/public/img/frame.jpg'), false); ?>') ">
    	
      <div class="modal-header">
        <h5 class="modal-title">Gift</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form autocomplete="off" action="<?php echo e(url('search'), false); ?>" method="get">
	        
	        <div style="border:1px solid black; background-image: url('<?php echo e(url('/public/img/frame.jpg'), false); ?>'); background-size: 100% 100%; height: 300px; padding-left: 10%; padding-top: 5%; padding-right: 10%">
	        	 <!-- <img src="<?php echo e(asset('/public/img/frame.jpg'), false); ?>" width="200" height="200"/> -->
	        	 <div style="font-size: 18px" id="pay">
	        	 		
	        	 </div>
	        	  <div style="font-size: 18px" id="total_pay">
	        	 		
	        	 </div>

	        	 <div>
	        	 	<h4 style="color: black">Dear <?php echo e($response->user()->name, false); ?>,</h4>
	        	 </div>
	        	 <div style="font-size: 18px" id="div_message">
	        	 	
	        	 </div>
	        	 <div style="font-size: 18px;position: absolute;bottom: 33%;right: 12%;" id="message_footer">
	        	 	
	        	 </div>
	        	 
	        </div>
	    	
	        <div class="modal-footer">
	        	<button data-toggle="modal"type="submit" id="buttonDonation" class="btn-padding-custom btn btn-lg btn-main custom-rounded">Checkout</button>

		       
		    </div>
	        
	    </form>
      </div>
    </div>
  </div>
</div>

 <div class="col-md-4">

	<?php if( isset($pledge) ): ?>
	 <div class="panel panel-default">
  		<div class="panel-body">
				<h3 class="btn-block margin-zero" style="line-height: inherit;">
					<?php echo e(trans('misc.seleted_pledge'), false); ?> <small><a href="<?php echo e(url('donate',$response->id), false); ?>"><?php echo e(trans('misc.remove'), false); ?></a></small>
				</h3>
 			<h3 class="btn-block margin-zero" style="line-height: inherit;">
 				<small><?php echo e(trans('misc.pledge'), false); ?></small>
 				<strong class="font-default"><?php echo e(App\Helper::amountFormat($pledge->amount), false); ?></strong>
 				</h3>
				<h4><?php echo e($pledge->title, false); ?></h4>
 				<p class="wordBreak">
 					<?php echo e($pledge->description, false); ?>

 				</p>

				<small class="btn-block text-muted">
					<?php echo e(trans('misc.delivery'), false); ?>:
				</small>
				<strong><?php echo e(date('F, Y', strtotime($pledge->delivery)), false); ?></strong>
 		</div><!-- panel-body -->
 	</div><!-- End Panel -->
<?php endif; ?>

	<!-- Start Panel -->
	<!-- <div class="panel panel-default">
		<div class="panel-body">
			<h3 class="btn-block margin-zero" style="line-height: inherit;">
				<strong class="font-default"><?php echo e(App\Helper::amountFormat($response->donations()->sum('donation')), false); ?></strong>
				<small><?php echo e(trans('misc.of'), false); ?> <?php echo e(App\Helper::amountFormat($response->goal), false); ?> <?php echo e(strtolower(trans('misc.goal')), false); ?></small>
				</h3>

				<span class="progress margin-top-10 margin-bottom-10">
					<span class="percentage" style="width: <?php echo e($percentage, false); ?>%" aria-valuemin="0" aria-valuemax="100" role="progressbar"></span>
				</span>

				<small class="btn-block margin-bottom-10 text-muted">
					<?php echo e($percentage, false); ?>% <?php echo e(trans('misc.raised'), false); ?> <?php echo e(trans('misc.by'), false); ?> <?php echo e(number_format($response->donations()->count()), false); ?> <?php echo e(trans_choice('misc.donation_plural',$response->donations()->count()), false); ?>

				</small>
		</div>
	</div><!- End Panel --> 

	<!-- Start Panel -->
	 	<div class="panel panel-default">
		  <div class="panel-body">
		    <div class="media none-overflow">

		    	<span class="btn-block text-center margin-bottom-10 text-muted"><strong><?php echo e(trans('misc.organizer'), false); ?></strong></span>

				  <div class="media-center margin-bottom-5">
				      <img class="img-circle center-block" src="<?php echo e(url('public/avatar/',$response->user()->avatar), false); ?>" width="60" height="60" >
				  </div>

				  <div class="media-body text-center">

				    	<h4 class="media-heading">
				    		<?php echo e($response->user()->name, false); ?>


				    	<?php if( Auth::guest()  || Auth::check() && Auth::user()->id != $response->user()->id ): ?>
				    		<a href="#" title="<?php echo e(trans('misc.contact_organizer'), false); ?>" data-toggle="modal" data-target="#sendEmail">
				    				<i class="fa fa-envelope myicon-right"></i>
				    		</a>
				    		<?php endif; ?>
				    		</h4>

				    <small class="media-heading text-muted btn-block margin-zero"><?php echo e(trans('misc.created'), false); ?> <?php echo e(date($settings->date_format, strtotime($response->date) ), false); ?></small>
				    <?php if( $response->location != '' ): ?>
				    <small class="media-heading text-muted btn-block"><i class="fa fa-map-marker myicon-right"></i> <?php echo e($response->location, false); ?></small>
				    <?php endif; ?>
				  </div>
				</div>
		  </div>
		</div><!-- End Panel -->

	<div class="panel panel-default">
		<div class="panel-body">
			<img class="img-responsive img-rounded" style="display: inline-block;" src="<?php echo e(url('public/campaigns/small',$response->small_image), false); ?>" />
			</div>
		</div>

	<div class="modal fade" id="sendEmail" tabindex="-1" role="dialog" aria-hidden="true">
	     		<div class="modal-dialog modalContactOrganizer">
	     			<div class="modal-content">
	     				<div class="modal-header headerModal headerModalOverlay position-relative" style="background-image: url('<?php echo e(url('public/campaigns/large',$response->large_image), false); ?>')">
					        <button type="button" class="close closeLight position-relative" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

					        <span class="btn-block margin-top-15 margin-bottom-15 text-center position-relative">
								      <img class="img-circle" src="<?php echo e(url('public/avatar/',$response->user()->avatar), false); ?>" width="80" height="80" >
								    </span>

							<h5 class="modal-title text-center font-default position-relative" id="myModalLabel">
					        	<?php echo e($response->user()->name, false); ?>

					        	</h5>

					        <h4 class="modal-title text-center font-default position-relative" id="myModalLabel">
					        	<?php echo e(trans('misc.contact_organizer'), false); ?>

					        	</h4>
					     </div><!-- Modal header -->

					      <div class="modal-body listWrap text-center center-block modalForm">

					    <!-- form start -->
				    <form method="POST" class="margin-bottom-15" action="<?php echo e(url('contact/organizer'), false); ?>" enctype="multipart/form-data" id="formContactOrganizer">
				    	<input type="hidden" name="_token" value="<?php echo e(csrf_token(), false); ?>">
				    	<input type="hidden" name="id" value="<?php echo e($response->user()->id, false); ?>">

					    <!-- Start Form Group -->
	                    <div class="form-group">
	                    	<input type="text" required="" name="name" class="form-control" placeholder="<?php echo e(trans('users.name'), false); ?>">
	                    </div><!-- /.form-group-->

	                    <!-- Start Form Group -->
	                    <div class="form-group">
	                    	<input type="text" required="" name="email" class="form-control" placeholder="<?php echo e(trans('auth.email'), false); ?>">
	                    </div><!-- /.form-group-->

	                    <!-- Start Form Group -->
	                    <div class="form-group">
	                    	<textarea name="message" rows="4" class="form-control" placeholder="<?php echo e(trans('misc.message'), false); ?>"></textarea>
	                    </div><!-- /.form-group-->

	                    <!-- Alert -->
	                    <div class="alert alert-danger display-none" id="dangerAlert">
								<ul class="list-unstyled text-left" id="showErrors"></ul>
							</div><!-- Alert -->

	                   <button type="submit" class="btn btn-lg btn-main custom-rounded" id="buttonFormSubmit"><?php echo e(trans('misc.send_message'), false); ?></button>
	                    </form>

	               <!-- Alert -->
	             <div class="alert alert-success display-none" id="successAlert">
								<ul class="list-unstyled" id="showSuccess"></ul>
							</div><!-- Alert -->

					      </div><!-- Modal body -->
	     				</div><!-- Modal content -->
	     			</div><!-- Modal dialog -->
	     		</div><!-- Modal -->

 </div><!-- /COL MD -->

 </div><!-- container wrap-ui -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script src="<?php echo e(asset('public/plugins/iCheck/icheck.min.js'), false); ?>"></script>

<script type="text/javascript">
/*function onlyNumber(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if ((charCode < 48 || charCode > 57))
        return false;
    return true;
}*/

$('#onlyNumber').focus();

$(document).ready(function() {

	$("#onlyNumber").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });


    $('input').iCheck({
	  	checkboxClass: 'icheckbox_square-red',
    	radioClass: 'iradio_square-red',
	    increaseArea: '20%' // optional
	  });
});

$('#paymentGateway').on('change', function(){
    if($(this).val() == 'bank_transfer') {
			$('#bankTransferBox').slideDown();
		} else {
				$('#bankTransferBox').slideUp();
		}
});

$('#message').on('change', function(){
    $message = $('#message').val();
    alert($message);
});



</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>