<!-- ***** Footer ***** -->
    <footer class="footer-main">
    	<div class="container">
    		
    		<div class="row">
    			<div class="col-md-4">
    				<a href="<?php echo e(url('/'), false); ?>">
    					<img src="<?php echo e(asset('public/img/watermark.png'), false); ?>" style="width: 30%;" />
    				</a>
    			   <p class="margin-tp-xs"><?php echo e($settings->description, false); ?></p>
    			   
    			   <ul class="list-inline">
					   <?php if( $settings->twitter != '' ): ?> 
					   <li><a href="<?php echo e($settings->twitter, false); ?>" class="ico-social"><i class="fa fa-twitter"></i></a></li>
					   <?php endif; ?>
					 
					 <?php if( $settings->facebook != '' ): ?>   
					   <li><a href="<?php echo e($settings->facebook, false); ?>" class="ico-social"><i class="fa fa-facebook"></i></a></li>
					 <?php endif; ?>
					
					 <?php if( $settings->instagram != '' ): ?>   
					   <li><a href="<?php echo e($settings->instagram, false); ?>" class="ico-social"><i class="fa fa-instagram"></i></a></li>
					 <?php endif; ?>
					 
					 <?php if( $settings->linkedin != '' ): ?>   
					   <li><a href="<?php echo e($settings->linkedin, false); ?>" class="ico-social"><i class="fa fa-linkedin"></i></a></li>
					   <?php endif; ?>
					 
					 <?php if( $settings->googleplus != '' ): ?>   
					   <li><a href="<?php echo e($settings->googleplus, false); ?>" class="ico-social"><i class="fa fa-google-plus"></i></a></li>
					 <?php endif; ?>
					 </ul >
					 
    			</div><!-- ./End col-md-* -->

    			
    			
    			<div class="col-md-3 margin-tp-xs">
    				<h4 class="margin-top-zero"><?php echo e(trans('misc.about'), false); ?></h4>
    				<ul class="list-unstyled">
    					<?php $__currentLoopData = App\Models\Pages::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        			<li><a class="link-footer" href="<?php echo e(url('/page',$page->slug), false); ?>"><?php echo e($page->title, false); ?></a></li>
        	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    				</ul>
    			</div><!-- ./End col-md-* -->
    			
    			
    			<div class="col-md-3 margin-tp-xs">
    				<h4 class="margin-top-zero"><?php echo e(trans('misc.categories'), false); ?></h4>
    				<ul class="list-unstyled">
    					<?php $__currentLoopData = App\Models\Categories::where('mode','on')->orderBy('name')->take(6)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        			<li><a class="link-footer" href="<?php echo e(url('category'), false); ?>/<?php echo e($category->slug, false); ?>"><?php echo e($category->name, false); ?></a></li>
        	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        	
        	<?php if( App\Models\Categories::count() > 6 ): ?>
        		<li><a class="link-footer" href="<?php echo e(url('categories'), false); ?>">
        			<strong><?php echo e(trans('misc.view_all'), false); ?> <i class="fa fa-long-arrow-right"></i></strong>
        		</a></li>
        		<?php endif; ?>
        		
    				</ul>
    			</div><!-- ./End col-md-* -->
    			
    			<div class="col-md-2 margin-tp-xs">
    				<h4 class="margin-top-zero"><?php echo e(trans('misc.links'), false); ?></h4>
    				<ul class="list-unstyled">
        			
        			<li>
        				<a class="link-footer" href="<?php echo e(url('/'), false); ?>"><?php echo e(trans('misc.campaigns'), false); ?></a>
        			</li>
        			
        			<?php if( Auth::guest() ): ?>
        			<li>
        				<a class="link-footer" href="<?php echo e(url('login'), false); ?>">
        					<?php echo e(trans('auth.login'), false); ?>

        				</a>
        				</li>

        				
        				<?php else: ?>
        				<li>
	          		 		<a href="<?php echo e(url('account'), false); ?>" class="link-footer">
	          		 			<?php echo e(trans('users.account_settings'), false); ?>

	          		 		</a>
	          		 		</li>
	          		 		
	          		 		<li>
	          		 			<a href="<?php echo e(url('logout'), false); ?>" class="logout link-footer">
	          		 				<?php echo e(trans('users.logout'), false); ?>

	          		 			</a>
	          		 		</li>
        				<?php endif; ?>
        				
    				</ul>
    			</div><!-- ./End col-md-* -->
    		</div><!-- ./End Row -->
    	</div><!-- ./End Container -->
    </footer><!-- ***** Footer ***** -->

<footer class="subfooter">
	<div class="container">
	<div class="row">
    			<div class="col-md-12 text-center padding-top-20">
    				<p>&copy; <?php echo e($settings->title, false); ?> - <?php echo date('Y'); ?></p>
    			</div><!-- ./End col-md-* -->
	</div>
</div>
</footer>    
