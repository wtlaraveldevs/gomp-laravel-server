<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('public/plugins/iCheck/all.css'), false); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
            <?php echo e(trans('admin.admin'), false); ?>

            	<i class="fa fa-angle-right margin-separator"></i>
            		<?php echo e(trans('misc.payment_settings'), false); ?>


          </h4>

        </section>

        <!-- Main content -->
        <section class="content">

        	 <?php if(Session::has('success_message')): ?>
		    <div class="alert alert-success">
		    	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">×</span>
								</button>
		       <i class="fa fa-check margin-separator"></i> <?php echo e(Session::get('success_message'), false); ?>

		    </div>
		<?php endif; ?>

        	<div class="content">

        		<div class="row">

        	<div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title"><strong><?php echo e(trans('misc.payment_settings'), false); ?></strong></h3>
                </div><!-- /.box-header -->

                <!-- form start -->
                <form class="form-horizontal" method="POST" action="<?php echo e(url('panel/admin/payments'), false); ?>" enctype="multipart/form-data">

                	<input type="hidden" name="_token" value="<?php echo e(csrf_token(), false); ?>">

					<?php echo $__env->make('errors.errors-forms', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>


                      <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('admin.currency_code'), false); ?></label>
                      <div class="col-sm-10">
                      	<select name="currency_code" class="form-control">

                      		<option <?php if( $settings->currency_code == 'USD' ): ?> selected="selected" <?php endif; ?> value="USD">USD - U.S Dollar</option>
						  	<option <?php if( $settings->currency_code == 'EUR' ): ?> selected="selected" <?php endif; ?>  value="EUR">EUR - Euro</option>
						  	<option <?php if( $settings->currency_code == 'GBP' ): ?> selected="selected" <?php endif; ?> value="GBP">GBP - UK</option>
						  	<option <?php if( $settings->currency_code == 'AUD' ): ?> selected="selected" <?php endif; ?> value="AUD">AUD - Australian Dollar</option>
						  	<option <?php if( $settings->currency_code == 'JPY' ): ?> selected="selected" <?php endif; ?> value="JPY">JPY - Japanese Yen</option>

						  	<option <?php if( $settings->currency_code == 'BRL' ): ?> selected="selected" <?php endif; ?> value="BRL">BRL - Brazilian Real</option>
						  	<option <?php if( $settings->currency_code == 'MXN' ): ?> selected="selected" <?php endif; ?>  value="MXN">MXN - Mexican Peso</option>
						  	<option <?php if( $settings->currency_code == 'SEK' ): ?> selected="selected" <?php endif; ?> value="SEK">SEK - Swedish Krona</option>
						  	<option <?php if( $settings->currency_code == 'CHF' ): ?> selected="selected" <?php endif; ?> value="CHF">CHF - Swiss Franc</option>


						  	<option <?php if( $settings->currency_code == 'SGD' ): ?> selected="selected" <?php endif; ?> value="SGD">SGD - Singapore Dollar</option>
						  	<option <?php if( $settings->currency_code == 'DKK' ): ?> selected="selected" <?php endif; ?> value="DKK">DKK - Danish Krone</option>
						  	<option <?php if( $settings->currency_code == 'RUB' ): ?> selected="selected" <?php endif; ?> value="RUB">RUB - Russian Ruble</option>

						  	<option <?php if( $settings->currency_code == 'CAD' ): ?> selected="selected" <?php endif; ?> value="CAD">CAD - Canadian Dollar</option>
						  	<option <?php if( $settings->currency_code == 'CZK' ): ?> selected="selected" <?php endif; ?> value="CZK">CZK - Czech Koruna</option>
						  	<option <?php if( $settings->currency_code == 'HKD' ): ?> selected="selected" <?php endif; ?> value="HKD">HKD - Hong Kong Dollar</option>
						  	<option <?php if( $settings->currency_code == 'PLN' ): ?> selected="selected" <?php endif; ?> value="PLN">PLN - Polish Zloty</option>
						  	<option <?php if( $settings->currency_code == 'NOK' ): ?> selected="selected" <?php endif; ?> value="NOK">NOK - Norwegian Krone</option>
                          </select>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                   <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('misc.fee_donation'), false); ?></label>
                      <div class="col-sm-10">
                      	<select name="fee_donation" class="form-control">
                      		<option <?php if( $settings->fee_donation == '1' ): ?> selected="selected" <?php endif; ?> value="1">1%</option>
                      		<option <?php if( $settings->fee_donation == '2' ): ?> selected="selected" <?php endif; ?> value="2">2%</option>
						  	<option <?php if( $settings->fee_donation == '3' ): ?> selected="selected" <?php endif; ?>  value="3">3%</option>
						  	<option <?php if( $settings->fee_donation == '4' ): ?> selected="selected" <?php endif; ?> value="4">4%</option>
						  	<option <?php if( $settings->fee_donation == '5' ): ?> selected="selected" <?php endif; ?> value="5">5%</option>

						  	<option <?php if( $settings->fee_donation == '6' ): ?> selected="selected" <?php endif; ?> value="6">6%</option>
						  	<option <?php if( $settings->fee_donation == '7' ): ?> selected="selected" <?php endif; ?> value="7">7%</option>
						  	<option <?php if( $settings->fee_donation == '8' ): ?> selected="selected" <?php endif; ?> value="8">8%</option>
						  	<option <?php if( $settings->fee_donation == '9' ): ?> selected="selected" <?php endif; ?> value="9">9%</option>

						  	<option <?php if( $settings->fee_donation == '10' ): ?> selected="selected" <?php endif; ?> value="10">10%</option>
						  	<option <?php if( $settings->fee_donation == '15' ): ?> selected="selected" <?php endif; ?> value="15">15%</option>
                          </select>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('misc.payments_options'), false); ?></label>
                      <div class="col-sm-10">
                        <div class="radio">
                        <label class="padding-zero">
                          <input type="checkbox" value="1" name="enable_paypal" <?php if( $settings->enable_paypal == 1 ): ?> checked="checked" <?php endif; ?>>
                          PayPal
                        </label>
                      </div>
                      <div class="radio">
                        <label class="padding-zero">
                          <input type="checkbox" value="1" name="enable_mangopay" <?php if( $settings->enable_mangopay == 1 ): ?> checked="checked" <?php endif; ?>>
                          MangoPay
                        </label>
                      </div>
                      <div class="radio">
                        <label class="padding-zero">
                          <input type="checkbox" value="1" name="enable_stripe" <?php if( $settings->enable_stripe == 1 ): ?> checked="checked" <?php endif; ?>>
                          Stripe
                        </label>
                      </div>
                      <div class="radio">
                        <label class="padding-zero">
                          <input type="checkbox" value="1" name="enable_bank_transfer" <?php if( $settings->enable_bank_transfer == 1 ): ?> checked="checked" <?php endif; ?>>
                          <?php echo e(trans('misc.bank_transfer'), false); ?>

                        </label>
                      </div>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-header with-border">
                    <h3 class="box-title"><strong>PayPal</strong></h3>
                  </div><!-- /.box-header -->

                  <!-- Start Box Body -->
               <div class="box-body">
                 <div class="form-group">
                   <label class="col-sm-2 control-label"><?php echo e(trans('admin.paypal_account'), false); ?></label>
                   <div class="col-sm-10">
                     <input type="text" value="<?php echo e($settings->paypal_account, false); ?>" name="paypal_account" class="form-control" placeholder="<?php echo e(trans('admin.paypal_account'), false); ?>">
                     <p class="help-block"><?php echo e(trans('admin.paypal_account_donations'), false); ?></p>
                   </div>
                 </div>
               </div><!-- /.box-body -->

               <!-- Start Box Body -->
               <div class="box-body">
                 <div class="form-group">
                   <label class="col-sm-2 control-label">PayPal Sandbox</label>
                   <div class="col-sm-10">
                     <div class="radio">
                     <label class="padding-zero">
                       <input type="radio" value="true" name="paypal_sandbox" <?php if( $settings->paypal_sandbox == 'true' ): ?> checked="checked" <?php endif; ?> checked>
                       On
                     </label>
                   </div>
                   <div class="radio">
                     <label class="padding-zero">
                       <input type="radio" value="false" name="paypal_sandbox" <?php if( $settings->paypal_sandbox == 'false' ): ?> checked="checked" <?php endif; ?>>
                       Off
                     </label>
                   </div>
                   </div>
                 </div>
               </div><!-- /.box-body -->

               <div class="box-header with-border">
                 <h3 class="box-title"><strong>Stripe</strong></h3>
               </div><!-- /.box-header -->

                 <!-- Start Box Body -->
                 <div class="box-body">
                   <div class="form-group">
                     <label class="col-sm-2 control-label">Stripe Secret Key</label>
                     <div class="col-sm-10">
                       <input type="text" value="<?php echo e($settings->stripe_secret_key, false); ?>" name="stripe_secret_key" class="form-control">
                      <p class="help-block"><a href="https://stripe.com/dashboard" target="_blank">https://stripe.com/dashboard</a></p>
                     </div>
                   </div>
                 </div><!-- /.box-body -->

                 <!-- Start Box Body -->
                 <div class="box-body">
                   <div class="form-group">
                     <label class="col-sm-2 control-label">Stripe Publishable Key</label>
                     <div class="col-sm-10">
                       <input type="text" value="<?php echo e($settings->stripe_public_key, false); ?>" name="stripe_public_key" class="form-control">
                      <p class="help-block"><a href="https://stripe.com/dashboard" target="_blank">https://stripe.com/dashboard</a></p>
                     </div>
                   </div>
                 </div><!-- /.box-body -->

                 <div class="box-header with-border">
                   <h3 class="box-title"><strong><?php echo e(trans('misc.bank_transfer'), false); ?></strong></h3>
                 </div><!-- /.box-header -->


                 <!-- Start Box Body -->
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo e(trans('misc.bank_swift_code'), false); ?></label>
                  <div class="col-sm-10">
                    <input type="text" value="<?php echo e($settings->bank_swift_code, false); ?>" name="bank_swift_code" class="form-control" placeholder="<?php echo e(trans('misc.bank_swift_code'), false); ?>">
                  </div>
                </div>
              </div><!-- /.box-body -->

              <!-- Start Box Body -->
           <div class="box-body">
             <div class="form-group">
               <label class="col-sm-2 control-label"><?php echo e(trans('misc.account_number'), false); ?></label>
               <div class="col-sm-10">
                 <input type="text" value="<?php echo e($settings->account_number, false); ?>" name="account_number" class="form-control" placeholder="<?php echo e(trans('misc.account_number'), false); ?>">
               </div>
             </div>
           </div><!-- /.box-body -->

                 <!-- Start Box Body -->
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo e(trans('misc.branch_name'), false); ?></label>
                  <div class="col-sm-10">
                    <input type="text" value="<?php echo e($settings->branch_name, false); ?>" name="branch_name" class="form-control" placeholder="<?php echo e(trans('misc.branch_name'), false); ?>">
                  </div>
                </div>
              </div><!-- /.box-body -->

              <!-- Start Box Body -->
           <div class="box-body">
             <div class="form-group">
               <label class="col-sm-2 control-label"><?php echo e(trans('misc.branch_address'), false); ?></label>
               <div class="col-sm-10">
                 <input type="text" value="<?php echo e($settings->branch_address, false); ?>" name="branch_address" class="form-control" placeholder="<?php echo e(trans('misc.branch_address'), false); ?>">
               </div>
             </div>
           </div><!-- /.box-body -->

           <!-- Start Box Body -->
        <div class="box-body">
          <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo e(trans('misc.account_name'), false); ?></label>
            <div class="col-sm-10">
              <input type="text" value="<?php echo e($settings->account_name, false); ?>" name="account_name" class="form-control" placeholder="<?php echo e(trans('misc.account_name'), false); ?>">
            </div>
          </div>
        </div><!-- /.box-body -->

        <!-- Start Box Body -->
     <div class="box-body">
       <div class="form-group">
         <label class="col-sm-2 control-label"><?php echo e(trans('misc.iban'), false); ?></label>
         <div class="col-sm-10">
           <input type="text" value="<?php echo e($settings->iban, false); ?>" name="iban" class="form-control" placeholder="<?php echo e(trans('misc.iban'), false); ?>">
         </div>
       </div>
     </div><!-- /.box-body -->

               <div class="box-footer">
                 <button type="submit" class="btn btn-success"><?php echo e(trans('admin.save'), false); ?></button>
               </div><!-- /.box-footer -->
               </form>

              </div><!-- /.row -->

        	</div><!-- /.content -->

          <!-- Your Page Content Here -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

	<!-- icheck -->
	<script src="<?php echo e(asset('public/plugins/iCheck/icheck.min.js'), false); ?>" type="text/javascript"></script>

	<script type="text/javascript">
		//Flat red color scheme for iCheck
        $('input[type="radio"]').iCheck({
          radioClass: 'iradio_flat-red'
        });

        $('input[type="checkbox"]').iCheck({
    	  	checkboxClass: 'icheckbox_square-red',
        	radioClass: 'iradio_square-red',
    	    increaseArea: '20%' // optional
	  });

	</script>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>