<div class="col-xs-12 col-sm-6 col-md-3 col-thumb">
 <?php

   $settings = App\Models\AdminSettings::first();

	if( str_slug( $key->title ) == '' ) {
		$slugUrl  = '';
	} else {
		$slugUrl  = '/'.str_slug( $key->title );
	}

	$url = url('campaign',$key->id).$slugUrl;
	// $percentage = round($key->donations()->sum('donation') / $key->goal * 100);

  // Deadline
	$timeNow = strtotime(Carbon\Carbon::now());

	if( $key->deadline != '' ) {
	    $deadline = strtotime($key->deadline);

		$date = strtotime($key->deadline);
	    $remaining = $date - $timeNow;

		$days_remaining = floor($remaining / 86400);
	}

?>
<div class="thumbnail padding-top-zero">

				<a class="position-relative btn-block img-grid" href="<?php echo e($url, false); ?>">

				<?php if( $key->featured == 1 ): ?>
					<span class="box-featured" title="<?php echo e(trans('misc.featured_campaign'), false); ?>"><i class="fa fa-trophy"></i></span>
					<?php endif; ?>

					<img title="<?php echo e(e($key->title), false); ?>" src="<?php echo e(asset('public/campaigns/small').'/'.$key->small_image, false); ?>" class="image-url img-responsive btn-block radius-image" />
				</a>

    			<div class="caption">
    				<h1 class="title-campaigns font-default">
    					<a title="<?php echo e(e($key->title), false); ?>" class="item-link" href="<?php echo e($url, false); ?>">
    					 <?php echo e(e($key->title), false); ?>

    					</a>
    				</h1>

    				<p class="desc-campaigns">
    					<?php if( isset($key->user()->id) ): ?>
    					<img src="<?php echo e(asset('public/avatar').'/'.$key->user()->avatar, false); ?>" width="20" height="20" class="img-circle avatar-campaign" /> <?php echo e($key->user()->name, false); ?>

    					<?php else: ?>
    					<img src="<?php echo e(asset('public/avatar/default.jpg'), false); ?>" width="20" height="20" class="img-circle avatar-campaign" /> <?php echo e(trans('misc.user_not_available'), false); ?>

    					<?php endif; ?>
    				</p>

    				<p class="desc-campaigns text-overflow">
    					 <?php echo e(str_limit(strip_tags($key->description),80,'...'), false); ?>

    				</p>

    				<p class="desc-campaigns">
              <span class="stats-campaigns margin-bottom-zero">

               <!--  <em>
                  <?php if( isset( $deadline ) && $key->finalized == 0 ): ?>

                    <?php if( $days_remaining > 0 ): ?>
                    <strong><?php echo e($days_remaining, false); ?></strong> <?php echo e(trans('misc.days_left'), false); ?>

                    <?php elseif( $days_remaining == 0 ): ?>
                      <strong><?php echo e(trans('misc.last_day'), false); ?></strong>
                    <?php else: ?>
                      <strong class="text-danger"><?php echo e(trans('misc.no_time_anymore').$days_remaining, false); ?></strong>
                    <?php endif; ?>

                  <?php endif; ?>

                  <?php if( $key->finalized == 1 ): ?>
                    <strong class="text-danger"><?php echo e(trans('misc.campaign_ended'), false); ?></strong>
                    <?php endif; ?>

                    <?php if( !isset( $deadline ) && $key->finalized == 0 ): ?>
                      <strong><?php echo e(trans('misc.no_deadline'), false); ?></strong>
                      <?php endif; ?>
                </em> -->

              </span>

    						<span class="stats-campaigns">
    							<span class="pull-left">
    								<strong><?php echo e(App\Helper::amountFormat($key->donations()->sum('donation')), false); ?></strong>
    								<?php echo e(trans('misc.raised'), false); ?>

    								</span>
    							 
    							</span>

	    					<span class="progress">
	    					
	    					</span>
    				</p>

            <!-- <h6 class="margin-bottom-zero">
    					<em><strong><?php echo e(trans('misc.goal'), false); ?> <?php echo e(App\Helper::amountFormat($key->goal), false); ?></strong></em>
    				</h6> -->

    			</div><!-- /caption -->
    		  </div><!-- /thumbnail -->
    	   </div><!-- /col-sm-6 col-md-4 -->
