<?php
$default   = url('public/avatar/default.jpg');
$size       = 40;
$gravatar = "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $donation->email ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size;

if( $donation->anonymous == 0 ) {
	$gravatar = $gravatar;
} else {
	$gravatar = $default;
}

?>
<li class="list-group-item">
       	<div class="media">
			  <div class="media-left">
			      <img class="media-object img-circle imgDonations" src="<?php echo $gravatar; ?>"  width="40" height="40">
			  </div>
			  <div class="media-body">
			    <h4 class="media-heading"><?php echo e($donation->fullname, false); ?></h4>
			    <span class="btn-block recent-donation-amount font-default">
			    	<?php echo e(App\Helper::amountFormat($donation->donation), false); ?>

			    </span>
			    <?php if( $donation->comment != '' ): ?>
			    <p class="margin-bottom-5"><?php echo e($donation->comment, false); ?></p>
			    <?php endif; ?>
			    <small class="btn-block timeAgo text-muted" data="<?php echo e(date('c', strtotime( $donation->date )), false); ?>"></small>
			  </div>
		</div>
</li>
