<?php
// ** Data User logged ** //
     $user = Auth::user();
	  ?>


<?php $__env->startSection('title'); ?> <?php echo e(trans('users.account_settings'), false); ?> - <?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="jumbotron md index-header jumbotron_set jumbotron-cover">
      <div class="container wrap-jumbotron position-relative">
        <h2 class="title-site"><?php echo e(trans('users.account_settings'), false); ?></h2>
      </div>
    </div>

<div class="container margin-bottom-40">

			<!-- Col MD -->
		<div class="col-md-8 margin-bottom-20">

			<?php if(session('notification')): ?>
			<div class="alert alert-success btn-sm alert-fonts" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            		<?php echo e(session('notification'), false); ?>

            		</div>
            	<?php endif; ?>

			<?php echo $__env->make('errors.errors-forms', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>



		<!-- *********** AVATAR ************* -->

		<form action="<?php echo e(url('upload/avatar'), false); ?>" method="POST" id="formAvatar" accept-charset="UTF-8" enctype="multipart/form-data">
    		<input type="hidden" name="_token" value="<?php echo e(csrf_token(), false); ?>">

    		<div class="text-center">
    			<img src="<?php echo e(asset('public/avatar').'/'.Auth::user()->avatar, false); ?>" alt="User" width="100" height="100" class="img-rounded avatarUser"  />
    		</div>

        <?php if( Auth::check() && Auth::user()->status != 'pending' ): ?>
    		<div class="text-center">
    			<button type="button" class="btn btn-default btn-border btn-sm" id="avatar_file" style="margin-top: 10px;">
	    		<i class="icon-camera myicon-right"></i> <?php echo e(trans('misc.change_avatar'), false); ?>

	    		</button>
	    		<input type="file" name="photo" id="uploadAvatar" accept="image/*" style="visibility: hidden;">
    		</div>
      <?php endif; ?>

			</form><!-- *********** AVATAR ************* -->



		<!-- ***** FORM ***** -->
       <form action="<?php echo e(url('account'), false); ?>" method="post" name="form">

          	<input type="hidden" name="_token" value="<?php echo e(csrf_token(), false); ?>">

            <!-- ***** Form Group ***** -->
            <div class="form-group has-feedback">
            	<label class="font-default"><?php echo e(trans('misc.full_name_misc'), false); ?></label>
              <input type="text" class="form-control login-field custom-rounded" value="<?php echo e(e( $user->name ), false); ?>" name="full_name" placeholder="<?php echo e(trans('misc.full_name_misc'), false); ?>" title="<?php echo e(trans('misc.full_name_misc'), false); ?>" autocomplete="off">
             </div><!-- ***** Form Group ***** -->

			<!-- ***** Form Group ***** -->
            <div class="form-group has-feedback">
            	<label class="font-default"><?php echo e(trans('auth.email'), false); ?></label>
              <input type="email" class="form-control login-field custom-rounded" value="<?php echo e($user->email, false); ?>" name="email" placeholder="<?php echo e(trans('auth.email'), false); ?>" title="<?php echo e(trans('auth.email'), false); ?>" autocomplete="off">
         </div><!-- ***** Form Group ***** -->

         <!-- ***** Form Group ***** -->
            <div class="form-group has-feedback">
            	<label class="font-default"><?php echo e(trans('misc.country'), false); ?></label>
            	<select name="countries_id" class="form-control" >
                      		<option value=""><?php echo e(trans('misc.select_your_country'), false); ?></option>
                      	<?php $__currentLoopData = App\Models\Countries::orderBy('country_name')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option <?php if( $user->countries_id == $country->id ): ?> selected="selected" <?php endif; ?> value="<?php echo e($country->id, false); ?>"><?php echo e($country->country_name, false); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </select>
            	    </div><!-- ***** Form Group ***** -->

           <button type="submit" id="buttonSubmit" class="btn btn-block btn-lg btn-main custom-rounded"><?php echo e(trans('misc.save_changes'), false); ?></button>

       </form><!-- ***** END FORM ***** -->

		</div><!-- /COL MD -->

		<div class="col-md-4">
			<?php echo $__env->make('users.navbar-edit', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		</div>


 </div><!-- container -->

 <!-- container wrap-ui -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

<script type="text/javascript">

	//<<<<<<<=================== * UPLOAD AVATAR  * ===============>>>>>>>//
    $(document).on('change', '#uploadAvatar', function(){

    $('.wrap-loader').show();

   (function(){
	 $("#formAvatar").ajaxForm({
	 dataType : 'json',
	 success:  function(e){
	 if( e ){
        if( e.success == false ){
		$('.wrap-loader').hide();

		var error = '';
                        for($key in e.errors){
                        	error += '' + e.errors[$key] + '';
                        }
		swal({
    			title: "<?php echo e(trans('misc.error_oops'), false); ?>",
    			text: ""+ error +"",
    			type: "error",
    			confirmButtonText: "<?php echo e(trans('users.ok'), false); ?>"
    			});

			$('#uploadAvatar').val('');

		} else {

			$('#uploadAvatar').val('');
			$('.avatarUser').attr('src',e.avatar);
			$('.wrap-loader').hide();
		}

		}//<-- e
			else {
				$('.wrap-loader').hide();
				swal({
    			title: "<?php echo e(trans('misc.error_oops'), false); ?>",
    			text: '<?php echo e(trans("misc.error"), false); ?>',
    			type: "error",
    			confirmButtonText: "<?php echo e(trans('users.ok'), false); ?>"
    			});

				$('#uploadAvatar').val('');
			}
		   }//<----- SUCCESS
		}).submit();
    })(); //<--- FUNCTION %
});//<<<<<<<--- * ON * --->>>>>>>>>>>
//<<<<<<<=================== * UPLOAD AVATAR  * ===============>>>>>>>//
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>