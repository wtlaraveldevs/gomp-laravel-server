<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
           <h4>
           <?php echo e(trans('admin.admin'), false); ?> <i class="fa fa-angle-right margin-separator"></i> <?php echo e(trans('misc.withdrawals'), false); ?> #<?php echo e($data->id, false); ?>

          </h4>
        </section>

        <!-- Main content -->
        <section class="content">

        	<div class="row">
            <div class="col-xs-12">
              <div class="box">

              	<div class="box-body">
              		<dl class="dl-horizontal">

					  <!-- start -->
					  <dt>ID</dt>
					  <dd><?php echo e($data->id, false); ?></dd>
					  <!-- ./end -->

					  <!-- start -->
					  <dt><?php echo e(trans_choice('misc.campaigns_plural', 1), false); ?></dt>
					  <dd><a href="<?php echo e(url('campaign',$data->campaigns()->id), false); ?>" target="_blank"><?php echo e($data->campaigns()->title, false); ?> <i class="fa fa-external-link-square"></i></a></dd>
					  <!-- ./end -->

					<?php if( $data->gateway == 'Paypal' ): ?>
					  <!-- start -->
					  <dt><?php echo e(trans('admin.paypal_account'), false); ?></dt>
					  <dd><?php echo e($data->account, false); ?></dd>
					  <!-- ./end -->

					  <?php else: ?>
					   <!-- start -->
					  <dt><?php echo e(trans('misc.bank_details'), false); ?></dt>
					  <dd><?php echo App\Helper::checkText($data->account); ?></dd>
					  <!-- ./end -->

					  <?php endif; ?>

					  <!-- start -->
					  <dt><?php echo e(trans('admin.amount'), false); ?></dt>
					  <dd><strong class="text-success"><?php if($settings->currency_position == 'left'): ?><?php echo e($settings->currency_symbol.$data->amount, false); ?><?php else: ?><?php echo e($data->amount.$settings->currency_symbol, false); ?><?php endif; ?></strong></dd>
					  <!-- ./end -->

					  <!-- start -->
					  <dt><?php echo e(trans('misc.payment_gateway'), false); ?></dt>
					  <dd><?php echo e($data->gateway, false); ?></dd>
					  <!-- ./end -->


					  <!-- start -->
					  <dt><?php echo e(trans('admin.date'), false); ?></dt>
					  <dd><?php echo e(date($settings->date_format, strtotime($data->date)), false); ?></dd>
					  <!-- ./end -->

					  <!-- start -->
					  <dt><?php echo e(trans('admin.status'), false); ?></dt>
					  <dd>
					  	<?php if( $data->status == 'paid' ): ?>
                      	<span class="label label-success"><?php echo e(trans('misc.paid'), false); ?></span>
                      	<?php else: ?>
                      	<span class="label label-warning"><?php echo e(trans('misc.pending_to_pay'), false); ?></span>
                      	<?php endif; ?>
					  </dd>
					  <!-- ./end -->

					<?php if( $data->status == 'paid' ): ?>
					  <!-- start -->
					  <dt><?php echo e(trans('misc.date_paid'), false); ?></dt>
					  <dd>
					  	<?php echo e(date('d M, y', strtotime($data->date_paid)), false); ?>

					  </dd>
					  <!-- ./end -->
					  <?php endif; ?>



					</dl>
              	</div><!-- box body -->

              	<div class="box-footer">
                  	 <a href="<?php echo e(url('panel/admin/withdrawals'), false); ?>" class="btn btn-default"><?php echo e(trans('auth.back'), false); ?></a>

                 <?php if( $data->gateway == 'Paypal' ): ?>

                 <?php
                 if ( $settings->paypal_sandbox == 'true') {
					// SandBox
					$action = "https://www.sandbox.paypal.com/cgi-bin/webscr";
					} else {
					// Real environment
					$action = "https://www.paypal.com/cgi-bin/webscr";
					}

                 ?>

                 <form name="_xclick" action="<?php echo e($action, false); ?>" method="post" class="displayInline">
				        <input type="hidden" name="cmd" value="_xclick">
				        <input type="hidden" name="return" value="<?php echo e(url('panel/admin/withdrawals'), false); ?>">
				        <input type="hidden" name="cancel_return"   value="<?php echo e(url('panel/admin/withdrawals'), false); ?>">
				        <input type="hidden" name="notify_url" value="<?php echo e(url('paypal/withdrawal/ipn'), false); ?>">
				        <input type="hidden" name="currency_code" value="<?php echo e($settings->currency_code, false); ?>">
				        <input type="hidden" name="amount" id="amount" value="<?php echo e($data->amount, false); ?>">
				        <input type="hidden" name="custom" value="<?php echo e($data->id, false); ?>">
				        <input type="hidden" name="item_name" value="<?php echo e(trans('misc.payment_campaigning').' '.$data->campaigns()->title, false); ?>">
				        <input type="hidden" name="business" value="<?php echo e($data->account, false); ?>">
				        <button type="submit" class="btn btn-default pull-right"><i class="fa fa-paypal"></i> <?php echo e(trans('misc.paid_paypal'), false); ?></button>
				        </form>

	        	<?php endif; ?>

                  <?php if( $data->status == 'pending' ): ?>

                <?php echo Form::open([
			            'method' => 'POST',
			            'url' => "panel/admin/withdrawals/paid/$data->id",
			            'class' => 'displayInline'
				        ]); ?>


	            	<?php echo Form::submit(trans('misc.mark_paid'), ['class' => 'btn btn-success pull-right myicon-right']); ?>

	        	<?php echo Form::close(); ?>


	        	<?php endif; ?>

                  </div><!-- /.box-footer -->

              </div><!-- box -->
            </div><!-- col -->
         </div><!-- row -->

          <!-- Your Page Content Here -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>