<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('public/plugins/iCheck/all.css'), false); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
            <?php echo e(trans('admin.admin'), false); ?>

            	<i class="fa fa-angle-right margin-separator"></i>
            		<?php echo e(trans('misc.withdrawals'), false); ?> <?php echo e(trans('misc.configure'), false); ?>

          </h4>

        </section>

        <!-- Main content -->
        <section class="content">

          <?php if(session('error')): ?>
        			<div class="alert alert-danger btn-sm alert-fonts" role="alert">
        				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    		<?php echo e(session('error'), false); ?>

                    		</div>
                    	<?php endif; ?>

                    	<?php if(session('success')): ?>
        			<div class="alert alert-success btn-sm alert-fonts" role="alert">
        				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    		<?php echo e(session('success'), false); ?>

                    		</div>
                    	<?php endif; ?>

        	<div class="content">

        		<div class="row">

        	<div class="box box-danger">
                <div class="box-header with-border">
                  <h5>
                    <?php echo e(trans('misc.select_method_payment'), false); ?> - <strong><?php echo e(trans('misc.default_withdrawal'), false); ?></strong>: <?php if( Auth::user()->payment_gateway == '' ): ?> <?php echo e(trans('misc.unconfigured'), false); ?> <?php else: ?> <?php echo e(Auth::user()->payment_gateway, false); ?> <?php endif; ?>
                    </h5>
                    <h3><i class="fa fa-paypal myicon-right"></i> PayPal</h3>
                </div><!-- /.box-header -->

                <!-- form start -->
                <form class="form-horizontal" method="post" action="<?php echo e(url('withdrawals/configure/paypal'), false); ?>">

                	<input type="hidden" name="_token" value="<?php echo e(csrf_token(), false); ?>">

					<?php echo $__env->make('errors.errors-forms', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                     <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('admin.paypal_account'), false); ?></label>
                      <div class="col-sm-10">
                        <input type="text" value="<?php echo e(Auth::user()->paypal_account, false); ?>" id="email_paypal" name="email_paypal" class="form-control" placeholder="<?php echo e(trans('admin.paypal_account'), false); ?>">
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
               <div class="box-body">
                 <div class="form-group">
                   <label class="col-sm-2 control-label"><?php echo e(trans('misc.confirm_email'), false); ?></label>
                   <div class="col-sm-10">
                     <input type="text" value="<?php echo e(Auth::user()->paypal_account, false); ?>" name="email_paypal_confirmation" class="form-control" placeholder="<?php echo e(trans('misc.confirm_email'), false); ?>">
                   </div>
                 </div>
               </div><!-- /.box-body -->

                  <div class="box-footer">
                    <!-- <a href="<?php echo e(url('dashboard/withdrawals'), false); ?>" class="btn btn-default"><?php echo e(trans('admin.cancel'), false); ?></a> -->
                    <button type="submit" class="btn btn-success pull-right"><?php echo e(trans('misc.save'), false); ?></button>
                  </div><!-- /.box-footer -->
                </form>

<hr class="box box-danger" />
        
                <!-- Stripe Settings -->
                <form class="form-horizontal" method="post" action="<?php echo e(url('withdrawals/configure/paypal'), false); ?>">

                  <input type="hidden" name="_token" value="<?php echo e(csrf_token(), false); ?>">

          <?php echo $__env->make('errors.errors-forms', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                     <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('admin.paypal_account'), false); ?></label>
                      <div class="col-sm-10">
                        <input type="text" value="<?php echo e(Auth::user()->paypal_account, false); ?>" id="email_paypal" name="email_paypal" class="form-control" placeholder="<?php echo e(trans('admin.paypal_account'), false); ?>">
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                 <div class="box-body">
                   <div class="form-group">
                     <label class="col-sm-2 control-label"><?php echo e(trans('misc.confirm_email'), false); ?></label>
                     <div class="col-sm-10">
                       <input type="text" value="<?php echo e(Auth::user()->paypal_account, false); ?>" name="email_paypal_confirmation" class="form-control" placeholder="<?php echo e(trans('misc.confirm_email'), false); ?>">
                     </div>
                   </div>
                 </div><!-- /.box-body -->

                  <div class="box-footer">
                    <a href="https://connect.stripe.com/oauth/authorize?response_type=code&client_id=ca_B0RLtWAPNycHB7XQaaKyBBG8TKc7xLAT&scope=read_write" class="btn btn-success pull-right"><?php echo e(trans('misc.stripe_config'), false); ?></a>
                    <!-- <button type="submit" class="btn btn-success pull-right"><?php echo e(trans('misc.stripe_config'), false); ?></button> -->
                  </div><!-- /.box-footer -->
                </form>
<hr />
<form method="post" action="<?php echo e(url('withdrawals/configure/bank'), false); ?>">
  <input type="hidden" name="_token" value="<?php echo e(csrf_token(), false); ?>">
           <div class="box box-danger">
                <div class="box-header">
                  <h3 class="box-title"><i class="fa fa-university myicon-right"></i> <?php echo e(trans('misc.bank_transfer'), false); ?> </h3>
                </div><!-- /.box-header -->

                <!-- Start Box Body -->
                <div class="box-body">
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo e(trans('misc.bank_details'), false); ?></label>
                    <div class="col-sm-10">

                      <textarea name="bank"rows="5" cols="40" class="form-control" placeholder="<?php echo e(trans('misc.bank_details'), false); ?>"><?php echo e(Auth::user()->bank, false); ?></textarea>
                    </div>
                  </div>
                </div><!-- /.box-body -->

                  <div class="box-footer">

                    <!-- <a href="<?php echo e(url('dashboard/withdrawals'), false); ?>" class="btn btn-default"><?php echo e(trans('admin.cancel'), false); ?></a> -->
                    <button type="submit" class="btn btn-success pull-right"><?php echo e(trans('misc.save'), false); ?></button>
                  </div><!-- /.box-footer -->
                </form>
              </div>

        		</div><!-- /.row -->

        	</div><!-- /.content -->

          <!-- Your Page Content Here -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

	<!-- icheck -->
	<script src="<?php echo e(asset('public/plugins/iCheck/icheck.min.js'), false); ?>" type="text/javascript"></script>

	<script type="text/javascript">
		//Flat red color scheme for iCheck
        $('input[type="radio"]').iCheck({
          radioClass: 'iradio_flat-red'
        });

        var token = '<?php echo e(csrf_token(), false); ?>';
        var id = '<?php echo e(Auth::user()->id, false); ?>';

        $(document).ready(function(){
          var x = getURLParameters('code');
          if(x === "failed"){
            alert('No Parameter Found');
          }
          else{
            // alert("Auth User ID is " + id);
            // console.log(token);
            $.ajax({
              method: "POST",
              url: "http://localhost:8083/GOMP/api/activate/stripe",
              contentType: "application/json",
              Accept: 'application/json',
              data: JSON.stringify({ auth_code: x, user_id: id }),
              headers: {
                  // Set any custom headers here.
                  // If you set any non-simple headers, your server must include these
                  // headers in the 'Access-Control-Allow-Headers' response header.
                  'Accept': 'application/json',
              },
            })
            .done(function( data ) {
              var res = JSON.parse(data)
              // alert( "data: " + data );
              console.log(data);
            });
          }
        })



        function getURLParameters(paramName)
        {
            var sURL = window.document.URL.toString();
            if (sURL.indexOf("?") > 0)
            {
                var arrParams = sURL.split("?");
                var arrURLParams = arrParams[1].split("&");
                var arrParamNames = new Array(arrURLParams.length);
                var arrParamValues = new Array(arrURLParams.length);

                var i = 0;
                for (i = 0; i<arrURLParams.length; i++)
                {
                    var sParam =  arrURLParams[i].split("=");
                    arrParamNames[i] = sParam[0];
                    if (sParam[1] != "")
                        arrParamValues[i] = unescape(sParam[1]);
                    else
                        arrParamValues[i] = "No Value";
                }

                for (i=0; i<arrURLParams.length; i++)
                {
                    if (arrParamNames[i] == paramName)
                    {
                        //alert("Parameter:" + arrParamValues[i]);
                        return arrParamValues[i];
                    }
                }
                return "failed";
            }
        }

	</script>




<?php $__env->stopSection(); ?>

<?php echo $__env->make('users.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>