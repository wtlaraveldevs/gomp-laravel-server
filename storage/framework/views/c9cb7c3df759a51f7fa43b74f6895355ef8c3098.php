<?php

	$settings = App\Models\AdminSettings::first();

	// $percentage = round($response->donations()->sum('donation') / $response->goal * 100);

	/*if( $percentage > 100 ) {
		$percentage = 100;
	} else {
		$percentage = $percentage;
	}*/

	// All Donations
	// $donations = $response->donations()->orderBy('id','desc')->paginate(10);

	// Updates
	$updates = $response->updates()->orderBy('id','desc')->paginate(1);

	if( str_slug( $response->title ) == '' ) {
		$slug_url  = '';
	} else {
		$slug_url  = '/'.str_slug( $response->title );
	}

	if( Auth::check() ) {
		// LIKE ACTIVE
	   $likeActive = App\Models\Like::where( 'user_id', Auth::user()->id )
	   ->where('campaigns_id',$response->id)
	   ->where('status','1')
	   ->first();

       if( $likeActive ) {
       	  $textLike   = trans('misc.unlike');
		  $icoLike    = 'fa fa-heart';
		  $statusLike = 'active';
       } else {
       		$textLike   = trans('misc.like');
		    $icoLike    = 'fa fa-heart-o';
			$statusLike = '';
       }
	}

	// Deadline
	$timeNow = strtotime(Carbon\Carbon::now());

	if( $response->deadline != '' ) {
	    $deadline = strtotime($response->deadline);

		$date = strtotime($response->deadline);
	    $remaining = $date - $timeNow;

		$days_remaining = floor($remaining / 86400);
	}

?>


<?php $__env->startSection('title'); ?><?php echo e($response->title.' - ', false); ?><?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>

<meta property="og:type" content="website" />
<meta property="og:image:width" content="<?php echo e(App\Helper::getWidth('public/campaigns/large/'.$response->large_image), false); ?>"/>
<meta property="og:image:height" content="<?php echo e(App\Helper::getHeight('public/campaigns/large/'.$response->large_image), false); ?>"/>

<!-- Current locale and alternate locales -->
<meta property="og:locale" content="en_US" />
<meta property="og:locale:alternate" content="es_ES" />

<!-- Og Meta Tags -->
<link rel="canonical" href="<?php echo e(url("campaign/$response->id").'/'.str_slug($response->title), false); ?>"/>
<meta property="og:site_name" content="<?php echo e($settings->title, false); ?>"/>
<meta property="og:url" content="<?php echo e(url("campaign/$response->id").'/'.str_slug($response->title), false); ?>"/>
<meta property="og:image" content="<?php echo e(url('public/campaigns/large',$response->large_image), false); ?>"/>

<meta property="og:title" content="<?php echo e($response->title, false); ?>"/>
<meta property="og:description" content="<?php echo e(strip_tags($response->description), false); ?>"/>
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:image" content="<?php echo e(url('public/campaigns/large',$response->large_image), false); ?>" />
<meta name="twitter:title" content="<?php echo e($response->title, false); ?>" />
<meta name="twitter:description" content="<?php echo e(strip_tags($response->description), false); ?>"/>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="jumbotron md index-header jumbotron_set jumbotron-cover">
      <div class="container wrap-jumbotron position-relative">
      </div>
    </div>

<div class="container margin-bottom-40 padding-top-40">

	<?php if(session()->has('donation_cancel')): ?>
	<div class="alert alert-danger text-center btn-block margin-bottom-20  custom-rounded" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">×</span>
								</button>
			<i class="fa fa-remove myicon-right"></i> <?php echo e(trans('misc.donation_cancel'), false); ?>

		</div>

		<?php endif; ?>

			<?php if(session('donation_success')): ?>
	<div class="alert alert-success text-center btn-block margin-bottom-20  custom-rounded" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">×</span>
								</button>
			<i class="fa fa-check myicon-right"></i> <?php echo e(trans('misc.donation_success'), false); ?>

		</div>

		<?php endif; ?>


<!-- Col MD -->
<div class="col-md-8 margin-bottom-20">

	<div class="text-center margin-bottom-20">
		<img class="img-responsive img-rounded" style="display: inline-block;" src="<?php echo e(url('public/campaigns/large',$response->large_image), false); ?>" />
</div>

<h1 class="font-default title-image none-overflow margin-bottom-20">

	 		<?php echo e($response->title, false); ?>

		</h1>

		<hr />

		<div class="row margin-bottom-30">
			<div class="col-md-3">
				<a class="btn btn-block btn-facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo e(url('campaign',$response->id).'/'.str_slug($response->title), false); ?>" target="_blank"><i class="fa fa-facebook myicon-right"></i> <?php echo e(trans('misc.share'), false); ?> (<?php echo e(App\Helper::countFacebookShare(URL::current()), false); ?>)</a>
			</div>

			<div class="col-md-3">
				<a class="btn btn-twitter btn-block" href="https://twitter.com/intent/tweet?url=<?php echo e(url('campaign',$response->id), false); ?>&text=<?php echo e(e( $response->title ), false); ?>" data-url="<?php echo e(url('campaign',$response->id), false); ?>" target="_blank"><i class="fa fa-twitter myicon-right"></i> <?php echo e(trans('misc.tweet'), false); ?></a>
			</div>

		<div class="col-md-3">
				<a class="btn btn-google-plus btn-block" href="https://plus.google.com/share?url=<?php echo e(url('campaign',$response->id).'/'.str_slug($response->title), false); ?>" target="_blank"><i class="fa fa-google-plus myicon-right"></i> <?php echo e(trans('misc.share'), false); ?></a>
			</div>

			<div class="col-md-3">
				<a class="btn btn-default btn-block" data-toggle="modal" data-target="#embedModal" href="#"><i class="fa fa-code myicon-right"></i> <?php echo e(trans('misc.embed'), false); ?></a>
			</div>

			<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="embedModal">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header headerModal">
				        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				        <h5 class="modal-title">
				        	<?php echo e(trans('misc.embed_title'), false); ?>

				        </h5>
				     </div><!-- Modal header -->

				     <div class="modal-body">
	 								 <div class="form-group">
										 <input type="text" class="form-control" value="<div style='width:350px;'><script src='<?php echo e(url('c',$response->id), false); ?>/widget.js' type='text/javascript'></script></div>" readonly="readonly" id="embedCode">
                    </div><!-- /.form-group-->
				     </div>

						 <div class="modal-footer">
							 <button class="btn btn-default" id="btn_copy_code"><?php echo e(trans('misc.copy_code'), false); ?></button>
						 </div>

			    </div>
			  </div>
			</div>

		</div>

<ul class="nav nav-tabs nav-justified margin-bottom-20">
  <li class="active"><a href="#desc" aria-controls="home" role="tab" data-toggle="tab" class="font-default"><strong><?php echo e(trans('misc.story'), false); ?></strong></a></li>
	<li><a href="#donations" aria-controls="home" role="tab" data-toggle="tab" class="font-default"><strong><?php echo e(trans('misc.donations'), false); ?></strong> <span class="badge update-ico"><?php echo e(number_format($response->donations()->count()), false); ?></a></span></li>
	 		<li><a href="#updates" aria-controls="home" role="tab" data-toggle="tab" class="font-default"><strong><?php echo e(trans('misc.updates'), false); ?></strong> <span class="badge update-ico"><?php echo e(number_format($updates->total()), false); ?></span></a></li>
</ul>

<div class="tab-content">
		<?php if( $response->description != '' ): ?>
		<div role="tabpanel" class="tab-pane fade in active description wordBreak"id="desc">
		<?php echo $response->description; ?>

		</div>
		<?php endif; ?>

		<div role="tabpanel" class="tab-pane fade in description wordBreak"id="donations">

			<?php if( $response->donations()->count() == 0 ): ?>
				<span class="btn-block text-center">
		    			<i class="ion ion-cash ico-no-result"></i>
		    		</span>
				<span class="text-center btn-block"><?php echo e(trans('misc.no_donations'), false); ?></span>

			<?php else: ?>

				<ul class="list-group" id="listDonations">

				    <?php $__currentLoopData = $donations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $donation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

				    <?php
				    $letter = str_slug(mb_substr( $donation->fullname, 0, 1,'UTF8'));

					if( $letter == '' ) {
						$letter = 'N/A';
					}

					if( $donation->anonymous == 1 ) {
						$letter = 'N/A';
						$donation->fullname = trans('misc.anonymous');
					}
				    ?>

				     <?php echo $__env->make('includes.listing-donations', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

				       	 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

				       	 <?php echo e($donations->links('vendor.pagination.loadmore'), false); ?>


					</ul>

			<?php endif; ?>

		</div>

		<div role="tabpanel" class="tab-pane fade description wordBreak margin-top-30" id="updates">

		<?php if( $updates->total() == 0 ): ?>
			<span class="btn-block text-center">
	    			<i class="icon-history ico-no-result"></i>
	    		</span>
			<span class="text-center btn-block"><?php echo e(trans('misc.no_results_found'), false); ?></span>

			<?php else: ?>

			<?php $__currentLoopData = $updates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $update): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php echo $__env->make('includes.ajax-updates-campaign', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

				 <?php echo e($updates->links('vendor.pagination.loadmore'), false); ?>


			<?php endif; ?>

		</div>
</div>

<!-- <div class="btn-block margin-top-20">
	<div class="fb-comments"  data-width="100%" data-href="<?php echo e(url('campaign',$response->id).'/'.str_slug($response->title), false); ?>" data-numposts="5"></div>
</div>
 -->

 </div><!-- /COL MD -->

 <div class="col-md-4">

<?php if( Auth::check()
&&  isset($response->user()->id)
&& Auth::user()->id == $response->user()->id
&& !isset( $deadline )
&& $response->finalized == 0
): ?>
 	<div class="row margin-bottom-20">
		<div class="col-md-12">
			<a class="btn btn-lg btn-default btn-block margin-bottom-5" href="<?php echo e(url('rewards/campaign',$response->id), false); ?>"><i class="icon-gift"></i> <?php echo e(trans('misc.add_reward'), false); ?></a>
		</div>
			<div class="col-md-12">
				<a class="btn btn-success btn-block margin-bottom-5" href="<?php echo e(url('edit/campaign',$response->id), false); ?>"><?php echo e(trans('misc.edit_campaign'), false); ?></a>
			</div>
			<div class="col-md-12">
				<a class="btn btn-info btn-block margin-bottom-5" href="<?php echo e(url('update/campaign',$response->id), false); ?>"><?php echo e(trans('misc.post_an_update'), false); ?></a>
			</div>
			<?php if( $response->donations()->count() == 0 ): ?>
			<div class="col-md-12">
				<a href="#" class="btn btn-danger btn-block" id="deleteCampaign" data-url="<?php echo e(url('delete/campaign',$response->id), false); ?>"><?php echo e(trans('misc.delete_campaign'), false); ?></a>
			</div>
			<?php endif; ?>
		</div>

<?php elseif( Auth::check()
&&  isset($response->user()->id)
&& Auth::user()->id == $response->user()->id
&& isset( $deadline )
&& $deadline > $timeNow
&& $response->finalized == 0
): ?>
		<div class="row margin-bottom-20">
			<div class="col-md-12">
				<a class="btn btn-lg btn-default btn-block margin-bottom-5" href="<?php echo e(url('rewards/campaign',$response->id), false); ?>"><i class="icon-gift"></i> <?php echo e(trans('misc.add_reward'), false); ?></a>
			</div>
			<div class="col-md-12">
				<a class="btn btn-success btn-block margin-bottom-5" href="<?php echo e(url('edit/campaign',$response->id), false); ?>"><?php echo e(trans('misc.edit_campaign'), false); ?></a>
			</div>
			<div class="col-md-12">
				<a class="btn btn-info btn-block margin-bottom-5" href="<?php echo e(url('update/campaign',$response->id), false); ?>"><?php echo e(trans('misc.post_an_update'), false); ?></a>
			</div>
			<?php if( $response->donations()->count() == 0 ): ?>
			<div class="col-md-12">
				<a href="#" class="btn btn-danger btn-block" id="deleteCampaign" data-url="<?php echo e(url('delete/campaign',$response->id), false); ?>"><?php echo e(trans('misc.delete_campaign'), false); ?></a>
			</div>
			<?php endif; ?>
		</div>

		<?php endif; ?>

<?php if( isset($response->user()->id) ): ?>
<!-- Start Panel -->
 	<div class="panel panel-default panel-transparent">
	  <div class="panel-body">
	    <div class="media none-overflow">
			  <div class="media-center margin-bottom-5">
			      <img class="img-circle center-block" src="<?php echo e(url('public/avatar',$response->user()->avatar), false); ?>" width="60" height="60" >
			  </div>
			  <div class="media-body text-center">

			    	<h4 class="media-heading">
			    		<?php echo e($response->user()->name, false); ?>


			    	<?php if( Auth::guest() || Auth::check() && Auth::user()->id != $response->user()->id ): ?>
			    		<a href="#" title="<?php echo e(trans('misc.contact_organizer'), false); ?>" data-toggle="modal" data-target="#sendEmail">
			    				<i class="fa fa-envelope myicon-right"></i>
			    		</a>
			    		<?php endif; ?>
			    		</h4>

			    <small class="media-heading text-muted btn-block margin-zero"><?php echo e(trans('misc.created'), false); ?> <?php echo e(date($settings->date_format, strtotime($response->date) ), false); ?></small>
			    <?php if( $response->location != '' ): ?>
			    <small class="media-heading text-muted btn-block"><i class="fa fa-map-marker myicon-right"></i> <?php echo e($response->location, false); ?></small>
			    <?php endif; ?>
			    			  </div>
			</div>
	  </div>
	</div><!-- End Panel -->

<?php if( isset( $deadline ) && $deadline > $timeNow && $response->finalized == 0 ): ?>
 	<div class="btn-group btn-block margin-bottom-20 <?php if( Auth::check() && Auth::user()->id == $response->user()->id ): ?> display-none <?php endif; ?>">
		<a href="<?php echo e(url('donate/'.$response->id.$slug_url), false); ?>" class="btn btn-main btn-donate btn-lg btn-block custom-rounded">
			<?php echo e(trans('misc.donate_now'), false); ?>

			</a>
		</div>

		<?php elseif( !isset( $deadline ) && $response->finalized == 0 ): ?>
		<div class="btn-group btn-block margin-bottom-20 <?php if( Auth::check() && Auth::user()->id == $response->user()->id ): ?> display-none <?php endif; ?>">
		<a href="<?php echo e(url('donate/'.$response->id.$slug_url), false); ?>" class="btn btn-main btn-donate btn-lg btn-block custom-rounded">
			<?php echo e(trans('misc.donate_now'), false); ?>

			</a>
		</div>

		<?php else: ?>

		<div class="alert btnDisabled text-center btn-block margin-bottom-20  custom-rounded" role="alert">
			<i class="fa fa-lock myicon-right"></i> <?php echo e(trans('misc.campaign_ended'), false); ?>

		</div>

		<?php endif; ?>

<?php else: ?>
<div class="alert btnDisabled text-center btn-block margin-bottom-20  custom-rounded" role="alert">
			<i class="fa fa-lock myicon-right"></i> <?php echo e(trans('misc.campaign_ended'), false); ?>

		</div>
<?php endif; ?>


	<div class="panel panel-default">
		<div class="panel-body text-center">
	<?php if( Auth::check() ): ?>
		<a href="#" class="btnLike likeButton <?php echo e($statusLike, false); ?>" data-id="<?php echo e($response->id, false); ?>" data-like="<?php echo e(trans('misc.like'), false); ?>" data-unlike="<?php echo e(trans('misc.unlike'), false); ?>">
			<h3 class="btn-block text-center margin-zero"><i class="<?php echo e($icoLike, false); ?>"></i> <span id="countLikes"><?php echo e(App\Helper::formatNumber($response->likes()->count()), false); ?></span></h3>
		</a>
		<?php else: ?>

		<a href="<?php echo e(url('login'), false); ?>" class="btnLike">
			<h3 class="btn-block text-center margin-zero"><i class="fa fa-heart-o"></i> <span id="countLikes"><?php echo e(App\Helper::formatNumber($response->likes()->count()), false); ?></span></h3>
		</a>

		<?php endif; ?>
	   </div>
	</div>

	<?php if( $response->deadline != '' && $response->finalized == 0 ): ?>
		<!-- Start Panel -->
	<div class="panel panel-default">
		<div class="panel-body">
			<h4 class="margin-zero text-center" data-date="<?php echo e(date('M d, Y', strtotime($response->deadline) ), false); ?>">

			<?php if( $days_remaining > 0 ): ?>
				<i class="fa fa-clock-o myicon-right"></i>
				<strong><?php echo e($days_remaining, false); ?> <?php echo e(trans('misc.days_left'), false); ?></strong>
				<?php elseif( $days_remaining == 0 ): ?>

				  <i class="fa fa-clock-o myicon-right"></i>
				<strong><?php echo e(trans('misc.last_day'), false); ?></strong>

				<?php else: ?>

				 <i class="fa fa-lock myicon-right"></i>
				<strong><?php echo e(trans('misc.no_time_anymore'), false); ?></strong>

				<?php endif; ?>

				</h4>
		</div>
	</div><!-- End Panel -->
	<?php endif; ?>

	<?php if( $response->featured == 1 ): ?>
		<!-- Start Panel -->
	<div class="panel panel-default">
		<div class="panel-body text-center">
			<h4 class="margin-zero font-default"><i class="fa fa-trophy myicon-right featured-icon"></i> <strong><?php echo e(trans('misc.featured_campaign'), false); ?></strong></h4>
		</div>
	</div><!-- End Panel -->
	<?php endif; ?>

	<!-- Start Panel -->
	<!-- <div class="panel panel-default">
		<div class="panel-body">
			<h3 class="btn-block margin-zero" style="line-height: inherit;">
				<strong class="font-default">
				</h3>


				<?php if( $response->categories_id != '' ): ?>
				<?php if( isset( $response->category->id ) && $response->category->mode == 'on' ): ?>
				<small class="btn-block">
					<a href="<?php echo e(url('category',$response->category->slug), false); ?>" title="<?php echo e($response->category->name, false); ?>">
						<i class="icon-tag myicon-right"></i> <?php echo e(str_limit($response->category->name, 18, '...'), false); ?>

						</a>
				</small>
				<?php endif; ?>
				<?php endif; ?>
		</div>
	</div> --><!-- End Panel -->

	<div class="input-group margin-bottom-20">
        <input type="text" readonly="readonly" id="url_campaign" class="form-control" value="<?php echo e($response->token_id, false); ?>"> 
        <!--URL::current()-->
        <div class="input-group-btn">
            <button class="btn btn-default" id="btn_campaign_url"><?php echo e(trans('misc.copy_link'), false); ?></button>
        </div>
    </div>

<?php if( isset( $deadline ) && $deadline > $timeNow && $response->finalized == 0
|| !isset( $deadline ) && $response->finalized == 0 ): ?>

<?php if( $response->rewards->count() != 0 ): ?>
	<?php $__currentLoopData = $response->rewards; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $reward): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<?php

				$pledge = '?pledge='.$reward->id;

				if( str_slug( $response->title ) == '' ) {

						$slugUrl  = $pledge;
					} else {
						$slugUrl  = '/'.str_slug( $response->title ).$pledge;
					}

				$pledgeClaimed = $response->donations()->where('rewards_id',$reward->id)->count();

				if( $pledgeClaimed < $reward->quantity ) {
					$url_campaign = url('donate/'.$response->id.$slugUrl);
				} else {
					$url_campaign = 'javascript:void(0);';
				}

		 ?>
	<!-- Start Panel -->
	<a href="<?php if( Auth::check() && Auth::user()->id == $response->user_id ): ?> <?php echo e(url('edit/rewards',$reward->id), false); ?> <?php else: ?> <?php echo e($url_campaign, false); ?> <?php endif; ?>" class="selectReward">
		<span class="cardSelectRewardBox">
			<span class="cardSelectReward">
				<span class="cardSelectRewardText">
					<?php if( Auth::check() && Auth::user()->id == $response->user_id ): ?>
				 <i class="fa fa-pencil"></i>	<?php echo e(trans('misc.edit_reward'), false); ?>

			 <?php elseif($pledgeClaimed < $reward->quantity): ?>
					<?php echo e(trans('misc.select_reward'), false); ?>

				<?php else: ?>
					<?php echo e(trans('misc.sold'), false); ?>

				<?php endif; ?>
				</span>
			</span>
		</span>


	<div class="panel panel-default">
 		<div class="panel-body">

			<h3 class="btn-block margin-zero" style="line-height: inherit;">
				<small><?php echo e(trans('misc.pledge'), false); ?></small>
				<strong class="font-default"><?php echo e(App\Helper::amountFormat($reward->amount), false); ?></strong>
				</h3>
				<h4><?php echo e($reward->title, false); ?></h4>
				<p class="wordBreak">
					<?php echo e($reward->description, false); ?>

				</p>
				<small class="btn-block margin-bottom-10 <?php if( $pledgeClaimed == $reward->quantity ): ?>text-danger <?php else: ?> text-muted <?php endif; ?>">
					<?php echo e(trans('misc.reward_claimed', ['claim' => $pledgeClaimed, 'total' => $reward->quantity]), false); ?> <?php if( $pledgeClaimed == $reward->quantity ): ?> <span class="label label-danger"><?php echo e(trans('misc.sold'), false); ?></span> <?php endif; ?>
				</small>

				<small class="btn-block text-muted">
					<?php echo e(trans('misc.delivery'), false); ?>:
				</small>
				<strong><?php echo e(date('F, Y', strtotime($reward->delivery)), false); ?></strong>
		</div><!-- panel-body -->
	</div><!-- End Panel -->
		</a>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>

<?php endif; ?> 

	<?php if( Auth::check() &&  isset($response->user()->id) && Auth::user()->id != $response->user()->id  ): ?>
	<div class="btn-block text-center">
		<a href="<?php echo e(url('report/campaign', $response->id), false); ?>/<?php echo e($response->user()->id, false); ?>"><i class="icon-warning myicon-right"></i> <?php echo e(trans('misc.report'), false); ?></a>
	</div>
	<?php endif; ?>

<?php if(  isset($response->user()->id) ): ?>
	<div class="modal fade" id="sendEmail" tabindex="-1" role="dialog" aria-hidden="true">
	     		<div class="modal-dialog modalContactOrganizer">
	     			<div class="modal-content">
	     				<div class="modal-header headerModal headerModalOverlay position-relative" style="background-image: url('<?php echo e(url('public/campaigns/large',$response->large_image), false); ?>')">
					        <button type="button" class="close closeLight position-relative" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

					        <span class="btn-block margin-top-15 margin-bottom-15 text-center position-relative">
								      <img class="img-circle" src="<?php echo e(url('public/avatar/',$response->user()->avatar), false); ?>" width="80" height="80" >
								    </span>

							<h5 class="modal-title text-center font-default position-relative" id="myModalLabel">
					        	<?php echo e($response->user()->name, false); ?>

					        	</h5>

					        <h4 class="modal-title text-center font-default position-relative" id="myModalLabel">
					        	<?php echo e(trans('misc.contact_organizer'), false); ?>

					        	</h4>
					     </div><!-- Modal header -->

					      <div class="modal-body listWrap text-center center-block modalForm">

					    <!-- form start -->
				    <form method="POST" class="margin-bottom-15" action="<?php echo e(url('contact/organizer'), false); ?>" enctype="multipart/form-data" id="formContactOrganizer">
				    	<input type="hidden" name="_token" value="<?php echo e(csrf_token(), false); ?>">
				    	<input type="hidden" name="id" value="<?php echo e($response->user()->id, false); ?>">

					    <!-- Start Form Group -->
	                    <div class="form-group">
	                    	<input type="text" required="" name="name" class="form-control" placeholder="<?php echo e(trans('users.name'), false); ?>">
	                    </div><!-- /.form-group-->

	                    <!-- Start Form Group -->
	                    <div class="form-group">
	                    	<input type="text" required="" name="email" class="form-control" placeholder="<?php echo e(trans('auth.email'), false); ?>">
	                    </div><!-- /.form-group-->

	                    <!-- Start Form Group -->
	                    <div class="form-group">
	                    	<textarea name="message" rows="4" class="form-control" placeholder="<?php echo e(trans('misc.message'), false); ?>"></textarea>
	                    </div><!-- /.form-group-->

	                    <!-- Alert -->
	                    <div class="alert alert-danger display-none" id="dangerAlert">
								<ul class="list-unstyled text-left" id="showErrors"></ul>
							</div><!-- Alert -->

	              <button type="submit" class="btn btn-lg btn-main custom-rounded" id="buttonFormSubmit"><?php echo e(trans('misc.send_message'), false); ?></button>
	             </form>

	             <!-- Alert -->
	             <div class="alert alert-success display-none" id="successAlert">
								<ul class="list-unstyled" id="showSuccess"></ul>
							</div><!-- Alert -->

					      </div><!-- Modal body -->
	     				</div><!-- Modal content -->
	     			</div><!-- Modal dialog -->
	     		</div><!-- Modal -->
     		<?php endif; ?>

 </div><!-- /COL MD -->

 </div><!-- container wrap-ui -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script type="text/javascript">

$("#embedCode,#url_campaign").click(function() {
	var $this = $(this);
    $this.select();
		});

textTruncate('#desc', ' <?php echo e(trans("misc.view_more"), false); ?>');

$(document).on('click','#updates .loadPaginator', function(r){
	r.preventDefault();
	 $(this).remove();
			$('<a class="list-group-item text-center loadMoreSpin"><i class="fa fa-circle-o-notch fa-spin fa-1x fa-fw"></i></a>').appendTo( "#updates" );

			var page = $(this).attr('href').split('page=')[1];
			$.ajax({
				url: '<?php echo e(url("ajax/campaign/updates"), false); ?>?id=<?php echo e($response->id, false); ?>&page=' + page
			}).done(function(data){
				if( data ) {
					$('.loadMoreSpin').remove();

					$( data ).appendTo( "#updates" );
					jQuery(".timeAgo").timeago();
					Holder.run({images:".holderImage"})
				} else {
					bootbox.alert( "<?php echo e(trans('misc.error'), false); ?>" );
				}
				//<**** - Tooltip
			});
	});

$(document).on('click','#listDonations .loadPaginator', function(e){
			e.preventDefault();
			$(this).remove();
			//$('<li class="list-group-item position-relative spinner spinnerList loadMoreSpin"></li>').appendTo( "#listDonations" )
			$('<a class="list-group-item text-center loadMoreSpin"><i class="fa fa-circle-o-notch fa-spin fa-1x fa-fw"></i></a>').appendTo( "#listDonations" );

			var page = $(this).attr('href').split('page=')[1];
			$.ajax({
				url: '<?php echo e(url("ajax/donations"), false); ?>?id=<?php echo e($response->id, false); ?>&page=' + page
			}).done(function(data){
				if( data ) {
					$('.loadMoreSpin').remove();

					$( data ).appendTo( "#listDonations" );
					jQuery(".timeAgo").timeago();
					Holder.run({images:".holderImage"})
				} else {
					bootbox.alert( "<?php echo e(trans('misc.error'), false); ?>" );
				}
				//<**** - Tooltip
			});
		});

		<?php if( Auth::check() ): ?>

$("#deleteCampaign").click(function(e) {
   	e.preventDefault();

   	var element = $(this);
	var url     = element.attr('data-url');

	element.blur();

	swal(
		{   title: "<?php echo e(trans('misc.delete_confirm'), false); ?>",
		 text: "<?php echo e(trans('misc.confirm_delete_campaign'), false); ?>",
		  type: "warning",
		  showLoaderOnConfirm: true,
		  showCancelButton: true,
		  confirmButtonColor: "#DD6B55",
		   confirmButtonText: "<?php echo e(trans('misc.yes_confirm'), false); ?>",
		   cancelButtonText: "<?php echo e(trans('misc.cancel_confirm'), false); ?>",
		    closeOnConfirm: false,
		    },
		    function(isConfirm){
		    	 if (isConfirm) {
		    	 	window.location.href = url;
		    	 	}
		    	 });

		 });

		 <?php if(session('noty_error')): ?>
    		swal({
    			title: "<?php echo e(trans('misc.error_oops'), false); ?>",
    			text: "<?php echo e(trans('misc.already_sent_report'), false); ?>",
    			type: "error",
    			confirmButtonText: "<?php echo e(trans('users.ok'), false); ?>"
    			});
   		 <?php endif; ?>

   		 <?php if(session('noty_success')): ?>
    		swal({
    			title: "<?php echo e(trans('misc.thanks'), false); ?>",
    			text: "<?php echo e(trans('misc.send_success'), false); ?>",
    			type: "success",
    			confirmButtonText: "<?php echo e(trans('users.ok'), false); ?>"
    			});
   		 <?php endif; ?>

		<?php endif; ?>

		// Copy Code Embed
		$(document).on('click','#btn_copy_code', function(){
						copyToClipboard('#embedCode',this);
				});
				// Copy Link Campaign
				$('#btn_campaign_url').click(function(){
                copyToClipboard('#url_campaign',this);
            });



				function copyToClipboard(element,btn) {
            var $temp = $('<input>');
            $("body").append($temp);
            $temp.val($(element).val()).select();
						$(element).select().focus();
            document.execCommand("copy");
						$(btn).html('<i class="fa fa-check"></i> <?php echo e(trans('misc.copied'), false); ?>').addClass('btn btn-success');
						//$('.popout').addClass('popout').html("Campaign URL has been copied.").fadeIn(500).delay(5000).fadeOut();
            $temp.remove();
		        }

		$('.selectReward').hover(

	   function () {
	      $(this).find('.cardSelectRewardBox').fadeIn();
	   },

	   function () {
	      $(this).find('.cardSelectRewardBox').fadeOut();
	   }
	);

</script>

<?php $__env->stopSection(); ?>
<?php session()->forget('donation_cancel') ?>
<?php session()->forget('donation_success') ?>

<?php echo $__env->make('app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>