<html>
      <body>
         <form action='' method='post'>
              <?php echo csrf_field(); ?>
                      <?php if($errors->any()): ?>
              <ul>
             <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <li> <?php echo e($error, false); ?> </li>
             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>

        <?php if( session( 'success' ) ): ?>
             <?php echo e(session( 'success' ), false); ?>

        <?php endif; ?>

             <label>Phone numbers (seperate with a comma [,])</label>
             <input type='text' name='numbers' />

            <label>Message</label>
            <textarea name='message'></textarea>

            <button type='submit'>Send!</button>
      </form>
    </body>
</html>