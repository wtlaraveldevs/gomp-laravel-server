<?php $settings = App\Models\AdminSettings::first(); ?>


<?php $__env->startSection('content'); ?> 
<div class="jumbotron md index-header jumbotron_set jumbotron-cover">
  	<div class="container wrap-jumbotron position-relative">
    	<h2 class="title-site">Successful</h2>
  	</div>
</div>

<div class="container margin-bottom-40">
	
	<!-- Col MD -->
	<div class="col-md-12 margin-top-20 margin-bottom-20">	
	     			    
	  	<div class="btn-block text-center">
			<i class="icon-search ico-no-result"></i>
		</div>
	    		
		<h3 class="margin-top-none text-center no-result no-result-mg">
			<?php echo e(trans('misc.no_results_found'), false); ?>

		</h3>
	    	
    </div><!-- /COL MD -->
</div><!-- container wrap-ui -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>