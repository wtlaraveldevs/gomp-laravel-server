<?php 

$settings = App\Models\AdminSettings::first(); 
$loggedIn = Auth::check();

?>
<!DOCTYPE html>
<html lang="en" style="overflow: hidden;">
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token(), false); ?>">
    <meta name="description" content="<?php echo $__env->yieldContent('description_custom'); ?><?php echo e($settings->description); ?>">
    <meta name="keywords" content="<?php echo e($settings->keywords); ?>" />
    <link rel="shortcut icon" href="<?php echo e(asset('public/img/favicon.png')); ?>" />
    <meta name="u" content="<?php echo e(Auth::check(), false); ?>" />

	<title><?php $__env->startSection('title'); ?><?php echo $__env->yieldSection(); ?> <?php if( isset( $settings->title ) ): ?><?php echo e($settings->title); ?><?php endif; ?></title>

	<?php echo $__env->make('includes.css_general', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Raleway:100,600' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<?php echo $__env->yieldContent('css'); ?>

	<!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

</head>
<body>

	<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/<?php echo e(config('fb_app.lang'), false); ?>/sdk.js#xfbml=1&version=v2.8&appId=<?php echo e(config('fb_app.id'), false); ?>";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

	<div class="popout font-default"></div>
	<div class="wrap-loader">
		<i class="fa fa-cog fa-spin fa-3x fa-fw cog-loader"></i>
		<i class="fa fa-cog fa-spin fa-3x fa-fw cog-loader-small"></i>
	</div>


	<div class="navbar navbar-inverse navbar-px padding-top-10 padding-bottom-10">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">

          	 <?php if( isset( $totalNotify ) ) : ?>
        	<span class="notify"><?php echo $totalNotify; ?></span>
        	<?php endif; ?>

            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo e(url('/'), false); ?>">
          	<img src="<?php echo e(asset('public/img/log.png'), false); ?>" class="logo" />
          	</a>
        </div><!-- navbar-header -->

        <div class="navbar-collapse collapse">

        	<ul class="nav navbar-nav navbar-right">

        			<?php $__currentLoopData = \App\Models\Pages::where('show_navbar', '1')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $_page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					 	
					 	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        		<?php if( Auth::check() ): ?>

        			<li class="dropdown">
			          <a href="javascript:void(0);" data-toggle="dropdown" class="userAvatar myprofile dropdown-toggle"> <!-- "<?php echo e(asset('public/avatar').'/'.Auth::user()->avatar, false); ?>"-->
			          		<img src="<?php echo e(asset('public/avatar').'/'.Auth::user()->avatar, false); ?>" alt="User" class="img-circle avatarUser" width="36" height="36" />
			          		<span class="title-dropdown font-default"><strong><?php echo e(trans('users.my_profile'), false); ?></strong></span>
			          		<i class="ion-chevron-down margin-lft5"></i>
			          	</a>

			          <!-- DROPDOWN MENU -->
			          <ul class="dropdown-menu arrow-up nav-session" role="menu" aria-labelledby="dropdownMenu4">
	          		 <?php if( Auth::user()->role == 'admin' ): ?>
	          		 	<li>
	          		 		<a href="<?php echo e(url('panel/admin'), false); ?>" class="text-overflow">
	          		 			<i class="icon-cogs myicon-right"></i> <?php echo e(trans('admin.admin'), false); ?></a>
	          		 			</li>
                      <li role="separator" class="divider"></li>
	          		 			<?php endif; ?>

	          		 			<li>
	          		 			<a href="<?php echo e(url('dashboard'), false); ?>" class="text-overflow">
	          		 				<i class="icon icon-dashboard myicon-right"></i> <?php echo e(trans('admin.dashboard'), false); ?>

	          		 				</a>
	          		 			</li>

                      <li>
	          		 			<a href="<?php echo e(url('dashboard/campaigns'), false); ?>" class="text-overflow">
	          		 			<i class="ion ion-speakerphone myicon-right"></i> <?php echo e(trans('misc.campaigns'), false); ?>

	          		 				</a>
	          		 			</li>

	          		 			<li>
	          		 			<a href="<?php echo e(url('user/likes'), false); ?>" class="text-overflow">
	          		 				<i class="fa fa-heart myicon-right"></i> <?php echo e(trans('misc.likes'), false); ?>

	          		 				</a>
	          		 			</li>

	          		 			<li>
	          		 			<a href="<?php echo e(url('account'), false); ?>" class="text-overflow">
	          		 				<i class="glyphicon glyphicon-cog myicon-right"></i> <?php echo e(trans('users.account_settings'), false); ?>

	          		 				</a>
	          		 			</li>

	          		 		<li>
	          		 			<a href="<?php echo e(url('logout'), false); ?>" class="logout text-overflow">
	          		 				<i class="glyphicon glyphicon-log-out myicon-right"></i> <?php echo e(trans('users.logout'), false); ?>

	          		 			</a>
	          		 		</li>
	          		 	</ul>
	          		</li>
					<?php else: ?>
        	  <?php endif; ?>
          </ul>

         </div>
     </div>
 	</div>

<div id="search">
    <button type="button" class="close">×</button>
    <form autocomplete="off" action="<?php echo e(url('search'), false); ?>" method="get">
        <input type="search" value="" name="q" id="btnItems" placeholder="<?php echo e(trans('misc.search_query'), false); ?>" />
        <button type="submit" class="btn btn-lg no-shadow btn-trans custom-rounded btn_search"  id="btnSearch"><?php echo e(trans('misc.search'), false); ?></button>
    </form>
</div>

<div class="modal" id="giveModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document" style="width: 25%;">
    <div class="modal-content">
      <div class="modal-header" style="background: #fbedbf; border: none;">
        <!-- <h5 class="modal-title">Modal title</h5> -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="background: #fbedbf;">
      	<div style="background: url('../GOMP/public/img/goldpot.png') no-repeat;background-size: 100% 100%;height: 320px;width: 100%; text-align: center; padding-top: 60%; padding-left: 15%">
        	<input class="form-control" style="width: 80%" type="search" id="query" value="" name="query"  placeholder="Unique GOMP ID" />
        </div>
        <div class="modal-footer">
	        <button type="button" onclick="findID();" class="btn btn-primary">Find</button>
	    </div>
      </div>
    </div>
  </div>
</div>

<div class="modal" id="buyModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document" style="width: 25%;">
    <div class="modal-content">
      <div class="modal-header" style="background: #fbedbf; border: none;">
        <!-- <h5 class="modal-title">Modal title</h5> -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="background: #fbedbf;">
      	<h4>To start receiving Gifts Of Money, buy a £2 GOMP and we will send you your Unique GOMP ID for you to share with your contacts</h4>
      	<div style="background: url('../GOMP/public/img/goldpot.png') no-repeat;background-size: 100% 100%;height: 180px;width: 50%; text-align: center; padding-top: 22%; margin: 0 auto">
        	<!-- <input class="form-control" style="width: 80%" type="search" id="query" value="" name="query"  placeholder="Unique GOMP ID" /> -->
        	<h3 style="color:white">GOMP <br /> £2</h3>
        </div>
        <div class="modal-footer" style="text-align: center;">
	        <button type="button" onclick="initiatePayment();" id="buttonPurchase" class="btn btn-primary" style="width: 45">Purchase</button>
	    </div>
      </div>
    </div>
  </div>
</div>


	<div class="jumbotron index-header jumbotron_set jumbotron-cover" style="height: 100%; padding: 100px 0;">  <!-- <?php if( Auth::check() ): ?> session-active-cover <?php endif; ?> -->
		<!-- <h1 class="title-site txt-left" id="titleSite"><?php echo e($settings->welcome_text, false); ?></h1> -->
      <div class="container wrap-jumbotron position-relative">
        <div class="row">
        	
        	<div class="parentHover" >
        		<a href="#giveModal" data-toggle="modal"  class="text-uppercase font-default button-hover" style="color: white;font-weight: bold; padding: 20% 36%">
					Give
				
        		</a>
        	</div>
        	<div class="parentHover">
        		<!-- href="<?php echo e(url('create/campaign'), false); ?>" -->
        		<a id="btnReceive"  class="text-uppercase font-default button-hover" style="color: white;font-weight: bold;padding: 20% 25%">
					Receive
				</a>
        	</div>
        	<div class="parentHover" style="padding-top: 1%;">
        		<a class="log-in custom-rounded text-uppercase font-default button-hover" href="<?php echo e(url('register'), false); ?>" style="color: white;font-weight: bold;">
					Send a message
				</a>
        	</div>
        	<div class="parentHover">
        		<a class="log-in custom-rounded text-uppercase font-default button-hover" href="<?php echo e(url('register'), false); ?>" style="color: white;font-weight: bold;">
					Register
				</a>
        	</div>
        	<!-- <div style="background: #FCC438;width: 10%;padding-left: 2%;padding-right: 2%;padding-top: 1%;padding-bottom: 1%;border-radius: 6px; margin-bottom: 1%;text-align: center;">
        		<a class="log-in custom-rounded text-uppercase font-default" href="<?php echo e(url('register'), false); ?>" style="color: white;font-weight: bold;">
					Get my money
				</a>
        	</div> -->
        	<div class="parentHover">
        		<a href="#search"  class="text-uppercase font-default button-hover" style="color: white;font-weight: bold; padding: 20% 28%">
					Search
				</a>
        	</div>
        	<!-- <div style="background: #FCC438;width: 10%;padding-left: 2%;padding-right: 2%;padding-top: 1%;padding-bottom: 1%;border-radius: 6px; margin-bottom: 1%;text-align: center;">
        		<a class="log-in custom-rounded text-uppercase font-default" href="<?php echo e(url('register'), false); ?>" style="color: white;font-weight: bold;">
					Thank you
				</a>
        	</div> -->
        	<div class="parentHover">
        		<a class="log-in custom-rounded text-uppercase font-default button-hover" href="<?php echo e(url('register'), false); ?>" style="color: white;font-weight: bold; padding: 20% 36%">
					Fee
				</a>
        	</div>
        </div>
        <!-- <p class="subtitle-site txt-left"><strong><?php echo e($settings->welcome_subtitle, false); ?></strong></p> -->
      </div><!-- container wrap-jumbotron -->
	</div><!-- jumbotron -->

		<?php echo $__env->make('includes.javascript_general', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<script src="https://checkout.stripe.com/checkout.js"></script>
	<script src="<?php echo e(asset('public/plugins/jquery.counterup/waypoints.min.js'), false); ?>"></script>
	<script src="<?php echo e(asset('public/plugins/jquery.counterup/jquery.counterup.min.js'), false); ?>"></script>

		<script type="text/javascript">

		$user_id = '';

		$(document).on('click','#campaigns .loadPaginator', function(r){
			r.preventDefault();
			 $(this).remove();
			 $('.loadMoreSpin').remove();
					$('<div class="col-xs-12 loadMoreSpin"><a class="list-group-item text-center"><i class="fa fa-circle-o-notch fa-spin fa-1x fa-fw"></i></a></div>').appendTo( "#campaigns" );

					var page = $(this).attr('href').split('page=')[1];
					$.ajax({
						url: '<?php echo e(url("ajax/campaigns"), false); ?>?page=' + page
					}).done(function(data){
						if( data ) {
							$('.loadMoreSpin').remove();

							$( data ).appendTo( "#campaigns" );
						} else {
							bootbox.alert( "<?php echo e(trans('misc.error'), false); ?>" );
						}
						//<**** - Tooltip
					});
			});

		jQuery(document).ready(function( $ ) {
			$('.counter').counterUp({
			delay: 10, // the delay time in ms
			time: 1000 // the speed time in ms
			});
		});

		 <?php if(session('success_verify')): ?>
    		swal({
    			title: "<?php echo e(trans('misc.welcome'), false); ?>",
    			text: "<?php echo e(trans('users.account_validated'), false); ?>",
    			type: "success",
    			confirmButtonText: "<?php echo e(trans('users.ok'), false); ?>"
    			});
   		 <?php endif; ?>

   		 <?php if(session('error_verify')): ?>
    		swal({
    			title: "<?php echo e(trans('misc.error_oops'), false); ?>",
    			text: "<?php echo e(trans('users.code_not_valid'), false); ?>",
    			type: "error",
    			confirmButtonText: "<?php echo e(trans('users.ok'), false); ?>"
    			});
   		 <?php endif; ?>

   		 // get value from ID
   		 // make url of search <- string
   		 // redirect to search
   		 //window.location.href = "http://localhost:8083/GOMP/search?q=db";
   		 function findID()
   		 {
   		 	var query = $('#query').val();
   		 	if(query === '' || query == null)
   		 	{
   		 		return;
   		 	}
   		 	window.location.href = "http://localhost:8083/GOMP/search?q="+query;
   		 }

   		 $(document).ready(function(){

   		 	$('#btnReceive').click(function(e){
   		 		const metas = document.getElementsByTagName('meta');

				for (let i = 0; i < metas.length; i++) {
				    if (metas[i].getAttribute('name') === 'u') {
				      if(metas[i].getAttribute('content') == '')
				      {
				      	window.location.href = "http://localhost:8083/GOMP/login"
				      }
				      else{
				      	$user_id = metas[i].getAttribute('content');
				      	$("#buyModal").modal();
				      }
				    }
				}
   		 	})
   		 })

   		 function initiatePayment()
   		 {
   		 	$.ajax({
			    type: "POST",
			    url: URL_BASE+"/api/purchase",
			    dataType: 'json',
			    contentType: "application/json",
              	Accept: 'application/json',
              	data: JSON.stringify({ "user_id": $user_id }),
              	headers: {
                  // Set any custom headers here.
                  // If you set any non-simple headers, your server must include these
                  // headers in the 'Access-Control-Allow-Headers' response header.
                  'Accept': 'application/json',
              	},
			   success: function( result ){

			   	console.log(result);

			   	//===== SUCCESS =====//
				if( result.success != false && result.stripeTrue == true  ) {

					var handler = StripeCheckout.configure({
					    key: result.key,
					    locale: 'auto',
					    token: function(token) {
					      // You can access the token ID with `token.id`.
					      // Get the token ID to your server-side code for use.
					      var $input = $('<input type=hidden name=stripeToken />').val(token.id);
					      // $('#formDonation').append($input).submit();
					      initiateStripeTransaction(token.id);
					    }
					});

				    // Open Checkout with further options:
				    handler.open({
				      currency: result.currency,
				      amount: result.amount
				    });

				  	// Close Checkout on page navigation:
				  	$(window).on('popstate', function() {
				   		handler.close();
				  	});

				  	// window.location.href = result.url;

					// $('.wrap-loader').hide();
			 	//     	element.removeAttr('disabled');
					// $('#errorDonation').fadeOut();
				}

				else if( result.success != false && result.stripeSuccess == true ) {
					window.location.href = result.url;
				}

				else{
					var error = '';
                    for( $key in result.errors ){
                    	error += '<li><i class="glyphicon glyphicon-remove myicon-right"></i> ' + result.errors[$key] + '</li>';
                    }
					$('#showErrorsDonation').html(error);
					$('#errorDonation').fadeIn(500);
					$('.wrap-loader').hide();
					element.removeAttr('disabled');
				}
			 }//<-- RESULT
		   });//<--- AJAX
   		}

   		function initiateStripeTransaction($token)
   		{
   			$.ajax({
			   	type: "POST",
			   	url: URL_BASE+"/api/purchase",
			   	dataType: 'json',
			   	contentType: "application/json",
              	Accept: 'application/json',
              	data: JSON.stringify({ "user_id": $user_id,
			   	"stripeToken": $token
			   	}),
              	headers: {
                  // Set any custom headers here.
                  // If you set any non-simple headers, your server must include these
                  // headers in the 'Access-Control-Allow-Headers' response header.
                  'Accept': 'application/json',
              	},
			   success: function( result ){

			   	console.log(result);

				if( result.success != false && result.stripeSuccess == true ) {
					window.location.href = result.url;
				}
				else{
					var error = '';
                    for( $key in result.errors ){
                    	error += '<li><i class="glyphicon glyphicon-remove myicon-right"></i> ' + result.errors[$key] + '</li>';
                    }
					// $('#showErrorsDonation').html(error);
					// $('#errorDonation').fadeIn(500);
					// $('.wrap-loader').hide();
					// element.removeAttr('disabled');
				}
			 }//<-- RESULT
		   });//<--- AJAX
   		}


		</script>

	<script type="text/javascript">

	Cookies.set('cookieBanner');

		$(document).ready(function() {
    if (Cookies('cookieBanner'));
    else {
    	$('.showBanner').fadeIn();
        $("#close-banner").click(function() {
            $(".showBanner").slideUp(50);
            Cookies('cookieBanner', true);
        });
    }
});
	</script>

</body>
</html>
