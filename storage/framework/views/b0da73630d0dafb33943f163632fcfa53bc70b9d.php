<?php $settings = App\Models\AdminSettings::first(); ?>


<?php $__env->startSection('title'); ?>
<?php echo e(trans('auth.sign_up'), false); ?> -
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('public/plugins/iCheck/all.css'), false); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="jumbotron md index-header jumbotron_set jumbotron-cover">
      <div class="container wrap-jumbotron position-relative">
        <h1 class="title-site"><?php echo e(trans('auth.sign_up'), false); ?></h1>
        <p class="subtitle-site"><strong><?php echo e($settings->title, false); ?></strong></p>
      </div>
    </div>

<div class="container margin-bottom-40">

	<div class="row">
<!-- Col MD -->
<div class="col-md-12">

	<h2 class="text-center position-relative"><?php echo e(trans('auth.sign_up'), false); ?></h2>

	<div class="login-form">

		<?php if(session('notification')): ?>
						<div class="alert alert-success text-center">

							<div class="btn-block text-center margin-bottom-10">
								<i class="glyphicon glyphicon-ok ico_success_cicle"></i>
								</div>

							<?php echo e(session('notification'), false); ?>

						</div>
					<?php endif; ?>

		<?php echo $__env->make('errors.errors-forms', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

          	<form action="<?php echo e(url('register'), false); ?>" method="post" name="form" id="signup_form">

            <input type="hidden" name="_token" value="<?php echo e(csrf_token(), false); ?>">


            <!-- FORM GROUP -->
            <div class="form-group has-feedback">
              <input type="text" class="form-control login-field custom-rounded" value="<?php echo e(old('name'), false); ?>" name="name" placeholder="<?php echo e(trans('users.name'), false); ?>" title="<?php echo e(trans('users.name'), false); ?>" autocomplete="off">
              <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div><!-- ./FORM GROUP -->

             <!-- FORM GROUP -->
            <div class="form-group has-feedback">
              <input type="text" class="form-control login-field custom-rounded" value="<?php echo e(old('email'), false); ?>" name="email" placeholder="<?php echo e(trans('auth.email'), false); ?>" title="<?php echo e(trans('auth.email'), false); ?>" autocomplete="off">
              <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div><!-- ./FORM GROUP -->


         <!-- FORM GROUP -->
         <div class="form-group has-feedback">
              <input type="password" class="form-control login-field custom-rounded" name="password" placeholder="<?php echo e(trans('auth.password'), false); ?>" title="<?php echo e(trans('auth.password'), false); ?>" autocomplete="off">
              <span class="glyphicon glyphicon-lock form-control-feedback"></span>
         </div><!-- ./FORM GROUP -->

         <div class="form-group has-feedback">
			<input type="password" class="form-control" name="password_confirmation" placeholder="<?php echo e(trans('auth.confirm_password'), false); ?>" title="<?php echo e(trans('auth.confirm_password'), false); ?>" autocomplete="off">
			<span class="glyphicon glyphicon-log-in form-control-feedback"></span>
		</div>

		<!-- FORM GROUP -->
         <div class="form-group has-feedback">
              <select name="countries_id" class="form-control" >
                      		<option value=""><?php echo e(trans('misc.select_your_country'), false); ?></option>
                      	<?php $__currentLoopData = App\Models\Countries::orderBy('country_name')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($country->id, false); ?>"><?php echo e($country->country_name, false); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </select>
         </div><!-- ./FORM GROUP -->

		<?php if( $settings->captcha == 'on' ): ?>
            <div class="form-group has-feedback">
              <input type="text" class="form-control login-field" name="captcha" id="lcaptcha" placeholder="" title="">
              <span class="glyphicon glyphicon-lock form-control-feedback"></span>

              <div class="alert alert-danger btn-sm margin-top-alert" id="errorCaptcha" role="alert" style="display: none;">
            		<strong><i class="glyphicon glyphicon-alert myicon-right"></i> <?php echo e(Lang::get('auth.error_captcha'), false); ?></strong>
            	</div>
            </div>
            <?php endif; ?>

            <div class="row margin-bottom-15">
              	<div class="col-xs-11">
              		<div class="checkbox icheck margin-zero">
         				<label class="margin-zero">
         					<input <?php if( old('agree_gdpr') ): ?> checked="checked" <?php endif; ?> class="no-show" name="agree_gdpr" type="checkbox" value="1">
         					<span class="keep-login-title"><?php echo e(trans('admin.i_agree_gdpr'), false); ?></span>
                  <?php if($settings->link_privacy != ''): ?>
                    <a href="<?php echo e($settings->link_privacy, false); ?>" target="_blank"><?php echo e(trans('admin.privacy_policy'), false); ?></a>
                  <?php endif; ?>
         			</label>
         		</div>
              	</div>
              </div><!-- row -->

           <button type="submit" id="buttonSubmit" class="btn btn-block btn-lg btn-main custom-rounded"><?php echo e(trans('auth.sign_up'), false); ?></button>
          </form>
     </div><!-- Login Form -->

 </div><!-- /COL MD -->

</div><!-- ROW -->

 </div><!-- row -->

 <!-- container wrap-ui -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

  <script src="<?php echo e(asset('public/plugins/iCheck/icheck.min.js'), false); ?>"></script>

	<script type="text/javascript">

 <?php if( $settings->captcha == 'on' ): ?>
/*
 *  ==============================================  Captcha  ============================== * /
 */
   var captcha_a = Math.ceil( Math.random() * 5 );
   var captcha_b = Math.ceil( Math.random() * 5 );
   var captcha_c = Math.ceil( Math.random() * 5 );
   var captcha_e = ( captcha_a + captcha_b ) - captcha_c;

function generate_captcha( id ) {
	var id = ( id ) ? id : 'lcaptcha';
	$("#" + id ).html( captcha_a + " + " + captcha_b + " - " + captcha_c + " = ").attr({'placeholder' : captcha_a + " + " + captcha_b + " - " + captcha_c, title: 'Captcha = '+captcha_a + " + " + captcha_b + " - " + captcha_c });
}
$("input").attr('autocomplete','off');
generate_captcha('lcaptcha');

$(document).on('click','#buttonSubmit', function(e){
   	e.preventDefault();
   	var captcha        = $("#lcaptcha").val();
    	if( captcha != captcha_e ){
    		$('.wrap-loader').hide();
				var error = true;
		        $("#errorCaptcha").fadeIn(500);
		        $('#lcaptcha').focus();

		        return false;
		      } else {
		      	$(this).css('display','none');
    			$('.auth-social').css('display','none');
    			$('<div class="btn-block text-center"><i class="fa fa-cog fa-spin fa-3x fa-fw fa-loader"></i></div>').insertAfter('#signup_form');
   				$('#signup_form').submit();
		      }
    });

    <?php else: ?>

	$('#buttonSubmit').click(function(){
    	$(this).css('display','none');
    	$('.auth-social').css('display','none');
    	$('<div class="btn-block text-center"><i class="fa fa-cog fa-spin fa-3x fa-fw fa-loader"></i></div>').insertAfter('#signup_form');
    });

    <?php endif; ?>

    <?php if(count($errors) > 0): ?>
    	scrollElement('#dangerAlert');
    <?php endif; ?>

    <?php if(session('notification')): ?>
    	$('#signup_form, #dangerAlert').remove();
    <?php endif; ?>

    $(document).ready(function(){
  	  $('input').iCheck({
  	  	checkboxClass: 'icheckbox_square-red',
  	  });
  	});

</script>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>