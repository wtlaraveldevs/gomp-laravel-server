// if ('serviceWorker' in navigator) {
//   navigator.serviceWorker.register('../firebase-messaging-sw.js')
//   .then(function(registration) {
//     console.log('Registration successful, scope is:', registration.scope);
//   }).catch(function(err) {
//     console.log('Service worker registration failed, error:', err);
//   });
// }

importScripts('https://www.gstatic.com/firebasejs/6.3.0/firebase-app.js');
    importScripts('https://www.gstatic.com/firebasejs/6.3.0/firebase-messaging.js');
   
   // Your web app's Firebase configuration
    var firebaseConfig = {
        messagingSenderId: "246577318659",
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);

// Retrieve Firebase Messaging object.
    messaging = firebase.messaging();

    messaging.setBackgroundMessageHandler(function(payload) {
      console.log('[firebase-messaging-sw.js] Received background message ', payload);
      // Customize notification here
      var notificationTitle = 'Background Message Title';
      var notificationOptions = {
        body: 'Background Message body.',
        icon: '/firebase-logo.png'
      };

      return self.registration.showNotification(notificationTitle,
        notificationOptions);
    });
    
