<?php

return [
    'driver' => env('FCM_PROTOCOL', 'http'),
    'log_enabled' => true,

    'http' => [
        'server_key' => env('FCM_SERVER_KEY', 'AAAAOWknSwM:APA91bF-T_6Yy03cUA8qtD2f0-nObR_RwBA4r7XpPLmGvf_uOf4AFk7PbANjCtQkfKQaikpdQlwZS2PsD64GPooBi7qvaKrzR1yl62pqpAwD9j0kJW-GldbeTWRX2In8c1WxlrUZYnPF'),
        'sender_id' => env('FCM_SENDER_ID', '246577318659'),
        'server_send_url' => 'https://fcm.googleapis.com/fcm/send',
        'server_group_url' => 'https://android.googleapis.com/gcm/notification',
        'timeout' => 30.0, // in second
    ],
];