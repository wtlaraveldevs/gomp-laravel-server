<?php
/**
* 
*/

namespace App\Classes;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;

use LaravelFCM\Message\Topics;
use App\Models\OAuth;
use FCM;

use DB;
use Log;
use Carbon;
class Firebase 
{
	public $title,$data,$tokens;
	private $time_to_live=60*20;

	function __construct()
	{
		$this->set_credentials();
	}

	public function set_credentials()
	{
		
	}

	public function set_title($title)
	{
		$this->title=$title;
		return $this;
	}

	public function set_actiontype($actiontype)
	{
		$this->data['actiontype']=$actiontype;
		return $this;
	}

	public function set_data($data)
	{
		$this->data=$data;
		return $this;

	}

	public function tokens($type,$id)
	{
		$ret = null;
		switch ($type) {
			case 'mobile_tokens':
				$ret = $this->get_mobile_tokens($id);
				break;
			case 'web_tokens':
				$ret = $this->get_web_tokens($id);
				break;
			default:
				$ret = $this;
		}
		return $ret;
	}

	public function get_web_tokens($user_id)
	{
		Log::debug("ID for webtoken ".$user_id);
		$tokens=[];
		$raw_tokens=DB::table("users")
		->where("id",$user_id)
		->whereNotNull("web_ftoken")
		->select("web_ftoken")
		->get();
		foreach ($raw_tokens as $t) {
			array_push($tokens,$t->web_ftoken);
		}
		$this->tokens=$tokens;

		Log::debug("Firebase, WEB_Tokens=".json_encode($tokens));

		return $this;
	}

	/*Returns array of tokens*/
	public function get_mobile_tokens($user_id)
	{
		$tokens=[];
		$raw_tokens=DB::table("users")
		->where("id",$user_id)
		->whereNotNull("app_ftoken")
		->select("app_ftoken")
		->get();
		foreach ($raw_tokens as $t) {
			array_push($tokens,$t->app_ftoken);
		}
		$this->tokens=$tokens;

		Log::debug("Firebase, APP_Tokens=".json_encode($tokens));

		return $this;
	}


	public function send()
	{
		$optionBuilder = new OptionsBuilder();
		$optionBuilder->setTimeToLive($this->time_to_live);
		$option=$optionBuilder->build();

		/*NOTIFICATION*/
		$notificationBuilder = new PayloadNotificationBuilder($this->title);
        $notificationBuilder->setSound('default');
        $notificationBuilder->setBody("You have got a notification from OpenSupermall.com");
        $notification=$notificationBuilder->build();

        /*DATA*/
		$dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($this->data);
        $data=$dataBuilder->build();

        $response=FCM::sendTo($this->tokens, $option,$notification,$data);

        // dump($response);
        return;
	}
}
