<?php

namespace App\Http\Controllers\RN;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\AdminSettings;
use App\Models\Campaigns;
use App\Models\Categories;
use App\Models\User;
use App\Http\Controllers\Controller;
use DB;
use Carbon;
use Log;

class GlobalController extends Controller
{
	public function search(Request $request) {

		$q = trim($request->text);
		$settings = AdminSettings::first();

		// $page = $request->input('page');

		$data = Campaigns::where( 'title','LIKE', '%'.$q.'%' )
		->where('status', 'active' )
		->orWhere('location','LIKE', '%'.$q.'%')
		->where('status', 'active' )
		->orWhere('token_id','LIKE', '%'.$q.'%')
		->where('status', 'active' )
		->groupBy('id')
		->orderBy('id', 'desc' )
		->get();
		// ->paginate( $settings->result_request );

		Log::debug("Data ".json_encode($data));

		$title = trans('misc.result_of').' '. $q .' - ';

		// $total = $data->total();

		return response()->json([
            "status"=>"success",
            "data"=>compact( 'data', 'title', 'total', 'q' )
        ]);

	}// End Method

	public function searchPot(Request $request) {

		$q = trim($request->text);
		$settings = AdminSettings::first();

		// $page = $request->input('page');

		$data = Campaigns::where('token_id','LIKE', '%'.$q.'%')
		->where('status', 'active' )
		->groupBy('id')
		->orderBy('id', 'desc' )
		->get();
		// ->paginate( $settings->result_request );

		Log::debug("Data ".json_encode($data));

		$title = trans('misc.result_of').' '. $q .' - ';

		// $total = $data->total();

		return response()->json([
            "status"=>"success",
            "data"=>compact( 'data', 'title', 'total', 'q' )
        ]);

	}// End Method

	protected function buygomp($campaign,$pledge){

		$email    = $this->request->email;
		$cents    = bcmul($this->request->amount, 100);
		$amount = (int)$cents;
		$currency_code = $this->settings->currency_code;
		$description = trans('misc.donation_for').' '.$campaign->title;
		$nameSite = $this->settings->title;


		if( isset( $this->request->stripeToken ) ) {

				\Stripe\Stripe::setApiKey($this->settings->stripe_secret_key);

				// Get the credit card details submitted by the form
				$token = $this->request->stripeToken;

				// Create a charge: this will charge the user's card
				try {
					$charge = \Stripe\Charge::create(array(
						"amount" => $amount, // Amount in cents
						"currency" => strtolower($currency_code),
						"source" => $token,
						"description" => $description
						));

					if( !isset( $this->request->anonymous ) ) {
						$this->request->anonymous = '0';
					}

					// Insert DB and send Mail
					$sql                   = new Donations;
					$sql->campaigns_id     = $campaign->id;
					$sql->txn_id           = 'null';
					$sql->fullname         = $this->request->full_name;
					$sql->email            = $this->request->email;
					$sql->country          = $this->request->country;
					$sql->postal_code      = $this->request->postal_code;
					$sql->donation         = $this->request->amount;
					$sql->payment_gateway  = 'Stripe';
					$sql->comment          = $this->request->comment;
					$sql->anonymous        = $this->request->anonymous;
					$sql->rewards_id       = $pledge;
					$sql->save();

					$this->sendEmail($campaign);

			return response()->json([
								'success' => true,
								'stripeSuccess' => true,
								'url' => url('paypal/donation/success',$campaign->id)
						]);

				} catch(\Stripe\Error\Card $e) {
					// The card has been declined
				}
		} else {
			return response()->json([
								'success' => true,
								'stripeTrue' => true,
								"key" => $this->settings->stripe_public_key,
								"email" => $email,
								 "amount" => $amount,
							"currency" => strtoupper($currency_code),
							"description" => $description,
							"name" => $nameSite
						]);
		}
	}//<----- End Method stripe()

}
