<?php

namespace App\Http\Controllers\RN;

use Mail;
use App\Models\User;
use Validator;
use Illuminate\Http\Request;
use App\Models\AdminSettings;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use DB;
use Carbon;
use Log;

class AuthController extends Controller
{

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
        // $this->middleware('guest', ['except' => 'logout']);
    }

    public function authenticate(Request $request)
    {

        Log::debug("Email ".$request->email);
        Log::debug("Password ".$request->password);

        $credentials = $request->only('email', 'password');
        Log::debug("User ".json_encode($credentials));

        // try {
        //     // attempt to verify the credentials and create a token for the user
        //     if (! $token = JWTAuth::attempt($credentials)) {
        //         return response()->json(['error' => 'invalid_credentials'], 401);
        //     }
        // } catch (JWTException $e) {

        //     Log::info("something went wrong whilst attempting to encode the token");
        //     return response()->json(['error' => 'could_not_create_token'], 500);
        // }

        if( $this->auth->attempt($credentials) ){
                
                if($this->auth->User()->status == 'active') {
                    $user=User::where('email',$request->email)->first();
                    return response()->json([
                        "status"=>"success",
                        "short_message"=>"Welcome ".$user->name
                    ]);
            }
        }

        // all good so return the token
        return response()->json([
                "status"=>"failed",
                "short_message"=>"Somwthing went wrong"
            ]);
    }


    public function register(Request $request)
    {

        $forename=$request->forename;
        $surname=$request->surname;
        $nick=$request->nick;
        $email=$request->email;
        $mobile=$request->mobile;
        $password=$request->mobile;
        $token = str_random(75);
        $address=$request->address;

        $response=User::create([
            'name' => $forename,
            'Surname' => $surname,
            'Nick' => $nick,
            'email' => $email,
            'Mobile'=> $mobile,
            'password' => bcrypt($password),
            'countries_id' => '',
            'avatar' => 'default.jpg',
            'status' => "active",
            'role' => 'normal',
            'token' => $token,
            'confirmation_code' => '',
            'Address' => $address
        ]);

        if(empty($response))
        {
            return response()->json([
                "status"=>"failed",
                "short_message"=>"Something went wrong while registration."
            ]);
        }
        else{
            return response()->json([
                "status"=>"success",
                "short_message"=>"Welcome ".$response->name
            ]);
        }

        
    }
}
