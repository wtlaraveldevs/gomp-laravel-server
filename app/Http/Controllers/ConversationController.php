<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\AdminSettings;
use App\Helper;
use Carbon\Carbon;
use DB;
use Log;
use App\Classes\Firebase;

class ConversationController extends Controller
{

	public function __construct( AdminSettings $settings, Request $request) {
		$this->settings = $settings::first();
		$this->request = $request;
	}

	public function index($id){

		$user_id=$this->request->id;

		$friends=DB::table('conversation')
			->where(function ($query) {
				    $query->where('from_user_id', '=', $this->request->id)
				          ->orWhere('to_user_id', '=', $this->request->id);
				})
			->get();

		$temp=DB::table('conversation')
			->where(function ($query) {
				    $query->where('from_user_id', '=', $this->request->id)
				          ->orWhere('to_user_id', '=', $this->request->id);
				})
			->first();

		if($temp->from_user_id == $user_id)
		{
			$friendName = DB::table('users')
				->where('id', $temp->to_user_id)
				->pluck('name')
				->first();

			$friendID = DB::table('users')
				->where('id', $temp->to_user_id)
				->pluck('id')
				->first();
		}
		else
		{
			$friendName = DB::table('users')
				->where('id', $temp->from_user_id)
				->pluck('name')
				->first();

			$friendID = DB::table('users')
				->where('id', $temp->from_user_id)
				->pluck('id')
				->first();
		}

		$ids = [];
		$firstId = DB::table('conversation')
			->where(function ($query) {
				    $query->where('from_user_id', '=', $this->request->id)
				          ->orWhere('to_user_id', '=', $this->request->id);
				})
			->pluck('conversation_id')
			->first();

		foreach ($friends as $record) {
			if($record->from_user_id == $this->request->id)
			{
				array_push($ids, $record->to_user_id);
			}
			else{
				array_push($ids, $record->from_user_id);	
			}
		}

		Log::debug("firstId =".$firstId);
		Log::debug("userId =".$user_id);

		$convo_users= DB::table('users')
			->whereIn('id', $ids)
			->select("users.*")
			->get();

		// Log::debug("Users =".json_encode($convo_users));

		$conversations = DB::table('convo_messages')
			->where('conversation_id', $firstId)
			->get();

		Log::debug("conversations =".json_encode($conversations));

		$searchedUsers =[];

		if(empty($friends))
		{
			$hasConversations = false;
		}
		else{
			$hasConversations = true;
		}
		

		return view('conversation.index',['hasconvo' => $hasConversations,'convo' => $conversations, 'searchUsers' => $searchedUsers, 'convo_users' => $convo_users,
			'friendName'=>$friendName, 'friendID'=>$friendID, 'user_id'=>$user_id]);
	}

	public function getConversation(Request $request)
	{
		$ret=array();
		$ret["status"]="failure";

		try{

			// user_id and friend_id
			$user_id = $request->user_id;
			$friend_id = $request->friend_id;

			
			$query = "SELECT * from convo_messages
					join conversation on conversation.conversation_id = convo_messages.conversation_id
					where (conversation.from_user_id = $friend_id AND conversation.to_user_id =$user_id) OR (conversation.from_user_id=$user_id AND conversation.to_user_id=$friend_id)";

			$conversations=DB::select(DB::raw($query));

			$ret["status"]="success";
			$ret["messages"]=$conversations;

			return response()->json($ret);

		}
		catch(\Exception $e)
		{
			Log::info('Error @ '.$e->getLine().' file '.$e->getFile().' '.$e->getMessage());
		}
	}
	
	// public function delete($id) {

	// 	$data = Campaigns::where('id', $this->request->id)
	// 	->where('user_id', Auth::user()->id)
	// 	->firstOrFail();

	// 	$path_small     = 'public/campaigns/small/';
	// 	$path_large     = 'public/campaigns/large/';
	// 	$path_updates = 'public/campaigns/updates/';

	// 	$updates = $data->updates()->get();

	// 	//Delete Updates
	// 	foreach ($updates as $key) {

	// 		if ( \File::exists($path_updates.$key->image) ) {
	// 				\File::delete($path_updates.$key->image);
	// 			}//<--- if file exists

	// 			$key->delete();
	// 	}//<--

	// 	// Delete Campaign
	// 	if ( \File::exists($path_small.$data->small_image) ) {
	// 				\File::delete($path_small.$data->small_image);
	// 			}//<--- if file exists

	// 			if ( \File::exists($path_large.$data->large_image) ) {
	// 				\File::delete($path_large.$data->large_image);
	// 			}//<--- if file exists

	// 			//Delete campaign Reported
	//  		 $campaignReporteds = CampaignsReported::where('campaigns_id',$this->request->id)->get();

	//  		 if($campaignReporteds) {
	//  			 foreach ($campaignReporteds as $campaignReported) {
	//  					 $campaignReported->delete();
	//  			 }//<-- foreach
	//  		 }// IF

	// 		 $data->delete();

	// 	 return redirect('/');

	// }//<<--- End Method


	// public function update($id){

	// 	$data = Campaigns::where('id', $this->request->id)
	// 	->where('user_id', Auth::user()->id)
	// 	->firstOrFail();

	// 	return view('campaigns.update')->withData($data);
	// }//<---- End Method

	public function searchUsers(Request $request)
	{
		$ret=array();
		$ret["status"]="failure";


		try{
			// get email_id from the request body.
			$email=$request->email;

			if(empty($email))
			{
				$ret["status"]="success";
				$ret["message"]="no user found";
			}

			$user=DB::table('users')
				->where('email',$email)
				->get();

			if(!empty($user))
			{
				$ret["status"]="success";
				$ret["users"]=$user;
			}
			else{
				$ret["status"]="failure";
				$ret["message"]="no user found";
			}
			
			return response()->json($ret);
		}
		catch(\Exception $e)
		{
			Log::info('Error @ '.$e->getLine().' file '.$e->getFile().' '.$e->getMessage());
		}
	}

	public function sendMessage(Request $request)
	{
		$ret=array();
		$ret["status"]="failure";

		try{
			$user_id=$request->user_id;
			Log::debug("user_id =".$user_id);
			$target_id=$request->target_id;
			Log::debug("target_id =".$target_id);
			$message=$request->message;
			Log::debug("message =".$message);

			$ids=array();
			array_push($ids, $user_id);
			array_push($ids, $target_id);

			Log::debug("Ids=".json_encode($ids));

			$ConvoId=DB::table('conversation')
				->whereIn('from_user_id', $ids)
				->whereIn('to_user_id', $ids)
				->pluck('conversation_id')
				->first();

			Log::debug("ConvoId =".$ConvoId);

			if(empty($ConvoId))
			{
				$data=[
					"from_user_id"=>$user_id,
					"to_user_id"=>$target_id
				];

				$conversation_id=DB::table('conversation')
					->insertGetId($data);


				$data=[
					"description"=>$message,
					"is_read"=>"0",
					"user_id"=>$user_id,
					"conversation_id"=>$conversation_id,
					"created_on"=>Carbon::now(),
					"updated_on"=>Carbon::now(),
				];

				DB::table('convo_messages')->insert($data);

			}
			else{
				$data=[
					"description"=>$message,
					"is_read"=>"0",
					"user_id"=>$user_id,
					"conversation_id"=>$ConvoId,
					"created_on"=>Carbon::now(),
					"updated_on"=>Carbon::now(),
				];

				DB::table('convo_messages')->insert($data);

			}
			$ret["status"]="success";

			$data=[
				"action_type"=>"new_message",
				"message"=>$message
			];

			/* Needs to be handled by a job ~ Zurez */
			try {
				$f = new Firebase();
				$f->set_title("You have a new message")->
				tokens("web_tokens", $target_id)->
				set_data($data)->
				send();
			} catch (\Exception $e) {
				Log::debug('Error '.$e->getMessage());
			}

			return response()->json($ret);
		}
		catch(\Exception $e)
		{
			Log::info('Error @ '.$e->getLine().' file '.$e->getFile().' '.$e->getMessage());
			$ret["status"]="failure";
			$ret["message"]="Something went wrong on the server.";
		}	
	}

}

