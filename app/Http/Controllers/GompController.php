<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\AdminSettings;
use App\Models\Campaigns;
use App\Models\Donations;
use App\Models\User;
use App\Models\Rewards;
use Fahim\PaypalIPN\PaypalIPNListener;
use App\Helper;
use Mail;
use Carbon\Carbon;


class GompController extends Controller
{
	public function __construct( AdminSettings $settings, Request $request) {
		$this->settings = $settings::first();
		$this->request = $request;

	}
	$pledge=1;
	$campaign={
		"id"=>1
	};
		
	public function send(){
		console.log($pledge);
		return $this->paypal($campaign,$pledge);
	}
	protected function paypal($campaign, $pledge)
	{
		// $campaign={
		// 	"id"=>1
		// };
		
			if ( $this->settings->paypal_sandbox == 'true') {
				// SandBox
				$action = "https://www.sandbox.paypal.com/cgi-bin/webscr";
				} else {
				// Real environment
				$action = "https://www.paypal.com/cgi-bin/webscr";
				}

			$urlSuccess = url('paypal/donation/success',1);
			$urlCancel   = url('paypal/donation/cancel',1);
			$urlPaypalIPN = url('paypal/ipn');

			return response()->json([
					        'success' => true,
					        'formPP' => '<form id="form_pp" name="_xclick" action="'.$action.'" method="post"  style="display:none">
					        <input type="hidden" name="cmd" value="_donations">
					        <input type="hidden" name="return" value="'.$urlSuccess.'">
					        <input type="hidden" name="cancel_return"   value="'.$urlCancel.'">
					        <input type="hidden" name="notify_url" value="'.$urlPaypalIPN.'">
					        <input type="hidden" name="currency_code" value="'.$this->settings->currency_code.'">
					        <input type="hidden" name="amount" id="amount" value="2">
					        <input type="hidden" name="custom" value="id=1&fn=&mail=&cc=&pc=&cm=&anonymous=true&pl='.$pledge.'">
					        <input type="hidden" name="item_name" value="buying a gomp">
					        <input type="hidden" name="business" value="'.$this->settings->paypal_account.'">
					        <input type="submit">
					        </form>',
					    ]);

	}//<------ End Method paypal()


}