<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\AdminSettings;
use App\Models\Campaigns;
use App\Models\Donations;
use App\Models\User;
use App\Models\Rewards;
use Fahim\PaypalIPN\PaypalIPNListener;
use App\Helper;
use Mail;
use Carbon\Carbon;
use Log;
use DB;


class PaymentController extends Controller
{
	public function __construct( AdminSettings $settings, Request $request) {
		$this->settings = $settings::first();
		$this->request = $request;
	}

	public function completed()
    {
	   Log::debug("This is sucess view".$this->request);
	   $uriCampaign = $this->request->path();

	   return redirect($uriCampaign."payment/sucess");
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function success($id)
    {
	    Log::debug("This is sucess view".$this->request);
	 	
	 	$dId=DB::table("donations")
	 		->where('campaigns_id', $id)
	 		->orderBy('id', 'desc')
	 		->pluck('id');

	 	DB::table('donations')
	 	->where('id', $dId)
	 	->update([
	 		"approved"=>'1',
	 		"completed"=>'1'
	 	]);

	 	DB::table('withdrawals')
	 	->insert([
	 		"campaigns_id"=>$id,
	 		"status"=>'pending'
	 	]);

		return view('default.thankyou');

    }// End Method

    public function paypalIpn(){

		$ipn = new PaypalIPNListener();

		$ipn->use_curl = false;

		if ( $this->settings->paypal_sandbox == 'true') {
			// SandBox
			$ipn->use_sandbox = true;
			} else {
			// Real environment
			$ipn->use_sandbox = false;
			}

	    $verified = $ipn->processIpn();

		//$report = Helper::checkTextDb($ipn->getTextReport()); // Report the transation

		$custom  = $_POST['custom'];
		parse_str($custom, $donation);

		$payment_status = $_POST['payment_status'];
		$txn_id               = $_POST['txn_id'];
		$amount             = $_POST['mc_gross'];

	    if ($verified) {
	        if($payment_status == 'Completed'){
	          // Check outh POST variable and insert in DB

	          $verifiedTxnId = Donations::where('txn_id',$txn_id)->first();

			if( !isset( $verifiedTxnId ) ) {

							}// <--- Verified Txn ID

	      } // <-- Payment status
	    } else {
	    	//Some thing went wrong in the payment !
	    }

    }//<----- End Method paypalIpn()


}
