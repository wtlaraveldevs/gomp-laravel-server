<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Twilio\Rest\Client;
use Validator;
use Log;

class BulkSmsController extends Controller
{
    //
    public function sendSms( Request $request )
    {
       // Your Account SID and Auth Token from twilio.com/console
       $sid    = env( 'TWILIO_SID' );
       $token  = env( 'TWILIO_TOKEN' );
       $client = new Client( $sid, $token );

       $validator = Validator::make($request->all(), [
           // 'numbers' => 'required',
           'message' => 'required'
       ]);

       Log::debug("My_NUMBER".env( 'TWILIO_FROM' ));

       if ( $validator->passes() ) {

          $client->messages->create(
            $request->number,
            [
              'from' => env( 'TWILIO_FROM' ),
              'body' => $request->message,
            ]
          );

           // $numbers_in_arrays = explode( ',' , $request->input( 'numbers' ) );

           // $message = $request->input( 'message' );
           // $count = 0;

           // foreach( $numbers_in_arrays as $number )
           // {
           //     $count++;

           //     $client->messages->create(
           //         $number,
           //         [
           //             'from' => env( 'TWILIO_FROM' ),
           //             'body' => $message,
           //         ]
           //     );
           // }

           return back(); //->with( 'success', $count . " messages sent!" );
              
       } else {
           return back()->withErrors( $validator );
       }
   }
}
